\begin{align*}
&v_{i}\left(\textbf{h}_{i},w\right ) = max_{j}\left [ r_{j}\left ( \textbf{h}_{i}|w \right )+v_{k}\left( \textbf{h}_{k},w \right )\right ],i\in D,j\in D\left ( i|\textbf{h}_{i} \right )\\
\end{align*}