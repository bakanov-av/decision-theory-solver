\begin{align*}
&v_{i}\left(\textbf{h}_{i},w\right ) = \sum_{j}p_{j}\left ( \textbf{h}_{i} \right )\left [ r_{j}\left ( \textbf{h}_{i}|w \right )+v_{k}\left( \textbf{h}_{k},w \right )\right ],i\in C,j\in C\left ( i|\textbf{h}_{i} \right )\\
\end{align*}