<html> 
	<p>     
		<b>Рассчет полезностей решений.</b><br/>    

		Ценности исходов определяются с учетом введенных обозначений как: <br/><br/>
		V(y<sub>1</sub>, d<sub>1</sub>, x<sub>1</sub>) = v(a<sub>1</sub>) = v<sub>1</sub> = 0.25; <br/>
		V(y<sub>1</sub>, d<sub>1</sub>, x<sub>2</sub>) = v(a<sub>2</sub>) = v<sub>2</sub> = -1; <br/>
		V(y<sub>2</sub>, d<sub>1</sub>, x<sub>1</sub>) = v(a<sub>3</sub>) = v<sub>3</sub> = 2; <br/>
		V(y<sub>2</sub>, d<sub>1</sub>, x<sub>2</sub>) = v(a<sub>4</sub>) = v<sub>4</sub> = -1; <br/>
		V(d<sub>2</sub>) = v(a<sub>5</sub>) = v<sub>5</sub> = 0. <br/>

		Полезность решения d<sub>1</sub> = 
		u(d<sub>1</sub>) = p<sub>k</sub>(p<sub>11</sub>v<sub>1</sub> + 
			(1 - p<sub>11</sub>)v<sub>2</sub>) + (1 - p<sub>k</sub>)(p<sub>12</sub>v<sub>3</sub> + 
			(1 - p<sub>12</sub>)v<sub>4</sub>), 
		u(d<sub>1</sub>) = p<sub>k</sub>(1.25p<sub>11</sub> - 1) + (1 - p<sub>k</sub>)(3p<sub>12</sub> - 1).<br/>

		Полезность решения d<sub>2</sub> = u(d<sub>2</sub>) = v<sub>4</sub> = 0.<br/><br/>

		<b>Выбор решения.</b><br/>
		Нэнси сделает ставку только в том случае, если u(d<sub>1</sub>) > 0, и, конечно, ее решение 
		будет зависеть от вероятностей p<sub>k</sub>, p<sub>11</sub> и p<sub>12</sub>: <br/>

		если u(d<sub>1</sub>) > 0, то d<sup>*</sup> = d<sub>1</sub>, иначе d<sup>*</sup> = d<sub>2</sub>. <br/>
		С учетом полученных значений полезности решения: <br/>
		d<sup>*</sup> = d<sub>1</sub>, если p<sub>k</sub>(1.25p<sub>11</sub> - 1) + (1 - p<sub>k</sub>)(3p<sub>12</sub> - 1) > 0, 
		иначе d<sup>*</sup> = d<sub>2</sub>. <br/><br/>

		<b>Вероятность безразличия</b><br/>
		Вероятность безразличия p&#772; - значение p<sub>12</sub>, при котором принятое
		решение не оказывает влияния на математическое ожидание получаемого <br/>дохода, т. е. 
		3p&#772; - 1 = 1.25p<sub>11</sub> - 1, следовательно: p&#772; = 5p<sub>11</sub> / 12.

		Тогда d<sup>*</sup> = d<sub>1</sub>, если p<sub>12</sub> > p&#772;, иначе d<sup>*</sup> = d<sub>2</sub>.
	</p>   
</html>