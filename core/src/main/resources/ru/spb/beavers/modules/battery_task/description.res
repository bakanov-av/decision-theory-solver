<html> 
	<h2 align='center'>  Неформальная постановка задачи  </h2>  
	<p>
		Оптовый поставщик дорогих промышленных батарей специального назначения может продать до 3 шт. в год.
		Стоимость производства одной батареи составляет v<sub>c</sub>. Отпускная цена каждой батареи – v<sub>s</sub>,
		и срок годности каждой – 1 год.	Все непроданные батареи, пролежавшие год, ничего не стоят (стоимость
		просроченных батарей незначительна по сравнению со стоимостью производства). Спрос на батареи предсказать
		невозможно, но он в любом случае не больше 3 шт. в год. Необходимо принять решение, сколько батарей стоит
		выпустить в начале года, чтобы максимизировать ожидаемую годовую прибыль.
	</p>         
</html>