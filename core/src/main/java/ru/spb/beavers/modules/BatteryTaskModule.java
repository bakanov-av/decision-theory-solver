package ru.spb.beavers.modules;

import ru.spb.beavers.modules.battery_task.Common;
import ru.spb.beavers.modules.battery_task.SolutionTree;
import ru.spb.beavers.modules.battery_task.TaskParms;

import javax.swing.*;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 * Класс реализует интерфейс ITaskModule, загружается ядром, используется для
 * решения задачи №2.1.1.
 *
 * @author Alexey Borisov
 */
public class BatteryTaskModule implements ITaskModule {

	private final TaskParms parms;

	private final JLabel headerLabel;
	private final JLabel costLabel;
	private final JLabel priceLabel;
	private final JLabel prob0Label;
	private final JLabel prob1Label;
	private final JLabel prob2Label;
	private final JLabel prob3Label;
	private final JLabel messageArea;

	private final JTextField costInputText;
	private final JTextField priceInputText;
	private final JTextField prob0InputText;
	private final JTextField prob1InputText;
	private final JTextField prob2InputText;
	private final JTextField prob3InputText;

	/**
	 * Устанавливает значения по умалчанию в текстовых полях ввода параметров задачи.
	 */
	private void setDefaultInputValues() {
		costInputText.setText("25");
		priceInputText.setText("210");
		prob0InputText.setText("0.15");
		prob1InputText.setText("0.35");
		prob2InputText.setText("0.30");
		prob3InputText.setText("0.20");
	}

	/**
	 * Проверяет, является ли передаваемый параметр текстовым представлением
	 * целого или вещественного числа.
	 *
	 * @param text строка для анализа
	 * @return true если text является символьной записью числа, иначе false.
	 */
	private boolean isValidTextValue(String text) {
		Pattern p = Pattern.compile("\\-?\\d+(\\.\\d{0,})?", Pattern.UNICODE_CASE);
		return p.split(text).length == 0;
	}

	/**
	 * Проверяет корректность введенных исходных данных в текстовых полях ввода
	 * параметро для решаемой задачи.
	 *
	 * @return true, если данные заданы корректно, иначе false.
	 */
	private boolean isParmsValid() {
		if (messageArea.getText().length() != 0) return false;
		if (costInputText.getText().length() == 0) return false;
		if (priceInputText.getText().length() == 0) return false;
		if (prob0InputText.getText().length() == 0) return false;
		if (prob1InputText.getText().length() == 0) return false;
		if (prob2InputText.getText().length() == 0) return false;
		if (prob3InputText.getText().length() == 0) return false;
		return true;
	}

	/**
	 * Сохраняет заданные пользователем параметры рашаемой задачи в объекте класса
	 * TaskParms.
	 */
	private void readParms() {
		parms.batteriesCost = Float.parseFloat(costInputText.getText());
		parms.batteriesPrice = Float.parseFloat(priceInputText.getText());
		parms.p0 = Float.parseFloat(prob0InputText.getText());
		parms.p1 = Float.parseFloat(prob1InputText.getText());
		parms.p2 = Float.parseFloat(prob2InputText.getText());
		parms.p3 = Float.parseFloat(prob3InputText.getText());
	}

	/**
	 * Инициализирует поля класса ссылками на реальные объекты.
	 * Устанавливает обработчики событий изменения текста в текстовых полях ввода
	 * параметров задачи, чтобы обеспечить проверку вводимых данных в режиме
	 * реального времени.
	 */
	public BatteryTaskModule() {
		parms = new TaskParms();

		headerLabel = new JLabel(Common.INPUT_HEADER);
		costLabel = new JLabel(Common.BATTERIES_COST);
		priceLabel = new JLabel(Common.BATTERIES_PRICE);
		prob0Label = new JLabel(Common.PROBABILITY_0);
		prob1Label = new JLabel(Common.PROBABILITY_1);
		prob2Label = new JLabel(Common.PROBABILITY_2);
		prob3Label = new JLabel(Common.PROBABILITY_3);
		messageArea = new JLabel();

		costInputText = new JTextField(20);
		costInputText.setText("25");
		costInputText.addCaretListener(new CaretListener() {
			@Override
			public void caretUpdate(CaretEvent ce) {
				costInputText.setBackground(Color.WHITE);
				messageArea.setText("");
				String text = costInputText.getText();
				float num = Float.parseFloat(text);
				if (!isValidTextValue(text) && text.length() != 0 || (num < 0.0f)) {
					costInputText.setBackground(new Color(255, 155, 155));
					messageArea.setText(Common.INPUT_ERROR);
				}
			}
		});
		priceInputText = new JTextField(20);
		priceInputText.setText("210");
		priceInputText.addCaretListener(new CaretListener() {
			@Override
			public void caretUpdate(CaretEvent ce) {
				priceInputText.setBackground(Color.WHITE);
				messageArea.setText("");
				String text = priceInputText.getText();
				float num = Float.parseFloat(text);
				if (!isValidTextValue(text) && text.length() != 0 || (num < 0.0f)) {
					priceInputText.setBackground(new Color(255, 155, 155));
					messageArea.setText(Common.INPUT_ERROR);
				}
			}
		});
		prob0InputText = new JTextField(20);
		prob0InputText.setText("0.15");
		prob0InputText.addCaretListener(new CaretListener() {
			@Override
			public void caretUpdate(CaretEvent ce) {
				prob0InputText.setBackground(Color.WHITE);
				messageArea.setText("");
				String text = prob0InputText.getText();
				float num = Float.parseFloat(text);
				if (!isValidTextValue(text) && text.length() != 0 || (num < 0.0f || num > 1.0f)) {
					prob0InputText.setBackground(new Color(255, 155, 155));
					messageArea.setText(Common.INPUT_ERROR);
				}
			}
		});
		prob1InputText = new JTextField(20);
		prob1InputText.setText("0.35");
		prob1InputText.addCaretListener(new CaretListener() {
			@Override
			public void caretUpdate(CaretEvent ce) {
				prob1InputText.setBackground(Color.WHITE);
				messageArea.setText("");
				String text = prob1InputText.getText();
				float num = Float.parseFloat(text);
				if (!isValidTextValue(text) && text.length() != 0 || (num < 0.0f || num > 1.0f)) {
					prob1InputText.setBackground(new Color(255, 155, 155));
					messageArea.setText(Common.INPUT_ERROR);
				}
			}
		});
		prob2InputText = new JTextField(20);
		prob2InputText.setText("0.30");
		prob2InputText.addCaretListener(new CaretListener() {
			@Override
			public void caretUpdate(CaretEvent ce) {
				prob2InputText.setBackground(Color.WHITE);
				messageArea.setText("");
				String text = prob2InputText.getText();
				float num = Float.parseFloat(text);
				if (!isValidTextValue(text) && text.length() != 0 || (num < 0.0f || num > 1.0f)) {
					prob2InputText.setBackground(new Color(255, 155, 155));
					messageArea.setText(Common.INPUT_ERROR);
				}
			}
		});
		prob3InputText = new JTextField(20);
		prob3InputText.setText("0.20");
		prob3InputText.addCaretListener(new CaretListener() {
			@Override
			public void caretUpdate(CaretEvent ce) {
				prob3InputText.setBackground(Color.WHITE);
				messageArea.setText("");
				String text = prob3InputText.getText();
				float num = Float.parseFloat(text);
				if (!isValidTextValue(text) && text.length() != 0 || (num < 0.0f || num > 1.0f)) {
					prob3InputText.setBackground(new Color(255, 155, 155));
					messageArea.setText(Common.INPUT_ERROR);
				}
			}
		});
	}

	/**
	 * Возвращает название задачи.
	 *
	 * @return строка с названием задачи.
	 */
	@Override
	public String getTitle() {
		return Common.TITLE;
	}

	/**
	 * Размещает в переданном объекте класса JPanel информацию с кратким описанием
	 * решаемой задачи.
	 *
	 * @param panel - контекст отображения данных.
	 */
	@Override
	public void initDescriptionPanel(JPanel panel) {
		String descriptionText = "";

		// Get the HTML based description text from resources.
		try {
			URL resource = ClassLoader.getSystemClassLoader().getResource(
					Common.RESOURCE_PATH + Common.DESCRIPTION_FILE
			);
			assert (resource != null);
			Path path = Paths.get(resource.toURI());
			List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
			for (String line : lines) descriptionText += line;
		} catch (IOException | URISyntaxException ex) {
			Logger.getLogger(BatteryTaskModule.class.getName()).log(Level.SEVERE, null, ex);
		}

		// Set up description context.
		JLabel descriptionLabel = new JLabel(descriptionText);
		descriptionLabel.setPreferredSize(new Dimension(770, 400));
		panel.removeAll();
		panel.add(descriptionLabel);
	}

	/**
	 * Размещает в переданном объекте класса JPanel информацию с подробным
	 * описанием решения задачи.
	 *
	 * @param panel - контекст отображения данных.
	 */
	@Override
	public void initSolutionPanel(JPanel panel) {
		String solutionText = "";

		// Get the HTML based solution text from resources.
		try {
			URL resource = ClassLoader.getSystemClassLoader().getResource(
					Common.RESOURCE_PATH + Common.SOLUTION_FILE
			);
			assert resource != null;
			Path path = Paths.get(resource.toURI());
			List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
			for (String line : lines) solutionText += line;
		} catch (IOException | URISyntaxException ex) {
			Logger.getLogger(BatteryTaskModule.class.getName()).log(Level.SEVERE, null, ex);
		}

		// Set up solution context.
		panel.removeAll();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.add(new JLabel(solutionText));
		panel.add(new SolutionTree(null));
		panel.add(new JLabel(Common.DIAGRAM_TEXT));
		URL resource = ClassLoader.getSystemClassLoader().getResource(
				Common.RESOURCE_PATH + Common.DIAGRAM_FILE
		);
		panel.add(new JLabel(new ImageIcon(resource)));
	}

	/**
	 * Размещает в переданном объекте класса JPanel текстовые поля для ввода
	 * параметров решаемой задачи.
	 *
	 * @param panel - контекст отображения данных.
	 */
	@Override
	public void initInputPanel(JPanel panel) {
		JPanel context = new JPanel();
		context.setLayout(new BoxLayout(context, BoxLayout.Y_AXIS));

		context.add(headerLabel);
		context.add(costLabel);
		context.add(costInputText);
		context.add(priceLabel);
		context.add(priceInputText);
		context.add(prob0Label);
		context.add(prob0InputText);
		context.add(prob1Label);
		context.add(prob1InputText);
		context.add(prob2Label);
		context.add(prob2InputText);
		context.add(prob3Label);
		context.add(prob3InputText);
		context.add(messageArea);

		panel.removeAll();
		panel.add(context);
	}

	/**
	 * Размещает в переданном объекте класса JPanel решение задачи.
	 *
	 * @param panel - контекст отображения данных.
	 */
	@Override
	public void initExamplePanel(JPanel panel) {
		panel.removeAll();
		if (!isParmsValid()) {
			JLabel errorLabel = new JLabel(Common.PARMS_ERROR);
			panel.add(errorLabel);
			return;
		}
		readParms();

		float u1 = parms.batteriesPrice - parms.batteriesCost;
		u1 -= parms.p0 * parms.batteriesPrice;

		float u2 = 2 * (parms.batteriesPrice - parms.batteriesCost);
		u2 -= 2 * parms.p0 * parms.batteriesPrice;
		u2 -= parms.p1 * parms.batteriesPrice;

		float u3 = 3 * (parms.batteriesPrice - parms.batteriesCost);
		u3 -= 3 * parms.p0 * parms.batteriesPrice;
		u3 -= 2 * parms.p1 * parms.batteriesPrice;
		u3 -= parms.p0 * parms.batteriesPrice;

		float solution = 0;
		String solutionText = "не производить ни одной батареи.";
		if (u1 > solution) {
			solution = u1;
			solutionText = "произвести одну батарею.";
		}
		if (u2 > solution) {
			solution = u2;
			solutionText = "произвести две батареи.";
		}
		if (u3 > solution) {
			solution = u3;
			solutionText = "произвести три батареи.";
		}

		String theoryText =
				"<html><h2 align='center'>Решение</h2><p>" +
						"Множество решений D = {d<sub>0</sub>, d<sub>1</sub>, d<sub>2</sub>, d<sub>3</sub>}, " +
						"где d<sub>i</sub> - решение выпускать i батарей (i = 0, 1, 2, 3). <br/>" +
						"Случайная величина Y = {y<sub>0</sub>, y<sub>1</sub>, y<sub>2</sub>, y<sub>3</sub>} " +
						"описывает спрос на батареи, " +
						"y<sub>i</sub> - спрос на i батарей (i = 0, 1, 2, 3).<br/>\n" +
						"Принятое решение описывается величиной <i>D</i>, принимающей значения из множества D. \n" +
						"Вероятность события y<sub>i</sub> обозначим как p<sub>i</sub> = Pr{Y ≤ i}. <br/></p><p>" +
						"Каждому из возможных исходов (таковых будет (n + 1)<sup>2</sup> = 16, т. к. n = 3) " +
						"поставим в соответствие ценность исхода: <br/>" +
						"V(y, 0) = 0;<br/>V(0, 1) = -" + parms.batteriesCost + ";<br/>" +
						"V(y ≥ 1, 1) = " + (parms.batteriesPrice - parms.batteriesCost) + ";" +
						"<br/>V(0, 2) = -" + 2 * parms.batteriesCost + ";<br/>" +
						"V(1, 2) = " + (parms.batteriesPrice - 2 * parms.batteriesCost) + ";<br/>" +
						"V(y ≥ 2, 2) = " + (2 * parms.batteriesPrice - 2 * parms.batteriesCost) + ";<br/>" +
						"V(0, 3) = -" + (3 * parms.batteriesCost) + ";<br/>" +
						"V(1, 3) = " + (parms.batteriesPrice - 3 * parms.batteriesCost) + ";<br/>" +
						"V(2, 3) = " + (2 * parms.batteriesPrice - 3 * parms.batteriesCost) + ";<br/>" +
						"V(3, 3) = " + (3 * parms.batteriesPrice - 3 * parms.batteriesCost) + ".<br/></p><p>" +
						"Полезность.<br/>" +
						"Полезность решений рассчитывается как:<br/>" +
						"u(d<sub>0</sub>) = E<sub>Y</sub>[V(y, 0)] = 0;<br/>" +
						"u(d<sub>1</sub>) = E<sub>Y</sub>[V(y, 1)] = (" + parms.batteriesPrice + " " +
						"- " + parms.batteriesCost + ") - " +
						parms.p0 + " * " + parms.batteriesPrice + " = " + u1 + ";<br/>" +
						"u(d<sub>2</sub>) = E<sub>Y</sub>[V(y, 2)] = 2(" + parms.batteriesPrice + " - " +
						parms.batteriesCost + ") - 2 * " + parms.p0 + " * " + parms.batteriesPrice + " - " +
						+parms.p1 + " * " + parms.batteriesPrice + " = " + u2 + ";<br/>" +
						"u(d<sub>3</sub>) = E<sub>Y</sub>[V(y, 3)] = 3(" + parms.batteriesPrice + " - " + parms.batteriesCost + ") - " +
						"3 * " + parms.p0 + " * " + parms.batteriesPrice + " - " +
						"2 * " + parms.p1 + " * " + parms.batteriesPrice + " - " + parms.p2 + " * " + parms.batteriesPrice + " = " +
						u3 + ";<br/><br/>\n" +
						"Выбор решения.<br/>Байесовский подход в данной задаче предполагает, что лучшее решение максимизирует полезность:<br/>" +
						"d<sup>*</sup> = argmax E<sub>Y</sub>[V(y, d)] = argmax u(d), d &#8712 D.<br/><br/>" +
						"Таким образом, Байесовское решение данной задачи - " + solutionText + "</p>" +
						"<p>Дерево решений для данной задачи будет иметь следующий вид:<br/><br/></p>  \n" +
						"</html>";

		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.add(new JLabel(theoryText));
		panel.add(new SolutionTree(parms));
		panel.add(new JLabel(Common.DIAGRAM_TEXT));
		URL resource = ClassLoader.getSystemClassLoader().getResource(
				Common.RESOURCE_PATH + Common.DIAGRAM_FILE
		);
		panel.add(new JLabel(new ImageIcon(resource)));
	}

	/**
	 * Предоставляет обработчик события нажатия кнопки, выполняющий сохранение
	 * исходных данных в файл.
	 *
	 * @return ActionListener
	 * @throws IllegalArgumentException
	 */
	@Override
	public ActionListener getPressSaveListener() throws IllegalArgumentException {
		return new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				JFileChooser saveDialog = new JFileChooser();
				saveDialog.showSaveDialog(null);
				File file = saveDialog.getSelectedFile();
				if (file == null) return;
				PrintWriter out = null;
				try {
					out = new PrintWriter(file);
					out.println(costInputText.getText());
					out.println(priceInputText.getText());
					out.println(prob0InputText.getText());
					out.println(prob1InputText.getText());
					out.println(prob2InputText.getText());
					out.println(prob3InputText.getText());
					out.close();
				} catch (FileNotFoundException ex) {
				} finally {
					if (out != null) out.close();
				}
			}
		};
	}

	/**
	 * Предоставляет обработчик события нажатия кнопки, выполняющий загрузку
	 * исходных данных из файла.
	 *
	 * @return ActionListener
	 * @throws IllegalArgumentException
	 */
	@Override
	public ActionListener getPressLoadListener() throws IllegalArgumentException {
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser openDialog = new JFileChooser();
				openDialog.showOpenDialog(null);
				File file = openDialog.getSelectedFile();
				if (file == null) return;
				Scanner sc = null;
				try {
					sc = new Scanner(file);
					costInputText.setText(sc.nextLine());
					priceInputText.setText(sc.nextLine());
					prob0InputText.setText(sc.nextLine());
					prob1InputText.setText(sc.nextLine());
					prob2InputText.setText(sc.nextLine());
					prob3InputText.setText(sc.nextLine());
				} catch (FileNotFoundException ex) {
					if (sc != null) sc.close();
					setDefaultInputValues();
				}
			}
		};
	}

	/**
	 * Предоставляет обработчик события нажатия кнопки, установку в текстовых
	 * плях ввода параметров задачи значений по умалчанию.
	 *
	 * @return ActionListener
	 * @throws IllegalArgumentException
	 */
	@Override
	public ActionListener getDefaultValuesListener() throws IllegalArgumentException {
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setDefaultInputValues();
			}
		};
	}

}