package ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.algorithm.conflictsolver;

import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.Challenger;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.ListenerHandler;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.listener.IStepLogger;

import java.util.Random;

public class RandomConflictSolver extends ConflictSolver {
    private ListenerHandler<IStepLogger> listener_handler_;

    @Override
    final public Challenger chose (Challenger challanger_a, Challenger challanger_b) {
        Random random = new Random();
        if (random.nextDouble() >= 0.5) {
            logAction("Выбираем первое решение.");
            return challanger_a;
        } else {
            logAction("Выбираем второе решение.");
            return challanger_b;
        }
    }

    public String getTypeString(){
        return "Произвольный";
    }

}

