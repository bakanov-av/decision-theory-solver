package ru.spb.beavers.modules.Task_3_1_2;

import com.sun.javafx.beans.annotations.NonNull;
import ru.spb.beavers.modules.Task_3_1_2.TaskCommon;
import ru.spb.beavers.modules.Task_3_1_1.attachCapable;
import ru.spb.beavers.modules.Task_3_1_1.creatComponentAsist;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Никита on 12.04.15.
 */
public class Solver implements attachCapable {

    private JPanel pane = new JPanel();

    private short SolutionType = 1;
    private int PlaceNum = 0; /** количество мест M **/
    private int PriceVf = 0; /** цена 1 **/
    private int PriceVd = 0; /** цена 2 **/
    private double VerP = 0.5;

    public Solver()
    {
        pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
        pane.setBackground(Color.white);
    }

    class ResolveAlgorithm
    {
        double dStar = 0;
       // double usefulValue = 0;

        public ResolveAlgorithm(){}

        public double resolveD()
        {
            dStar = ((double)PriceVd/(double)PriceVf)*PlaceNum; //(2*PlaceNum + Math.sqrt(4*(Math.pow(PlaceNum,2)) - 8*(1- PriceVd/PriceVf)))/2;
            return dStar;
        }

        public double decision(short type)
        {
           if(type==2)
           {
               creatComponentAsist.setPicture(pane, TaskCommon.PIC_RESOLVE_2);
               JLabel textLabl2 = new JLabel("<html><h2>   Вероятность запроса на билет со скидкой: "+VerP+"</h2></html>");
               textLabl2.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
               pane.add(textLabl2);
               String formulaSymb = "P*Vf >  Vd";
               String formulaSymb2 = "P*Vf <  Vd";
               String formulaNum = "p*Vf = "+VerP+"*"+PriceVf+"="+VerP*PriceVf+" > "+PriceVd;
               String formulaNum2 = "p*Vf = "+VerP+"*"+PriceVf+"="+VerP*PriceVf+" < "+PriceVd;
               if(VerP*PriceVf > PriceVd)
               {
                   JLabel formLabel = creatComponentAsist.formula(formulaSymb+" : "+formulaNum);
                   formLabel.setAlignmentX(JComponent.CENTER_ALIGNMENT);
                   pane.add(formLabel);
                   JLabel textLabl= new JLabel("<html><h2>Рекомендуется продать билет по полной цене</h2></html>");
                   textLabl.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
                   pane.add(textLabl);

               }
               else
               {
                   JLabel formLabel = creatComponentAsist.formula(formulaSymb2+" : "+formulaNum2);
                   formLabel.setAlignmentX(JComponent.CENTER_ALIGNMENT);
                   pane.add(formLabel);
                   JLabel textLabl= new JLabel("<html><h2>Рекомендуется продать билет по скидке</h2></html>");
                   textLabl.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
                   pane.add(textLabl);
               }

           }
           if(type==1)
           {
               creatComponentAsist.setPicture(pane, TaskCommon.PIC_RESOLVE_1);
               String formulaSymb = "d^{*}= Px(d^{*})*M"; //"do=\\frac{2M + \\sqrt {4M^2-8(1- \\frac{Vd}{Vf})}}{2}";
               String formulaNum = "\\frac{"+PriceVd+"}{"+PriceVf+"}*"+PlaceNum+"";  //"\\frac{2*"+PlaceNum + " \\sqrt {4*"+PlaceNum + "^2-8*(1- \\frac{"+PriceVd+"}{"+PriceVf+"})}}{2}";

               JLabel textLabl1 = new JLabel("<html><h2>Расчет d*:</h2></html>");
               textLabl1.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
               pane.add(textLabl1);


               int D = (int)resolveD();
               JLabel formLabel = creatComponentAsist.formula(formulaSymb+"="+formulaNum+"="+ D);
               formLabel.setAlignmentX(JComponent.CENTER_ALIGNMENT);
               pane.add(formLabel);

               JLabel textLabl2 = new JLabel("<html><h2>Количество билетов по скидке: "+D+"</h2></html>");
               textLabl2.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
               pane.add(textLabl2);

               JLabel textLabl3 = new JLabel("<html><h2> Количество билетов без скидки: "+(PlaceNum-D)+"</h2></html>");
               textLabl3.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
               pane.add(textLabl3);

           }
           return  0;
        }



    }
    public void initTerms(@NonNull short SolutType, @NonNull int placeN, @NonNull int Vf, @NonNull int Vd, @NonNull double Ver )
    {
        SolutionType = SolutType;
        PlaceNum = placeN;
        PriceVf = Vf;
        PriceVd = Vd;
        VerP = Ver;
    }

    public void resolve()
    {
        pane.removeAll();
        JLabel Title = new JLabel("<html><h2>Пример решения</h2></html>");
        pane.add(Title);
        if(SolutionType==2)
        {

            ResolveAlgorithm resAlg = new ResolveAlgorithm();
            resAlg.decision(SolutionType);

        }
        else
        {
            ResolveAlgorithm resAlg = new ResolveAlgorithm();
            resAlg.decision(SolutionType);
        }


    }

    @Override
    public void attachToExPanel(@NonNull JPanel exPanel)
    {
        exPanel.removeAll();
        exPanel.add(pane);
    }
}
