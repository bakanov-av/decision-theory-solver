package ru.spb.beavers.modules.half_order.graph.matrix.operations;

import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.impl.SparseDoubleMatrix2D;

/**
 * Created by Владимир on 12.04.2015.
 */
public class MatrixOrepations {
    public static DoubleMatrix2D matrixToInverseMatrix(DoubleMatrix2D matrix){
        DoubleMatrix2D suppMatrix = new SparseDoubleMatrix2D(matrix.columns(),  matrix.rows());
        for (int i = 0; i < matrix.rows(); i++) {
            for (int j = 0; j < matrix.columns(); j++) {
                double k1 = matrix.getQuick(i,j);
                double k2 = matrix.getQuick(j,i);
                suppMatrix.setQuick(i, j, k2);
                suppMatrix.setQuick(j,i, k1);

            }
        }
        return suppMatrix;
    }
    public static DoubleMatrix2D matrixToSuppMatrix(DoubleMatrix2D matrix){
        DoubleMatrix2D suppMatrix = new SparseDoubleMatrix2D(matrix.columns(),  matrix.rows());
        for (int i = 0; i < matrix.rows(); i++) {
            for (int j = 0; j < matrix.columns(); j++) {
                double k = matrix.getQuick(i,j);
                if(k == 0){
                    suppMatrix.setQuick(i, j, 1.0);
                }else{
                    suppMatrix.setQuick(i,j, 0.0);
                }
            }
        }
        return suppMatrix;
    }

    public static DoubleMatrix2D matrixToIndifferenceMatrix(DoubleMatrix2D matrix){
        matrix = matrixToSuppMatrix(matrix);
        for (int i = 0; i < matrix.rows(); i++) {
            for (int j = 0; j < matrix.columns(); j++) {
                if(matrix.getQuick(i,j) != matrix.getQuick(j,i)){
                    matrix.setQuick(i,j, 0);
                    matrix.setQuick(j,i, 0);
                }
            }
        }
        for (int i = 0; i < matrix.columns(); i++) {
            matrix.setQuick(i,i, 1.0);
        }
        return matrix;
    }
}
