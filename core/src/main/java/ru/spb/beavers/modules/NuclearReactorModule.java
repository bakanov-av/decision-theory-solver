package ru.spb.beavers.modules;

import edu.uci.ics.jung.algorithms.layout.Layout;
import ru.spb.beavers.modules.nuclear_reactor.Br4Constants;
import ru.spb.beavers.modules.nuclear_reactor.Br4Multigraph;
import ru.spb.beavers.modules.nuclear_reactor.NuclearTaskTable;

import javax.swing.*;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.StyleSheet;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Bazankov Aleksei and Leontiev Ury on 2.04.2015.
 * This is class represented module of task named
 * "Nuclear Reactor Problem with one criterion"
 * Discipline: "Decision Theory Solver"
 */
public class NuclearReactorModule implements ITaskModule {

    Br4Multigraph multigraph;
    JTable inTable;

    public NuclearReactorModule() {
        multigraph = new Br4Multigraph();
    }

    private String fullDescription = "Полное описание задачи";

    @Override
    public String getTitle() {
        return "1.5.1 Задача о ядерном\nреакторе с одним критерием";
    }

    @Override
    public void initDescriptionPanel(JPanel panel) {
        StyleSheet styleSheet = new StyleSheet();
        JTextArea jTextArea = new JTextArea(new HTMLDocument(styleSheet));
        jTextArea.setText(Br4Constants.shortDescription);
        jTextArea.setWrapStyleWord(true);
        jTextArea.setLineWrap(true);
        jTextArea.setEditable(false);
        panel.setLayout(new GridLayout());
        panel.add(jTextArea);
    }

    @Override
    public void initSolutionPanel(JPanel panel) {
//        Layout<Br4Multigraph.Vertex, Br4Multigraph.Edge> layout = new FRLayout2<>(multigraph.init(), new Dimension(300, 300));
//        BasicVisualizationServer<Br4Multigraph.Vertex, Br4Multigraph.Edge> vv = new BasicVisualizationServer<>(layout);
//        vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller<Br4Multigraph.Vertex>() {
//            @Override
//            public String transform(Br4Multigraph.Vertex vertex) {
//                return Integer.toString(vertex.step);
//            }
//        });
//        vv.getRenderContext().setEdgeLabelTransformer(new ToStringLabeller<Br4Multigraph.Edge>() {
//            @Override
//            public String transform(Br4Multigraph.Edge edge) {
//                return edge.description;
//            }
//        });
//
//        int width = (int) vv.getSize().getWidth(), height = (int) vv.getSize().getHeight();
//
//        int first_column = 0;
//        int second_column = (int) (1f / 8f * width);
//        int third_column = (int) (2f / 8f * width);
//        int fouth_column = (int) (3f / 8f * width);
//        int fifth_column = width;
//
//        setPosition(layout, Br4Multigraph.v1, new Point(first_column, (int) (height * 4f / 11f)));
//        setPosition(layout, Br4Multigraph.v2, new Point(second_column, (int) (height * 6.5f / 11f)));
//
//        setPosition(layout, Br4Multigraph.v13, new Point(third_column, (int) (height * 0.5f / 11f)));
//        setPosition(layout, Br4Multigraph.v23p, new Point(third_column, (int) (height * 3.5f / 11f)));
//        setPosition(layout, Br4Multigraph.v23n, new Point(third_column, (int) (height * 8.5 / 11f)));
//
//        setPosition(layout, Br4Multigraph.v34, new Point(fouth_column, (int) (height * 0.5f / 11f)));
//        setPosition(layout, Br4Multigraph.v34p, new Point(fouth_column, (int) (height * 2.5f / 11f)));
//        setPosition(layout, Br4Multigraph.v35p, new Point(fouth_column, (int) (height * 5f / 11f)));
//        setPosition(layout, Br4Multigraph.v34n, new Point(fouth_column, (int) (height * 7.5f / 11f)));
//        setPosition(layout, Br4Multigraph.v35n, new Point(fouth_column, (int) (height * 10f / 11f)));
//
//        setPosition(layout, Br4Multigraph.v46s, new Point(fifth_column, (int) (height * 0f / 11f)));
//        setPosition(layout, Br4Multigraph.v46f, new Point(fifth_column, (int) (height * 1f / 11f)));
//        setPosition(layout, Br4Multigraph.v46ps, new Point(fifth_column, (int) (height * 2f / 11f)));
//        setPosition(layout, Br4Multigraph.v46pf, new Point(fifth_column, (int) (height * 3f / 11f)));
//        setPosition(layout, Br4Multigraph.v56ps, new Point(fifth_column, (int) (height * 4f / 11f)));
//        setPosition(layout, Br4Multigraph.v56pm, new Point(fifth_column, (int) (height * 5f / 11f)));
//        setPosition(layout, Br4Multigraph.v56pl, new Point(fifth_column, (int) (height * 6f / 11f)));
//        setPosition(layout, Br4Multigraph.v46ns, new Point(fifth_column, (int) (height * 7f / 11f)));
//        setPosition(layout, Br4Multigraph.v46nf, new Point(fifth_column, (int) (height * 8f / 11f)));
//        setPosition(layout, Br4Multigraph.v56ns, new Point(fifth_column, (int) (height * 9f / 11f)));
//        setPosition(layout, Br4Multigraph.v56nm, new Point(fifth_column, (int) (height * 10f / 11f)));
//        setPosition(layout, Br4Multigraph.v56nl, new Point(fifth_column, (int) (height * 11f / 11f)));
//
//        vv.getRenderer().getVertexLabelRenderer().setPosition(Renderer.VertexLabel.Position.CNTR);
//        panel.add(vv);
//        JLabel jLabel = new JLabel();
//        jLabel.setText(Br4Constants.formalDescription);
//        panel.add(jLabel);
        ImageIcon reactorDescription = new ImageIcon(NuclearReactorModule.class.getResource("nuclear_reactor/reactor.jpg"));
        JLabel reactorOne = new JLabel();
        reactorOne.setSize(660, 3045);
        reactorOne.setIcon(new ImageIcon(reactorDescription.getImage().getScaledInstance(660, 3045, reactorDescription.getImage().SCALE_DEFAULT)));
        JScrollPane scrollPane = new JScrollPane(
                reactorOne,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
        );

        panel.add(scrollPane);
        panel.add(reactorOne);
    }

    private void setPosition(Layout<Br4Multigraph.Vertex, Br4Multigraph.Edge> layout,
                             Br4Multigraph.Vertex v2,
                             Point location) {
        layout.setLocation(v2, location);
        layout.lock(v2, true);
    }


    private JPanel panel;

    public void initInputPanel(JPanel panel) {
        this.panel = panel;
        String[] columnNames = {"Узел", "Следующий узел", "Вероятность ветви", "Стоимость ветви"};
        //Массив ячеек таблицы
        Object[][] dataTable1 = {
                {Br4Multigraph.v1.description, Br4Multigraph.e13.description, "-", "0"},
                {Br4Multigraph.v1.description, Br4Multigraph.e12.description, "-", "-1"},
                {Br4Multigraph.v2.description, Br4Multigraph.e23p.description, "0.6", "-"},
                {Br4Multigraph.v2.description, Br4Multigraph.e23n.description, "0.4", "-"},
                {Br4Multigraph.v13.description, Br4Multigraph.e34.description, "-", "-4"},
                {Br4Multigraph.v23p.description, Br4Multigraph.e34p.description, "-", "-2"},
                {Br4Multigraph.v23p.description, Br4Multigraph.e35p.description, "-", "-4"},
                {Br4Multigraph.v23n.description, Br4Multigraph.e34n.description, "-", "-2"},
                {Br4Multigraph.v23n.description, Br4Multigraph.e35n.description, "-", "-2"},
                {Br4Multigraph.v34.description, Br4Multigraph.e46s.description, "0.98", "10"},
                {Br4Multigraph.v34.description, Br4Multigraph.e46f.description, "0.02", "-2"},
                {Br4Multigraph.v34p.description, Br4Multigraph.e46ps.description, "0.98", "10"},
                {Br4Multigraph.v34p.description, Br4Multigraph.e46pf.description, "0.02", "-2"},
                {Br4Multigraph.v34n.description, Br4Multigraph.e46ns.description, "0.98", "10"},
                {Br4Multigraph.v34n.description, Br4Multigraph.e46nf.description, "0.02", "-2"},
                {Br4Multigraph.v35p.description, Br4Multigraph.e56ps.description, "0.9", "16"},
                {Br4Multigraph.v35p.description, Br4Multigraph.e56pm.description, "0.06", "-2"},
                {Br4Multigraph.v35p.description, Br4Multigraph.e56pl.description, "0.04", "-6"},
                {Br4Multigraph.v35n.description, Br4Multigraph.e56ns.description, "0.4", "16"},
                {Br4Multigraph.v35n.description, Br4Multigraph.e56nm.description, "0.46", "-2"},
                {Br4Multigraph.v35n.description, Br4Multigraph.e56nl.description, "0.14", "-6"},
        };
        //Создаём таблицу
        inTable = new JTable(dataTable1, columnNames);
        //Изменяем ширину столбцов таблицы
        //jTable1.getColumnModel().getColumn(0).setPreferredWidth(200);
        //Изменяем высоту строк таблицы
        // jTable1.setRowHeight(7,45);
        // panel.add(jTable1);

        NuclearTaskTable myModel = new NuclearTaskTable(inTable.getRowCount(), inTable.getColumnCount());
        inTable.setModel(myModel);

        myModel.setDataVector(dataTable1, columnNames);

        myModel.setCellEditable(2, 2, true);
        myModel.setCellEditable(3, 2, true);
        for (int i = 9; i <= 20; i++) {
            myModel.setCellEditable(i, 2, true);
        }
        for (int i = 0; i <= 20; i++) {
            if (i != 2 && i != 3) {
                myModel.setCellEditable(i, 3, true);
//                myModel.addTableModelListener(new TableModelListener() {
//                    @Override
//                    public void tableChanged(TableModelEvent e) {
//                        System.out.println(
//                                new StringBuilder()
//                                        .append("column: ")
//                                        .append(e.getColumn())
//                                        .append(", first row: ")
//                                        .append(e.getFirstRow())
//                                        .append(", last row: ")
//                                        .append(e.getLastRow())
//                        );
//                    }
//                });
            }
        }

        setUpContext(panel, inTable);
    }


    private boolean checkInputData() {
        boolean result = true;
        float value, value2, value3;
        for (int i = 0; i < 21; i++) {
            if (i != 0 && i != 1 && i != 4 && i != 5 && i != 6 && i != 7 && i != 8) {
                try {
                    value = Float.valueOf(inTable.getValueAt(i, 2).toString());
                    if (value < 0 || value > 1) {
                        System.err.println("Неверно указана вероятность!");
                        result = false;
                    }
                    // System.out.println(value);
                } catch (NumberFormatException e) {
                    System.err.println("Неверный формат строки!");
                    result = false;
                    inTable.setValueAt("Неверный формат строки!", i, 2);
                    return result;
                }
            }
        }

        for (int i = 0; i < 21; i++) {
            if (i != 2 && i != 3) {
                try {
                    Float f = Float.parseFloat(inTable.getValueAt(i, 3).toString());
                    // System.out.println(value);
                } catch (NumberFormatException e) {
                    System.err.println("Неверный формат строки!");
                    result = false;
                    inTable.setValueAt("Неверный формат строки!", i, 3);
                    return result;
                }
            }
        }


        value = Float.valueOf(inTable.getValueAt(2, 2).toString());
        value2 = Float.valueOf(inTable.getValueAt(3, 2).toString());
        if (value + value2 != 1) {
            inTable.setValueAt("ERORR!", 2, 2);
            inTable.setValueAt("ERORR!", 3, 2);
            result = false;
        }

        value = Float.valueOf(inTable.getValueAt(9, 2).toString());
        value2 = Float.valueOf(inTable.getValueAt(10, 2).toString());
        if (value + value2 != 1) {
            inTable.setValueAt("ERORR!", 9, 2);
            inTable.setValueAt("ERORR!", 10, 2);
            result = false;
        }

        value = Float.valueOf(inTable.getValueAt(11, 2).toString());
        value2 = Float.valueOf(inTable.getValueAt(12, 2).toString());
        if (value + value2 != 1) {
            inTable.setValueAt("ERORR!", 11, 2);
            inTable.setValueAt("ERORR!", 12, 2);
            result = false;
        }

        value = Float.valueOf(inTable.getValueAt(13, 2).toString());
        value2 = Float.valueOf(inTable.getValueAt(14, 2).toString());
        if (value + value2 != 1) {
            inTable.setValueAt("ERORR!", 13, 2);
            inTable.setValueAt("ERORR!", 14, 2);
            result = false;
        }

        value = Float.valueOf(inTable.getValueAt(15, 2).toString());
        value2 = Float.valueOf(inTable.getValueAt(16, 2).toString());
        value3 = Float.valueOf(inTable.getValueAt(17, 2).toString());
        if (value + value2 + value3 != 1) {
            inTable.setValueAt("ERORR!", 15, 2);
            inTable.setValueAt("ERORR!", 16, 2);
            inTable.setValueAt("ERORR!", 17, 2);
            result = false;
        }

        value = Float.valueOf(inTable.getValueAt(18, 2).toString());
        value2 = Float.valueOf(inTable.getValueAt(19, 2).toString());
        value3 = Float.valueOf(inTable.getValueAt(20, 2).toString());
        if (value + value2 + value3 != 1) {
            inTable.setValueAt("ERORR!", 18, 2);
            inTable.setValueAt("ERORR!", 19, 2);
            inTable.setValueAt("ERORR!", 20, 2);
            result = false;
        }
        return result;
    }


    @Override
    public void initExamplePanel(JPanel panel) {
        if (!checkInputData()) {
            JTextArea jTextArea = new JTextArea();
            jTextArea.setText("Ошибка исходных данных вернитесь назад и исправьте!");
            panel.removeAll();
            panel.add(jTextArea);
        } else {
            // ----------------- ТЕСТ ПЕРЕДОВОГО РЕАКТОРА НЕ ПРОЙДЕН --------------------------
            Br4Multigraph.e56ns.chance = Float.parseFloat((String) inTable.getValueAt(18, 2));      // вероятность успеха передового реактора
            Br4Multigraph.e56ns.cost = Integer.parseInt((String) inTable.getValueAt(18, 3));          // ожидаемый доход
            Br4Multigraph.e56nm.chance = Float.parseFloat((String) inTable.getValueAt(19, 2));      // вероятность мелкой аварии передового реактора
            Br4Multigraph.e56nm.cost = Integer.parseInt((String) inTable.getValueAt(19, 3));          // ожидаемые потери
            Br4Multigraph.e56nl.chance = Float.parseFloat((String) inTable.getValueAt(20, 2));      // вероятность крупной аварии передового реактора
            Br4Multigraph.e56nl.cost = Integer.parseInt((String) inTable.getValueAt(20, 3));          // ожидаемые потери

            Br4Multigraph.e46ns.chance = Float.parseFloat((String) inTable.getValueAt(13, 2));      // вероятность успеха обычного реактора
            Br4Multigraph.e46ns.cost = Integer.parseInt((String) inTable.getValueAt(13, 3));          // ожидаемый доход
            Br4Multigraph.e46nf.chance = Float.parseFloat((String) inTable.getValueAt(14, 2));      // вероятность аварии обычного реактора
            Br4Multigraph.e46nf.cost = Integer.parseInt((String) inTable.getValueAt(14, 3));          // ожидаемые потери

            Br4Multigraph.e34n.cost = Integer.parseInt((String) inTable.getValueAt(7, 3));        // стоимость постройки обычного реактора
            Br4Multigraph.e35n.cost = Integer.parseInt((String) inTable.getValueAt(8, 3));        // стоимость постройки передового реактора
            // ----------------- ТЕСТ ПЕРЕДОВОГО РЕАКТОРА ПРОЙДЕН ----------------------------
            Br4Multigraph.e56ps.chance = Float.parseFloat((String) inTable.getValueAt(15, 2));     // вероятность успеха передового реактора
            Br4Multigraph.e56ps.cost = Integer.parseInt((String) inTable.getValueAt(15, 3));       // ожидаемый доход
            Br4Multigraph.e56pm.chance = Float.parseFloat((String) inTable.getValueAt(16, 2));     // вероятность мелкой аварии передового реактора
            Br4Multigraph.e56pm.cost = Integer.parseInt((String) inTable.getValueAt(16, 3));       // ожидаемые потери
            Br4Multigraph.e56pl.chance = Float.parseFloat((String) inTable.getValueAt(17, 2));     // вероятность крупной аварии передового реактора
            Br4Multigraph.e56pl.cost = Integer.parseInt((String) inTable.getValueAt(17, 3));       // ожидаемые потери

            Br4Multigraph.e46ps.chance = Float.parseFloat((String) inTable.getValueAt(11, 2));     // вероятность успеха обычного реактора
            Br4Multigraph.e46ps.cost = Integer.parseInt((String) inTable.getValueAt(11, 3));       // ожидаемый доход
            Br4Multigraph.e46pf.chance = Float.parseFloat((String) inTable.getValueAt(12, 2));     // вероятность аварии обычного реактора
            Br4Multigraph.e46pf.cost = Integer.parseInt((String) inTable.getValueAt(12, 3));       // ожидаемые потери

            Br4Multigraph.e34p.cost = Integer.parseInt((String) inTable.getValueAt(5, 3));        // стоимость постройки обычного реактора
            Br4Multigraph.e35p.cost = Integer.parseInt((String) inTable.getValueAt(6, 3));        // стоимость постройки передового реактора
            // ----------------- НЕ ПРОВОДИТЬ ПРОВЕРКУ ----------------------------------------
            Br4Multigraph.e46s.chance = Float.parseFloat((String) inTable.getValueAt(9, 2));      // вероятность успеха обычного реактора
            Br4Multigraph.e46s.cost = Integer.parseInt((String) inTable.getValueAt(9, 3));        // ожидаемый доход
            Br4Multigraph.e46f.chance = Float.parseFloat((String) inTable.getValueAt(10, 2));      // вероятность аварии обычного реактора
            Br4Multigraph.e46f.cost = Integer.parseInt((String) inTable.getValueAt(10, 3));        // ожидаемые потери
            Br4Multigraph.e34.cost = Integer.parseInt((String) inTable.getValueAt(4, 3));         // стоимость постройки обычного реактора
            // ----------------- РЕЗУЛЬТАТЫ ПРОВЕРКИ ------------------------------------------
            Br4Multigraph.e23p.chance = Float.parseFloat((String) inTable.getValueAt(2, 2));      // вероятность успешного прохождения теста
            Br4Multigraph.e23n.chance = Float.parseFloat((String) inTable.getValueAt(3, 2));      // вероятность провального прохождения теста
            // -----------------
            Br4Multigraph.e13.cost = Integer.parseInt((String) inTable.getValueAt(0, 3));         // стоимость непрохождения проверки
            Br4Multigraph.e12.cost = Integer.parseInt((String) inTable.getValueAt(1, 3));         // стоимость прохождения проверки

            multigraph.solve();

            String[] columnNames = {"Узел", "Следующий узел", "Ожидаемый доход в узле", "Оптимальное решение"};
            Object[][] resultsData = {
                    {"Решение о проверке", "Не проводить проверку", Br4Multigraph.v1.value, Br4Multigraph.node1Decision},
                    {"Решение о проверке", "Проводить проверку", "-//-", "-//-"},
                    {"Результаты проверки", "Вероятность успеха", Br4Multigraph.v2.value, " "},
                    {"Результаты проверки", "Вероятность аварии", "-//-", " "},
                    {"Какой реактор (без проверки)", "Строить обычный реактор", Br4Multigraph.v13.value, "ОБЫЧНЫЙ"},
                    {"Какой реактор (рез.проверки - успех)", "Строить обычный реактор", Br4Multigraph.v23n.value, Br4Multigraph.node3pDecision},
                    {"Какой реактор (рез.проверки - успех)", "Строить передовой реактор", "-//-", "-//-"},
                    {"Какой реактор (рез.проверки - авария)", "Строить обычный реактор", Br4Multigraph.v23p.value, Br4Multigraph.node3nDecision},
                    {"Какой реактор (рез.проверки - авария)", "Строить передовой реактор", "-//-", "-//-"},
                    {"Обычный (без проверки)", "Вероятность успеха", Br4Multigraph.v34.value, " "},
                    {"Обычный (без проверки)", "Вероятность аварии", "-//-", " "},
                    {"Обычный (проверка-успех)", "Вероятность успеха", Br4Multigraph.v34p.value, " "},
                    {"Обычный (проверка-успех)", "Вероятность аварии", "-//-", " "},
                    {"Обычный (проверка-авария)", "Вероятность успеха", Br4Multigraph.v34n.value, " "},
                    {"Обычный (проверка-авария)", "Вероятность аварии", "-//-", " "},
                    {"Передовой (проверка-успех)", "Вероятность успеха", Br4Multigraph.v35p.value, " "},
                    {"Передовой (проверка-успех)", "Вероятность небольшой аварии", "-//-", " "},
                    {"Передовой (проверка-успех)", "Вероятность крупной аварии", "-//-", " "},
                    {"Передовой (проверка-авария)", "Вероятность успеха", Br4Multigraph.v35n.value, " "},
                    {"Передовой (проверка-авария)", "Вероятность небольшой аварии", "-//-", " "},
                    {"Передовой (проверка-авария)", "Вероятност крупной аварии", "-//-", " "}
            };

            JTable resultTable = new JTable(resultsData, columnNames);
            resultTable.setEnabled(false);
            setUpContext(panel, resultTable);
        }
    }

    @Override
    public ActionListener getPressSaveListener() throws IllegalArgumentException {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        };
    }

    @Override
    public ActionListener getPressLoadListener() throws IllegalArgumentException {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        };
    }

    @Override
    public ActionListener getDefaultValuesListener() throws IllegalArgumentException {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String[] columnNames = {"Узел", "Следующий узел", "Вероятность ветви", "Стоимость ветви"};
                Object[][] dataTable1 = {
                        {Br4Multigraph.v1.description, Br4Multigraph.e13.description, "-", "0"},
                        {Br4Multigraph.v1.description, Br4Multigraph.e12.description, "-", "-1"},
                        {Br4Multigraph.v2.description, Br4Multigraph.e23p.description, "0.6", "-"},
                        {Br4Multigraph.v2.description, Br4Multigraph.e23n.description, "0.4", "-"},
                        {Br4Multigraph.v13.description, Br4Multigraph.e34.description, "-", "-4"},
                        {Br4Multigraph.v23p.description, Br4Multigraph.e34p.description, "-", "-2"},
                        {Br4Multigraph.v23p.description, Br4Multigraph.e35p.description, "-", "-4"},
                        {Br4Multigraph.v23n.description, Br4Multigraph.e34n.description, "-", "-2"},
                        {Br4Multigraph.v23n.description, Br4Multigraph.e35n.description, "-", "-2"},
                        {Br4Multigraph.v34.description, Br4Multigraph.e46s.description, "0.98", "10"},
                        {Br4Multigraph.v34.description, Br4Multigraph.e46f.description, "0.02", "-2"},
                        {Br4Multigraph.v34p.description, Br4Multigraph.e46ps.description, "0.98", "10"},
                        {Br4Multigraph.v34p.description, Br4Multigraph.e46pf.description, "0.02", "-2"},
                        {Br4Multigraph.v34n.description, Br4Multigraph.e46ns.description, "0.98", "10"},
                        {Br4Multigraph.v34n.description, Br4Multigraph.e46nf.description, "0.02", "-2"},
                        {Br4Multigraph.v35p.description, Br4Multigraph.e56ps.description, "0.9", "16"},
                        {Br4Multigraph.v35p.description, Br4Multigraph.e56pm.description, "0.06", "-2"},
                        {Br4Multigraph.v35p.description, Br4Multigraph.e56pl.description, "0.04", "-6"},
                        {Br4Multigraph.v35n.description, Br4Multigraph.e56ns.description, "0.4", "16"},
                        {Br4Multigraph.v35n.description, Br4Multigraph.e56nm.description, "0.46", "-2"},
                        {Br4Multigraph.v35n.description, Br4Multigraph.e56nl.description, "0.14", "-6"},
                };
                inTable = new JTable(dataTable1, columnNames);
                inTable = new JTable(dataTable1, columnNames);
                setUpContext(panel, inTable);
                panel.updateUI();
            }
        };
    }

    // *****************************************************************************************
    // private block

    private void setUpContext(JPanel workspace, Component context) {
        assert workspace != null;
        assert context != null;

        workspace.removeAll();
        workspace.setLayout(new GridLayout(1, 1));

        JScrollPane scrollPane = new JScrollPane(
                context,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
        );
        workspace.add(scrollPane);
    }


}