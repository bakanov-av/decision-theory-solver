package ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui.rabsourse;

/**
 * Created by Sasha on 09.04.2015.
 */
public class VertexS {

    public String name;
    public String shape;

    public VertexS(String name, String shape)
    {
        this.name = name;
        this.shape = shape;
    }
}
