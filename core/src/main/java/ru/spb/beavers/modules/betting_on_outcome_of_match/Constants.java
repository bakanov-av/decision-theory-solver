package ru.spb.beavers.modules.betting_on_outcome_of_match;

import java.awt.Dimension;

public class Constants {
    public static final Dimension TEXT_FIELD_SIZE = new Dimension(800, 30);
    public static final Dimension CHART_SIZE = new Dimension(800, 300);
    public static final String TITLE = "1.4.2 Пари на исход матча";
    public static final String RESOURCE_PATH = "./ru/spb/beavers/modules/betting_on_outcome_of_match/";
    public static final String DESCRIPTION_FILE = "description.res";
    public static final String EXAPMLE_WITHOUT_ADD_INFO = "example_without_add_info.res";
    public static final String EXAPMLE_WITH_ADD_INFO = "example_with_add_info.res";
    public static final String SOLUTION_FILE_PART1 = "solution_part1.res";
    public static final String SOLUTION_FILE_PART2 = "solution_part2.res";
    public static final String SOLUTION_FILE_PART3 = "solution_part3.res";
    public static final String DIAGRAM_WITHOUT_ADD_INFO = "diagram_without_add_info.png";
    public static final String DIAGRAM_WITH_ADD_INFO = "diagram_with_add_info.png";
    public static final String TREE_WITHOUT_ADD_INFO = "tree_without_add_info.png";
    public static final String TREE_WITH_ADD_INFO = "tree_with_add_info.png";
    public static final float  CHART_STEP = 0.001f;
    
    public static final String INPUT_PANEL_TITLE = 
            "<html><h2 align='center'>Введите исходные данные</h2></html>";  
	
    public static final String DIAGRAM_TEXT = 
            "<html><br/><b>Диаграмма влияния:</b><br/><br/></html>";
    
    public static final String HAS_ADD_INFO_STRING = 
            "Решить вариант задачи с учетом дополнительной информации";
    
    public static final String PK_INPUT_STRING = 
            "<html>Вероятность, что тренер примет решение попытаться " +
            "забить гол - p<sub>k</sub>:</html>";
    
    public static final String P11_INPUT_STRING = 
            "<html>Вероятность победы команды в случае, если тренер примет " + 
            "решение попытаться забить гол - p<sub>11</sub>:</html>";
    
    public static final String P12_INPUT_STRING = 
            "<html>Вероятность победы команды в случае, если тренер примет " + 
            "решение попытаться совершить тачдаун - p<sub>12</sub>:</html>";
    
    public static final String PARMS_ERROR_STRING = 
            "<html><h2><font color='#FF9B9B'>Исходные данные заданы не полностью" +
            " либо содержат ошибки</font></h2></html>";
}
