package ru.spb.beavers.modules.half_order.graph.matrix.operations;

import edu.uci.ics.jung.graph.DirectedSparseGraph;
import ru.spb.beavers.modules.half_order.graph.GraphContainer;

import java.util.Collection;

/**
 * Created by Владимир on 13.04.2015.
 */
public class StandardGraph {
    public static void fillStandardGraph(GraphContainer container){
        DirectedSparseGraph<String, String> graph = container.getGraph();
        Collection<String> edges = graph.getEdges();
        for (String edge : edges) {
            container.getGraph().removeEdge(edge);
        }
        Collection<String> vert = graph.getVertices();
        for (String v : vert) {
            container.getGraph().removeVertex(v);
        }
        container.getGraph().addVertex("A");
        container.getGraph().addVertex("B");
        container.getGraph().addVertex("C");
        container.getGraph().addVertex("D");
        container.getGraph().addVertex("E");
        container.getGraph().addEdge("e1", "A", "B");
        container.getGraph().addEdge("e2", "B", "C");
        container.getGraph().addEdge("e3", "C", "E");
        container.getGraph().addEdge("e4", "A", "D");
        container.getGraph().addEdge("e5", "B", "D");
        container.getGraph().addEdge("e6", "B", "E");
        container.getGraph().addEdge("e7", "D", "E");
        container.getGraph().addEdge("e8", "A", "C");
        container.getGraph().addEdge("e9", "A", "E");
    }

}
