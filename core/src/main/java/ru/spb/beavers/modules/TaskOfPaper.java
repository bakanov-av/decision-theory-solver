package ru.spb.beavers.modules;

import edu.uci.ics.jung.algorithms.layout.TreeLayout;
import edu.uci.ics.jung.graph.*;
import edu.uci.ics.jung.visualization.*;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.renderers.DefaultVertexLabelRenderer;
import edu.uci.ics.jung.visualization.renderers.Renderer;
import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.traces.Trace2DSimple;
import info.monitorenter.gui.chart.views.ChartPanel;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.functors.ConstantTransformer;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.*;



public class TaskOfPaper implements ITaskModule {

    private class SaveListener implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
            //System.out.println("SaveListener performed");

            //FileNameExtensionFilter filter = new FileNameExtensionFilter("*.*");
            JFileChooser fc = new JFileChooser();
            //fc.setFileFilter(filter);
            if ( fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION ) {
                try ( FileWriter fw = new FileWriter(fc.getSelectedFile()) ) {
                    fw.write(toFile());
                    fw.close();
                }
                catch ( IOException e1 ) {
                    System.out.println("Error of save\n" + e1.toString());
                }
            }

        }

        private String toFile() {
            String s = vcField.getText();
            s += "\n";
            s += vsField.getText();
            s += "\n";
            s += mField.getText();
            s += "\n";
            s += pField.getText();

            return s;
        }
    }

    private class LoadListener implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
            //System.out.println("LoadListener performed");

            //FileNameExtensionFilter filter = new FileNameExtensionFilter("*.txt", "*.*");
            JFileChooser fc = new JFileChooser();
            //fc.setFileFilter(filter);
            if ( fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION ) {
                try ( FileReader fr = new FileReader(fc.getSelectedFile()))  {
                    BufferedReader br = new BufferedReader(fr);
                    vcField.setText(br.readLine());
                    vsField.setText(br.readLine());
                    mField.setText(br.readLine());
                    pField.setText(br.readLine());
                    fr.close();
                }
                catch ( IOException e1 ) {
                    System.out.println("Error of load\n" + e1.toString());
                }
            }
        }
    }

    private class DefaultValuesListener implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
            //System.out.println("DefaultValuesListener performed");
            vcField.setText("4");
            vsField.setText("6");
            mField.setText("2");
            pField.setText("0.2,0.5,0.3");
        }
    }

    private SaveListener saveListener = null;
    private LoadListener loadListener = null;
    private DefaultValuesListener defaultValuesListener = null;

    // Формат вывода дробных чисел
    DecimalFormat myFormatter = new DecimalFormat("###.###");

    /**
     * @return Название задачи. 1 - 4 слова.
     */
    public String getTitle() {
        return "2.1.2. Задача о газетах";
    }


    /**
     * Инициализация элементов на панели, дающей краткое описание решаемой задачи.
     * @param panel Инициализируемая панель.
     */
    public void initDescriptionPanel(JPanel panel) {
        panel.setPreferredSize(new Dimension(0, 0));

        panel.setLayout(new BorderLayout());

        // Заголовок
        descriptionTextAreaName.setEditable(false);
        descriptionTextAreaName.setLineWrap(true);
        descriptionTextAreaName.setWrapStyleWord(true);
        Font font1 = new Font("Verdana", Font.BOLD, 14);
        descriptionTextAreaName.setFont(font1);
        descriptionTextAreaName.setText("\t\t           Задача о газетах");
        panel.add(descriptionTextAreaName, BorderLayout.PAGE_START);

        // Текстовое описание
        descriptionTextArea.setEditable(false);
        descriptionTextArea.setLineWrap(true);
        descriptionTextArea.setWrapStyleWord(true);
        Font font2 = new Font("Verdana", Font.LAYOUT_LEFT_TO_RIGHT, 12);
        descriptionTextArea.setFont(font2);
        descriptionTextArea.setText("\n\tНеформальная постановка задачи:\n" +
                "Продавец газет ежедневно закупает газеты по оптовой цене, а затем продает их по розничной. Не проданные в течение дня газеты полностью теряют свою стоимость. Спрос на газеты определяется различными внешними факторами и не носит регулярного характера. Доход продавца определяется количеством проданных газет и разницей между оптовой и розничной стоимостью газет. Если продавец закупает недостаточно газет и спрос на газеты превышает имеющееся количество газет то продавец не получит части дохода, которую мог бы получить, закупив большее число газет. Если продавец закупает слишком много газет и часть газет остается не проданными, то продавец теряет на каждой непроданной газете ее оптовую стоимость. Задача сводится к выбору количества газет, закупка и продажа которых принесут продавцу наибольший доход.\n");
        descriptionTextArea.append("\nВ данной задаче рассматривается только баесовский (жадный) подход, так как при осторожном подходе предприниматель будет закупать при любом раскладе нулевое количество газет.");
        panel.add(descriptionTextArea, BorderLayout.CENTER);
    }
    private JTextArea descriptionTextAreaName = new JTextArea();
    private JTextArea descriptionTextArea = new JTextArea();


    /**
     * Инициализация элементов на панели, предоставляющие подробное решение задачи.
     * Выводится полная теоритическая справка, вывод всех формул, графиков, описание входных/выходных данных задачи.
     * @param panel Инициализируемая панель.
     */
    public void initSolutionPanel(JPanel panel) {
        panel.setPreferredSize(new Dimension(800, 1951));

        BufferedImage image = null;
        try {
            image = ImageIO.read(TaskOfPaper.class.getResource("TaskOfPaper/theory.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        JLabel imageLabel = new JLabel(new ImageIcon(image/*.getScaledInstance(800, 2000, 2)*/));
        imageLabel.setSize(800, 1951);

        panel.add(imageLabel);
    }


    /**
     * Инициализация элементов панели ввода исходных данных.
     * @param panel Инициализируемая панель.
     */
    public void initInputPanel(JPanel panel) {
        panel.setPreferredSize(new Dimension(0, 0));

        GroupLayout layout = new GroupLayout(panel);
        panel.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        vcLabel.setText("Стоимость закупки: ");
        vcLabel.setEnabled(false);
        vcLabel.setEditable(false);
        vcField.setEditable(true);
        vcField.setText("4");

        vsLabel.setText("Стоимость продажи: ");
        vsLabel.setEnabled(false);
        vsLabel.setEditable(false);
        vsField.setEditable(true);
        vsField.setText("6");

        mLabel.setText("Количество газет: ");
        mLabel.setEnabled(false);
        mLabel.setEditable(false);
        mField.setEditable(true);
        mField.setText("2");

        pLabel.setText("Массив вероятностей: ");
        pLabel.setEnabled(false);
        pLabel.setEditable(false);
        pField.setEditable(true);
        pField.setText("0.2,0.5,0.3");

        inputTextArea.setEditable(false);
        inputTextArea.setLineWrap(true);
        inputTextArea.setWrapStyleWord(true);
        Font font2 = new Font("Verdana", Font.LAYOUT_LEFT_TO_RIGHT, 12);
        inputTextArea.setFont(font2);
        inputTextArea.setText("\tОписание входных данных:\n" +
                "Стоимость закупки - закупочная цена одной газеты.\n" +
                "Стоимость продажи - отпускная цена одной газеты.\n" +
                "Количество газет - максимальное количество газет, доступных для покупки предпринимателем.\n" +
                "Массив вероятностей - массив, в котором указаны вероятности покупки каждого количества газет, начиная от нуля и заканчивая указанным выше количеством газет. Сумма всех вероятностей массива должна быть равна единице.\n\n" +
                //"Тра-ля-ля, тра-ля-ля мы везем с собой кота,\n" +
                //"Чижика, собаку, кошку, забияку ...\n\n" +
                "\tОписание формата файла с исходными данными:\n" +
                "I строка: <Цена оптовой закупки одной газеты>\n" +
                "II строка: <Цена розничной продажи одной газеты>\n" +
                "III строка: <Максимальное количество закупаемых газет>\n" +
                "IV строка: <Массив вероятностей покупки каждого количества газет от 0 до максимального количества>\n\n" +
                "\tПример формата файла с исходными данными:\n" +
                "4\n" +
                "6\n" +
                "2\n" +
                "0.2,0.5,0.3\n");

        layout.setHorizontalGroup(
                layout.createParallelGroup()
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(vcLabel)
                                .addComponent(vcField)
                                .addComponent(vsLabel)
                                .addComponent(vsField)
                                .addComponent(mLabel)
                                .addComponent(mField))
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(pLabel)
                                .addComponent(pField))
                        .addComponent(inputTextArea)
        );
        layout.setVerticalGroup(
                layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(vcLabel)
                                .addComponent(vcField)
                                .addComponent(vsLabel)
                                .addComponent(vsField)
                                .addComponent(mLabel)
                                .addComponent(mField))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                .addComponent(pLabel)
                                .addComponent(pField))
                        .addComponent(inputTextArea)
        );
    }
    private JTextField vcLabel = new JTextField();
    private JTextField vcField = new JTextField(10);
    private JTextField vsLabel = new JTextField();
    private JTextField vsField = new JTextField(10);
    private JTextField mLabel = new JTextField();
    private JTextField mField = new JTextField(10);
    private JTextField pLabel = new JTextField();
    private JTextField pField = new JTextField(55);
    private JTextArea inputTextArea = new JTextArea(20, 70);


    /**
     * Инициализация элементов панели оторбражающей решение задачи с введенными исходными данными.
     * @param panel Инициализируемая панель.
     */
    public void initExamplePanel(JPanel panel) {
        panel.setPreferredSize(new Dimension(800, 1300));

        panel.setLayout(null);

        outputTextArea.setEditable(false);
        outputTextArea.setText("");

        // Введем входные параметры
        try {
            Vc = Integer.parseInt(vcField.getText());
            Vs = Integer.parseInt(vsField.getText());
            m = Integer.parseInt(mField.getText());
        }
        catch (Exception e) {
            outputTextArea.append("\tВведены некоректные значения!!!\n");
            return;
        }

        //*********************************
        // Фокусы разработчиков
        // Для соответсвия количества газет (указали 5 газет, а получаем 4)
        m++;
        //*********************************

        p = new double [m];
        // Считывание введенных данных
        String[] items = pField.getText().split(",");
        // Проверка размера
        if (m != items.length) {
            outputTextArea.append("\tВведены некоректные значения!!!\n");
            return;
        }
        for (int i = 0; i < items.length; i++) {
            try {
                p[i] = Double.parseDouble(items[i]);
                if (p[i] < 0) {
                    outputTextArea.append("\tВведены некоректные значения!!!\n");
                    return;
                }
            }
            catch (NumberFormatException nfe) {
                outputTextArea.append("\tВведены некоректные значения!!!\n");
                return;
            }
        }
        // Проверка на отрицательность
        if (Vc < 0 || Vs < 0 || m < 0) {
            outputTextArea.append("\tВведены некоректные значения!!!\n");
            return;
        }

        // Проверка значений вероятности
        double proverkaP = 0;
        for(int i = 0; i < m; i++) {
            proverkaP += p[i];
        }
        proverkaP = Math.round((proverkaP)*100000000)/100000000;
        if (proverkaP != 1){
            outputTextArea.append("\tСумма массива вероятностей не равна 1!!!\n");
            return;
        }

        // Расчет полезностей
        U = new double [m];
        for(int i = 0; i < m; i++) {
            double Z = 0;
            for(int j = 0; j < i; j++) {
                Z += (i-j)*p[j];
            }
            U[i] = i*(Vs-Vc) - Vs*Z;
        }

        // Поиск максимального значения полезности и сохранения индекса.
        for(int i = 0; i < m; i++) {
            if(max < U[i]){
                max = U[i];
                d = i;
            }
        }

        // Расчет ценностей.
        V = new double [m][m];
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < m; j++) {
                if (j < i){
                    V[j][i] = Vs * j - Vc * i;
                }
                else{
                    V[j][i] = Vs * i - Vc * i;
                }
            }
        }

        // Исходные данные
        outputTextArea.append("\tИсходные данные:\n");
        outputTextArea.append("Стоимость закупки = " + vcField.getText() + "\n");
        outputTextArea.append("Стоимость продажи = " + vsField.getText() + "\n");
        outputTextArea.append("Количество газет = " + mField.getText() + "\n");
        outputTextArea.append("Массив вероятностей = [" + pField.getText() + "]\n");

        outputTextArea.append("\n\tРешение:\n");
        // Вывод на экран полученных значений.
        outputTextArea.append("Массив полученных полезностей:\n");
        for(int i = 0; i < m; i++) {
            outputTextArea.append(myFormatter.format(U[i]) + ";    ");
        }

        outputTextArea.append("\n\n" +
                "\tРезультат решения:\n" +
                "Максимальная полезность: " + myFormatter.format(max) + "\n");
        outputTextArea.append("Получена при покупке данного количества газет: " + d + "\n");

        /*outputTextArea.append("\n\tМассив полученных ценностей:\n");
        for(int i = 0; i < m; i++) {
            outputTextArea.append("\n");
            for(int j = 0; j < m; j++) {
                outputTextArea.append(myFormatter.format(V[j][i]) + "   ");
            }
        }*/

        outputTextArea.setSize(785, 300);
        outputTextArea.setLocation(0, 0);
        panel.add(outputTextArea);

        // Плотность вероятности
        ChartPanel plot1 = setPlot1();
        plot1.setSize(785, 300);
        plot1.setLocation(0, 300);
        panel.add(plot1);

        // Графическое решение
        ChartPanel plot = setPlot();
        plot.setSize(785, 300);
        plot.setLocation(0, 600);
        panel.add(plot);

        // Дерево решений
        GraphZoomScrollPane graph = setGraph();
        graph.setSize(700, 400);
        graph.setLocation(50, 900);
        panel.add(graph);
    }
    private JTextArea outputTextArea = new JTextArea();
    // Вход
    double Vc; // Стоимость закупки
    double Vs; // Стоимость продажи
    int m; // Количество газет для покупки
    double[] p; // Массив вероятностей

    // Выход
    int d = 0; // Оптимальное количество газет
    double max = 0; // Максимальная теоретическая выручка
    double[] U; // Массив полезностей
    double[][] V; // Массив ценностей

    private ChartPanel setPlot1 () {
        // Графическое решение
        Chart2D chart = new Chart2D();
        chart.getAxisX().getAxisTitle().setTitle("i");
        chart.getAxisY().getAxisTitle().setTitle("P(i)");
        chart.setVisible(true);

        // График функции распределения
        ITrace2D plot = new Trace2DSimple();
        plot.setName("Плотность вероятности");
        plot.setColor(new Color(0, 0, 0));
        chart.addTrace(plot);
        for(int i = 0; i < m; i++){
            plot.addPoint(i, p[i]);
        }
        // Точка (0, 0)
        ITrace2D line1 = new Trace2DSimple();
        line1.setName("");
        chart.addTrace(line1);
        line1.addPoint(0, 0);

        ChartPanel chartpanel = new ChartPanel(chart);
        chartpanel.setVisible(true);

        return chartpanel;
    }

    private ChartPanel setPlot () {
        // Графическое решение
        Chart2D chart = new Chart2D();
        chart.getAxisX().getAxisTitle().setTitle("d");
        chart.getAxisY().getAxisTitle().setTitle("1 - P(Y)");
        chart.setVisible(true);

        // График функции распределения
        ITrace2D plot = new Trace2DSimple();
        plot.setName("Графическое решение задачи");
        plot.setColor(new Color(0, 0, 0));
        chart.addTrace(plot);
        double rasp = 0;
        double point = 0;
        for(int i = 0; i < m; i++){
            rasp += p[i];
            if (i == d) point = rasp;
            plot.addPoint(i, 1.0 - rasp);
        }
        // Вспомогательные линии
        ITrace2D line1 = new Trace2DSimple();
        line1.setName("");
        line1.setColor(new Color(220, 220, 220));
        chart.addTrace(line1);
        line1.addPoint(0, 1.0 - point);
        line1.addPoint(d, 1.0 - point);
        ITrace2D line2 = new Trace2DSimple();
        line2.setName("");
        line2.setColor(new Color(220, 220, 220));
        chart.addTrace(line2);
        line2.addPoint(d, 0);
        line2.addPoint(d, 1.0 - point);

        ChartPanel chartpanel = new ChartPanel(chart);
        chartpanel.setVisible(true);

        return chartpanel;
    }

    private GraphZoomScrollPane setGraph() {
        // Дерево решений
        DelegateTree<Integer, String> g = new DelegateTree<>();
        g.setRoot(0);
        for(int i = 0; i < m; i++) {
            g.addChild(String.valueOf(i + 1), 0, i + 1);
            for (int j = 0; j < m; j++) {
                int n = (m + 1) + m * i + j;
                g.addChild(String.valueOf(n), i + 1, n);
            }
        }

        TreeLayout layout = new TreeLayout<>(g);
        VisualizationViewer/*<yournode, youredge>*/ vv = new VisualizationViewer<>(layout);

        // Подписи вершин
        vv.getRenderContext().setVertexLabelTransformer(new Transformer<Integer, String>() {
            @Override
            public String transform(Integer i) {
                if (i == 0) return "D";
                if (i >= 1 && i <= m) return "L" + Integer.toString(i-1);
                return myFormatter.format(V[(i - (m + 1)) % m][(i - (m + 1)) / m]);
            }
        });
        // Подписи ребер
        vv.getRenderContext().setEdgeLabelTransformer(new Transformer<String, String>() {
            @Override
            public String transform(String edgeName) {
                int en = Integer.parseInt(edgeName);
                if (en >= 1 && en < m + 1)
                    return "d" + Integer.toString(en - 1) + " = " + Integer.toString(en - 1) + "";
                return "x" + Integer.toString((en - (m + 1)) % m) + " = " + Integer.toString((en - (m + 1)) % m) + "";
            }
        });

        // Установка места подписи вершин
        vv.getRenderer().getVertexLabelRenderer().setPosition(Renderer.VertexLabel.Position.CNTR);

        // Игрульки с графом
        DefaultModalGraphMouse gm = new DefaultModalGraphMouse();
        gm.setMode(ModalGraphMouse.Mode.EDITING);
        vv.setGraphMouse(gm);

        // Оформление вершин
        // Transformer maps the vertex number to a vertex property
        Transformer<Integer,Paint> vertexColor = new Transformer<Integer,Paint>() {
            public Paint transform(Integer i) {
                if (i == 0) return Color.GREEN;
                if (i >= 1 && i < m +1) return Color.cyan;
                return Color.YELLOW;
            }
        };
        Transformer<Integer,Shape> vertexSize = new Transformer<Integer,Shape>(){
            public Shape transform(Integer i){
                Ellipse2D circle = new Ellipse2D.Double(-15, -15, 30, 30);
                Ellipse2D circle_big = new Ellipse2D.Double(-20, -15, 40, 30);
                Rectangle2D rectangle = new Rectangle2D.Double(-15, -15, 30, 30);
                if (i == 0) return rectangle;
                if (i >= 1 && i < m +1) return circle;
                return circle_big;
            }
        };
        vv.getRenderContext().setVertexFillPaintTransformer(vertexColor);
        vv.getRenderContext().setVertexShapeTransformer(vertexSize);

        // Поворот панели
        //vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.LAYOUT).rotate(3*Math.PI/2, vv.getCenter());

        // Цвет ребер
        vv.getRenderContext().setEdgeDrawPaintTransformer(new ConstantTransformer(Color.white));
        vv.getRenderContext().setEdgeStrokeTransformer(new ConstantTransformer(new BasicStroke(2.5f)));

        // тестирование фигни
        vv.getRenderContext().setLabelOffset(20);

        GraphZoomScrollPane scrollPane = new GraphZoomScrollPane(vv);
        return scrollPane;
    }


    /**
     * Listener может бросать IllegalArgumentException с описанием ошибки.
     * @return Слушатель, который будет вызван для сохранения набора исходных данных в файл.
     */
    public ActionListener getPressSaveListener() throws IllegalArgumentException {
        if (saveListener == null) {
            saveListener = new SaveListener();
        }
        return saveListener;
    }

    /**
     * Listener может бросать IllegalArgumentException с описанием ошибки
     * @return Слушатель, который будет вызван для загрузки набора исходных данных из файла.
     */
    public ActionListener getPressLoadListener() throws IllegalArgumentException {
        if (loadListener == null) {
            loadListener = new LoadListener();
        }
        return loadListener;
    }

    /**
     * Listener может бросать IllegalArgumentException с описанием ошибки
     * @return Слушатель, который будет вызван для установки значений по умолчанию входных параметров задачи.
     */
    public ActionListener getDefaultValuesListener() throws IllegalArgumentException {
        if (defaultValuesListener == null) {
            defaultValuesListener = new DefaultValuesListener();
        }
        return defaultValuesListener;
    }
}
