package ru.spb.beavers.modules.hotelrooms;

/**
 * Contains coordinates for graphic solution of task
 * @author AntoVita
 *
 */
public class Point {
	public double x;
	public double yVf;
	public double yVt;
	
	Point(double x, double yVf, double yVt) {
		this.x = x;
		this.yVf = yVf;
		this.yVt = yVt;
	}
}