package ru.spb.beavers.modules.hotelrooms;


/**
 * Contains input data
 * @author AntoVita
 *
 */
public class InputData {
	public double M;
	public double Vf;
	public double Vt;
	public DistributionFunction Px;
	public DistributionFunction Py;
	
	public int countOfVertices;
}
