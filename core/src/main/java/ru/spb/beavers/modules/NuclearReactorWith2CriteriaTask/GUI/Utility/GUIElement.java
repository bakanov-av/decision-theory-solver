package ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.GUI.Utility;

import edu.uci.ics.jung.algorithms.layout.StaticLayout;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.SparseMultigraph;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import org.apache.commons.collections15.Transformer;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.GUI.SolutionPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Point2D;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GUIElement {
    private static class LocationTransformer implements Transformer<VertexShape, Point2D> {
        private Map vx_coords;

        public LocationTransformer(Map vx_coords)
        {
            this.vx_coords = vx_coords;
        }

        @Override
        public Point2D transform(VertexShape vertex) {
            Point2D vx_coord = (Point2D)vx_coords.get(vertex.name);
            return new Point2D.Double((double) vx_coord.getX(), (double) vx_coord.getY());
        }
    };

    private static class EdgeLabelTransformer implements Transformer<String, String> {
        private Map edge_values;

        public EdgeLabelTransformer(Map edge_values)
        {
            this.edge_values = edge_values;
        }

        @Override
        public String transform(String edge) {
            if (edge_values != null) {
                Point2D edge_value = (Point2D)edge_values.get(edge);
                return String.valueOf(edge.charAt(0)) + " (" + edge_value.getX() + "; " + edge_value.getY() + ")";
            }
            return String.valueOf(edge.charAt(0));
        }
    };

    public static JLabel setTextHeader(String header, int oy, int width, int height, JPanel panel)
    {
        JLabel jl = new JLabel("<html><body width=\"" + width + "\" height=\"" + height
                + "\"><center><b><i>" + header + "</i></b></center></body></html>");
        jl.setFont(new Font("Arial", Font.BOLD, 15));
        jl.setSize(width, height);
        jl.setLocation(panel.getWidth() / 2 - jl.getWidth() / 2, oy);
        return jl;
    }

    public static JLabel setTextHeader(String header, int oy, int width, int height, LastElCoords last_el_coords,  JPanel panel)
    {
        JLabel jl = new JLabel("<html><body width=\"" + width + "\" height=\"" + height
                + "\"><center><b><i>" + header + "</i></b></center></body></html>");
        jl.setFont(new Font("Arial", Font.BOLD, 15));
        jl.setSize(width, height);
        jl.setLocation(panel.getWidth() / 2 - jl.getWidth() / 2, last_el_coords.y + last_el_coords.height + oy);
        last_el_coords.y = jl.getY();
        last_el_coords.height = jl.getHeight();
        return jl;
    }

    public static JScrollPane setHtmlText(String url_string, int x, int y, int width, int height, JPanel panel)
    {
        JTextPane jtp = new JTextPane();

        try {
            File dir = new File(SolutionPanel.class.getResource("./../").toURI());
            URI uri = SolutionPanel.class.getResource(url_string).toURI();
            try {
                jtp.setPage(uri.toURL());
            } catch (Exception e) {
            }

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        JScrollPane scroll = new JScrollPane(jtp);
        scroll.setSize(width, height);
        scroll.setLocation(x, y);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scroll.getVerticalScrollBar().setUnitIncrement(16);
        jtp.setFocusable(false);
        //jtp.setEditable(false);
        jtp.setBackground(new Color(240,240,240));
        return scroll;
    }

    public static void changeHtmlPage(String new_url_string, JTextPane jtp)
    {
        try {
            URI uri = SolutionPanel.class.getResource(new_url_string).toURI();
            try {
                jtp.setPage(uri.toURL());
            } catch (Exception e) {
            }

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public static JScrollPane setHtmlText600Px(String url_string, LastElCoords last_el_coords, JPanel panel)
    {
        return setHtmlText(url_string, panel.getWidth() / 2 - 300, last_el_coords.y, 600, last_el_coords.height, panel);
    }

    public static  VisualizationViewer<VertexShape, String> setDescriptionTree(JPanel panel, Map edges, LastElCoords last_el_coords, int offset_y)
    {
        int vx_r = 10;
        int delta_x = 130;
        int ox = 20;
        int oy = 40;

        Map vx_coords = new HashMap();
        vx_coords.put("1", new Point2D.Double(ox, oy+vx_r*14));
        vx_coords.put("3.1", new Point2D.Double(ox+delta_x*2, oy+vx_r*2));
        vx_coords.put("2", new Point2D.Double(ox+delta_x, oy+vx_r*28));
        vx_coords.put("3.2", new Point2D.Double(ox+delta_x*2, oy+vx_r*18));
        vx_coords.put("3.3", new Point2D.Double(ox+delta_x*2, oy+vx_r*43));
        vx_coords.put("4.1", new Point2D.Double(ox+delta_x*3, oy+vx_r*2));
        vx_coords.put("4.2", new Point2D.Double(ox+delta_x*3, oy+vx_r*12));
        vx_coords.put("5.1", new Point2D.Double(ox+delta_x*3, oy+vx_r*25));
        vx_coords.put("4.3", new Point2D.Double(ox+delta_x*3, oy+vx_r*37));
        vx_coords.put("5.2", new Point2D.Double(ox+delta_x*3, oy+vx_r*50));
        vx_coords.put("6.1", new Point2D.Double(ox+delta_x*4, oy));
        vx_coords.put("6.2", new Point2D.Double(ox+delta_x*4, oy+vx_r*5));
        vx_coords.put("6.3", new Point2D.Double(ox+delta_x*4, oy+vx_r*10));
        vx_coords.put("6.4", new Point2D.Double(ox+delta_x*4, oy+vx_r*15));
        vx_coords.put("6.5", new Point2D.Double(ox+delta_x*4, oy+vx_r*20));
        vx_coords.put("6.6", new Point2D.Double(ox+delta_x*4, oy+vx_r*25));
        vx_coords.put("6.7", new Point2D.Double(ox+delta_x*4, oy+vx_r*30));
        vx_coords.put("6.8", new Point2D.Double(ox+delta_x*4, oy+vx_r*35));
        vx_coords.put("6.9", new Point2D.Double(ox+delta_x*4, oy+vx_r*40));
        vx_coords.put("6.10", new Point2D.Double(ox+delta_x*4, oy+vx_r*45));
        vx_coords.put("6.11", new Point2D.Double(ox+delta_x*4, oy+vx_r*50));
        vx_coords.put("6.12", new Point2D.Double(ox + delta_x * 4, oy + vx_r * 55));

        ArrayList<VertexShape> vx_list = new ArrayList<VertexShape>(22);

        vx_list.add(new VertexShape("1", "Square")); // 0
        vx_list.add(new VertexShape("3.1", "Square")); // 1
        vx_list.add(new VertexShape("2", "Circle")); // 2
        vx_list.add(new VertexShape("3.2", "Square")); // 3
        vx_list.add(new VertexShape("3.3", "Square")); // 4
        vx_list.add(new VertexShape("4.1", "Circle")); // 5
        vx_list.add(new VertexShape("4.2", "Circle")); // 6
        vx_list.add(new VertexShape("5.1", "Circle")); // 7
        vx_list.add(new VertexShape("4.3", "Circle")); // 8
        vx_list.add(new VertexShape("5.2", "Circle")); // 9
        vx_list.add(new VertexShape("6.1", "Dot")); // 10
        vx_list.add(new VertexShape("6.2", "Dot")); // 11
        vx_list.add(new VertexShape("6.3", "Dot")); // 12
        vx_list.add(new VertexShape("6.4", "Dot")); // 13
        vx_list.add(new VertexShape("6.5", "Dot")); // 14
        vx_list.add(new VertexShape("6.6", "Dot")); // 15
        vx_list.add(new VertexShape("6.7", "Dot")); // 16
        vx_list.add(new VertexShape("6.8", "Dot")); // 17
        vx_list.add(new VertexShape("6.9", "Dot")); // 18
        vx_list.add(new VertexShape("6.10", "Dot")); // 19
        vx_list.add(new VertexShape("6.11", "Dot")); // 20
        vx_list.add(new VertexShape("6.12", "Dot")); // 21

        Graph<VertexShape, String> basis = new SparseMultigraph<VertexShape, String>();
        for(VertexShape vx: vx_list)
        {
            basis.addVertex(vx);
        }

        basis.addEdge("N", vx_list.get(0), vx_list.get(1));
        basis.addEdge("T", vx_list.get(0), vx_list.get(2));
        basis.addEdge("C1", vx_list.get(1), vx_list.get(5));
        basis.addEdge("s1", vx_list.get(5), vx_list.get(10));
        basis.addEdge("f1", vx_list.get(5), vx_list.get(11));
        basis.addEdge("p", vx_list.get(2), vx_list.get(3));
        basis.addEdge("C2", vx_list.get(3), vx_list.get(6));
        basis.addEdge("s2", vx_list.get(6), vx_list.get(12));
        basis.addEdge("f2", vx_list.get(6), vx_list.get(13));
        basis.addEdge("A1", vx_list.get(3), vx_list.get(7));
        basis.addEdge("s3", vx_list.get(7), vx_list.get(14));
        basis.addEdge("m1", vx_list.get(7), vx_list.get(15));
        basis.addEdge("l1", vx_list.get(7), vx_list.get(16));
        basis.addEdge("n", vx_list.get(2), vx_list.get(4));
        basis.addEdge("C3", vx_list.get(4), vx_list.get(8));
        basis.addEdge("s4", vx_list.get(8), vx_list.get(17));
        basis.addEdge("f3", vx_list.get(8), vx_list.get(18));
        basis.addEdge("A2", vx_list.get(4), vx_list.get(9));
        basis.addEdge("s5", vx_list.get(9), vx_list.get(19));
        basis.addEdge("m2", vx_list.get(9), vx_list.get(20));
        basis.addEdge("l2", vx_list.get(9), vx_list.get(21));

        StaticLayout<VertexShape, String> layout = new StaticLayout<VertexShape, String>(
                basis, new LocationTransformer(vx_coords));
        layout.setSize(new Dimension(250, 250));
        edu.uci.ics.jung.visualization.VisualizationViewer<VertexShape, String> vv = new edu.uci.ics.jung.visualization.VisualizationViewer<VertexShape, String>(layout);
        vv.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line<VertexShape, String>());
        vv.getRenderContext().setVertexLabelTransformer(new Transformer<VertexShape, String>() {
            public String transform(VertexShape vx) {
                return vx.name;
            }
        });
        vv.getRenderContext().setEdgeLabelTransformer(new EdgeLabelTransformer(edges));
        vv.getRenderer().setVertexRenderer(new DependencyDiagramRenderer());
        vv.getRenderer().getVertexLabelRenderer().setPosition(edu.uci.ics.jung.visualization.renderers.Renderer.VertexLabel.Position.N);
        vv.setSize(650, 650);
        last_el_coords.updateValues(offset_y, vv.getHeight());
        vv.setLocation(panel.getWidth() / 2 - vv.getWidth() / 2, last_el_coords.y);
        return vv;
    }
}
