package ru.spb.beavers.modules.optimization_of_binary_relation_br18;

import edu.uci.ics.jung.graph.DirectedSparseMultigraph;

import java.util.Collection;
import java.util.HashMap;

/**
 * Created by Belyaev Andrey on 11.04.2015.
 */
public class BinaryRelation {

    private HashMap<String, BinaryElement> elements = new HashMap<String, BinaryElement>();

    public BinaryRelation() {
    }

    public BinaryElement addOrGetElement(String name) {
        if (!elements.containsKey(name)) {
            BinaryElement element = new BinaryElement(name);
            elements.put(name, element);
            return element;
        } else {
            return elements.get(name);
        }
    }

    public BinaryElement addOrGetElement(BinaryElement element) {
        if (!elements.containsValue(element)) {
            elements.put(element.getName(), element);
            return element;
        } else {
            return elements.get(element.getName());
        }
    }

    public void removeElement(String elementName) {
        elements.remove(elementName);
        BinaryElement elementForRemoving = new BinaryElement(elementName);
        for (BinaryElement element : getElements()) {
            element.getRelationTo().remove(elementForRemoving);
        }
    }

    public void addConnection(String fromElementName, String toElementName) {
        BinaryElement fromElement = addOrGetElement(fromElementName);
        BinaryElement toElement = addOrGetElement(toElementName);
        fromElement.addRelationTo(toElement);
    }

    public Collection<BinaryElement> getElements() {
        return elements.values();
    }

    public boolean hasConnection(BinaryElement fromElement, BinaryElement toElement) {
        return elements.get(fromElement.getName()).getRelationTo().contains(toElement);
    }

    public boolean elementIsReachableFromOther(BinaryElement from, BinaryElement to) {
        if(elements.get(from.getName()).getRelationTo().contains(to)) {
            return true;
        } else {
            for (BinaryElement element : elements.get(from.getName()).getRelationTo()) {
                if (elementIsReachableFromOther(element, to)) {
                    return true;
                }
            }
        }

        return false;
    }

    public void removeConnection(BinaryElement fromElement, BinaryElement toElement) {
        try {
        elements.get(fromElement.getName()).getRelationTo().remove(toElement);
        } catch (NullPointerException npe) {
            System.out.println("Element " + fromElement.getName() + " not found");
        }
    }

    public void removeConnection(String from, String to) {
        try {
            elements.get(from).getRelationTo().remove(new BinaryElement(to));
        } catch (NullPointerException npe) {
            System.out.println("Element " + from + " not found");
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("BinaryRelationship{");
        for (BinaryElement element : elements.values()) {
            for (BinaryElement connectedElement : element.getRelationTo()) {
                result.append(element.getName() + "->" + connectedElement.getName() + ",");
            }
        }
        result.append("}");

        return result.toString();
    }

    public DirectedSparseMultigraph<String, String> getGraph() {
        DirectedSparseMultigraph<String, String> g = new DirectedSparseMultigraph<String, String>();

        for (BinaryElement element : getElements()) {
            g.addVertex(element.getName());
        }

        Integer edgeNumber = 0;
        for (BinaryElement element : getElements()) {
            for (BinaryElement connectedTo : element.getRelationTo()) {
                g.addEdge((edgeNumber++).toString(), element.getName(), connectedTo.getName());
            }
        }

        return g;
    }
}
