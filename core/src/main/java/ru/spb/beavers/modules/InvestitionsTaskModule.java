package ru.spb.beavers.modules;

import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.net.URI;

public class InvestitionsTaskModule implements ITaskModule {

    private static final String RESOURCES_PATH = "investitions_task/";
    private static final String HTML_PATH = RESOURCES_PATH + "html/";
    private static final String HTML_DESCRIPTION = HTML_PATH + "description.html";
    private static final String HTML_THEORY = HTML_PATH + "solution.html";
    private static final String IMAGES_PATH = RESOURCES_PATH + "images/";
    private static final String IMG_SOLUTION = IMAGES_PATH + "solution.png";
    private static final String IMG_EXAMPLE_1 = IMAGES_PATH + "example_1.png";
    private static final String IMG_EXAMPLE_2 = IMAGES_PATH + "example_2.png";
    private static final String IMG_EXAMPLE_3 = IMAGES_PATH + "example_3.png";
    private static final String IMG_EXAMPLE_4 = IMAGES_PATH + "example_4.png";
    private static final String IMG_EXAMPLE_5 = IMAGES_PATH + "example_5.png";
    private static final String IMG_EXAMPLE_6 = IMAGES_PATH + "example_6.png";
    private static final String IMG_EXAMPLE_7 = IMAGES_PATH + "example_7.png";
    private static final String IMG_GRAPH = IMAGES_PATH + "graph.png";
    private static final String IMG_CHART = IMAGES_PATH + "chart.png";

    private final JTextField tfStockLoss;
    private final JTextField tfStockProfit;
    private final JTextField tfBondsProfit;

    public InvestitionsTaskModule() {
        tfStockLoss = new JTextField();
        tfStockProfit = new JTextField();
        tfBondsProfit = new JTextField();
    }

    @Override
    public String getTitle() {
        return "1.1.1 Задача об\nинвестировании средств";
    }

    /**
     * Приводит компоненты к исходному состоянию
     */
    private void resetComponents() {
        tfStockLoss.setText("");
        tfStockProfit.setText("");
        tfBondsProfit.setText("");
    }

    @Override
    public void initDescriptionPanel(JPanel panel) {
        resetComponents();
        panel.setLayout(new GridLayout(1, 1));

        JTextPane descriptionPane = new JTextPane();
        descriptionPane.setPreferredSize(new Dimension(380, 200));
        try {
            URI uri = InvestitionsTaskModule.class.getResource(HTML_DESCRIPTION).toURI();
            descriptionPane.setPage(uri.toURL());
            descriptionPane.setFont(new Font("Times new roman", 0, 20));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        JScrollPane scrollPane = new JScrollPane(descriptionPane);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane.getVerticalScrollBar().setUnitIncrement(16);
        scrollPane.setBorder(null);
        scrollPane.setOpaque(false);
        scrollPane.setBackground(new Color(0, 0, 0, 0));
        descriptionPane.setFocusable(false);
        descriptionPane.setBackground(new Color(240, 240, 240));
        panel.add(scrollPane);
    }

    @Override
    public void initSolutionPanel(JPanel panel) {

        panel.setLayout(new GridLayout(1, 1));

        JLabel lbDescription = new JLabel(new ImageIcon(InvestitionsTaskModule.class.getResource(IMG_SOLUTION)));
        lbDescription.setSize(728, 4120);
        panel.add(lbDescription);
    }

    @Override
    public void initInputPanel(JPanel panel) {
        panel.setLayout(null);

        JLabel lbTitle = new JLabel("Ввод исходных данных");
        lbTitle.setFont(new Font("Arial", 0, 24));
        lbTitle.setBounds(300, 20, 400, 30);
        panel.add(lbTitle);

        JLabel lbStockLoss = new JLabel("Прибыль при неуспешном вложении в акции: ");
        lbStockLoss.setFont(new Font("Arial", 0, 16));
        lbStockLoss.setBounds(30, 100, 350, 30);
        panel.add(lbStockLoss);

        tfStockLoss.setBounds(370, 100, 120, 30);
        panel.add(tfStockLoss);

        JLabel lbStockProfit = new JLabel("Прибыль при успешном вложении в акции: ");
        lbStockProfit.setFont(new Font("Arial", 0, 16));
        lbStockProfit.setBounds(30, 150, 350, 30);
        panel.add(lbStockProfit);

        tfStockProfit.setBounds(370, 150, 120, 30);
        panel.add(tfStockProfit);

        JLabel lbBondsProfit = new JLabel("Прибыль при вложении в облигации: ");
        lbBondsProfit.setFont(new Font("Arial", 0, 16));
        lbBondsProfit.setBounds(30, 200, 350, 30);
        panel.add(lbBondsProfit);

        tfBondsProfit.setBounds(370, 200, 120, 30);
        panel.add(tfBondsProfit);
    }

    @Override
    public void initExamplePanel(JPanel panel) {
        validateInputData();
        panel.setLayout(null);
        panel.setPreferredSize(new Dimension(900, 2700));

        JLabel lbExample1 = new JLabel(new ImageIcon(InvestitionsTaskModule.class.getResource(IMG_EXAMPLE_1)));
        lbExample1.setBounds(0, 0, 728, 61);
        panel.add(lbExample1);

        JLabel formula1 = getFormula("V(y_1,d_1)=V(y_2,d_1)=v(a_2)= v_2=" + tfBondsProfit.getText());
        formula1.setLocation(20, 80);
        panel.add(formula1);

        JLabel formula2 = getFormula("V(y_1,d_2)=v(a_1)= v_1=" + tfStockProfit.getText());
        formula2.setLocation(20, 110);
        panel.add(formula2);

        JLabel formula3 = getFormula("V(y_2,d_2)=v(a_2)= v_3=" + tfStockLoss.getText());
        formula3.setLocation(20, 140);
        panel.add(formula3);

        JLabel lbExample2 = new JLabel(new ImageIcon(InvestitionsTaskModule.class.getResource(IMG_EXAMPLE_2)));
        lbExample2.setBounds(0, 200, 728, 61);
        panel.add(lbExample2);

        JLabel formula4 = getFormula("L_1: u(d_1) = u(L_1)= v_2=" + tfBondsProfit.getText());
        formula4.setLocation(20, 265);
        panel.add(formula4);

        JLabel lbExample3 = new JLabel(new ImageIcon(InvestitionsTaskModule.class.getResource(IMG_EXAMPLE_3)));
        lbExample3.setBounds(0, 300, 728, 61);
        panel.add(lbExample3);

        JLabel formula5 = getFormula(String.format("L_2: u(d_2)= u(L_2)= %.0f * p_1 + %.0f",
                Double.valueOf(tfStockProfit.getText()) - Double.valueOf(tfBondsProfit.getText()),
                Double.valueOf(tfBondsProfit.getText())));
        formula5.setLocation(20, 345);
        panel.add(formula5);

        JLabel lbHorizontalStrut1 = new JLabel("");
        lbHorizontalStrut1.setBorder(BorderFactory.createLineBorder(Color.GRAY, 2));
        lbHorizontalStrut1.setBounds(80, 410, 600, 3);
        panel.add(lbHorizontalStrut1);

        JLabel lbExample4 = new JLabel(new ImageIcon(InvestitionsTaskModule.class.getResource(IMG_EXAMPLE_4)));
        lbExample4.setBounds(0, 440, 728, 538);
        panel.add(lbExample4);

        JLabel lbChanceIndifference = new JLabel(String.format("%.3f",
                (Double.valueOf(tfBondsProfit.getText()) - Double.valueOf(tfStockLoss.getText()))
              / (Double.valueOf(tfStockProfit.getText()) - Double.valueOf(tfStockLoss.getText()))
        ));
        lbChanceIndifference.setBounds(165, 932, 170, 30);
        lbChanceIndifference.setFont(new Font("Times new roman", 0, 18));
        panel.add(lbChanceIndifference);


        JLabel formula6 = getFormula(
                "\\[ = \\left\\{"
                        + " \\begin{array}{l l}"
                        + " d_1, & \\quad \\text{если } p_1)>" + String.valueOf(
                        (Double.valueOf(tfBondsProfit.getText()) - Double.valueOf(tfStockLoss.getText()))
                                / (Double.valueOf(tfStockProfit.getText()) - Double.valueOf(tfStockLoss.getText()))
                                + "\\\\"
                + " d_2, & \\quad \\text{иначе}"
                + " \\end{array} \\right.\\]"));
        formula6.setLocation(450, 825);
        panel.add(formula6);

        JLabel lbHorizontalStrut2 = new JLabel("");
        lbHorizontalStrut2.setBorder(BorderFactory.createLineBorder(Color.GRAY, 2));
        lbHorizontalStrut2.setBounds(80, 1000, 600, 3);
        panel.add(lbHorizontalStrut2);

        JLabel lbExample5 = new JLabel(new ImageIcon(InvestitionsTaskModule.class.getResource(IMG_EXAMPLE_5)));
        lbExample5.setBounds(0, 1030, 728, 293);
        panel.add(lbExample5);

        JLabel formula7 = getFormula(String.format("%.1f * p_1 + %.1f",
                Double.valueOf(tfStockProfit.getText()) - Double.valueOf(tfBondsProfit.getText()),
                Double.valueOf(tfBondsProfit.getText())));
        formula7.setLocation(532, 1289);
        panel.add(formula7);

        JLabel lbHorizontalStrut3 = new JLabel("");
        lbHorizontalStrut3.setBorder(BorderFactory.createLineBorder(Color.GRAY, 2));
        lbHorizontalStrut3.setBounds(80, 1355, 600, 3);
        panel.add(lbHorizontalStrut3);

        JLabel lbExample6 = new JLabel(new ImageIcon(InvestitionsTaskModule.class.getResource(IMG_EXAMPLE_6)));
        lbExample6.setBounds(10, 1385, 761, 276);
        panel.add(lbExample6);

        JLabel formula8 = getFormula(String.format(
                "\\[ \\left\\{"
                        + " \\begin{array}{l l}"
                        + " %.2f * p_1, & \\quad \\text{если } p_1>%.2f\\\\" // v1 - v2
                        + " %.2f * (1 - p_1), & \\quad \\text{иначе}" // v2-v3
                        + " \\end{array} \\right.\\] ",
                Double.valueOf(tfStockProfit.getText()) - Double.valueOf(tfBondsProfit.getText()),
                (Double.valueOf(tfBondsProfit.getText()) - Double.valueOf(tfStockLoss.getText()))
                        / (Double.valueOf(tfStockProfit.getText()) - Double.valueOf(tfStockLoss.getText())),
                Double.valueOf(tfBondsProfit.getText()) - Double.valueOf(tfStockLoss.getText())));
        formula8.setLocation(70, 1599);
        panel.add(formula8);

        JLabel solutionGraph = new JLabel() {

            private final static int SPACEUNIT = 30;

            @Override
            public void paint(Graphics g) {
                super.paint(g);
                ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                g.drawOval(0, 5 * SPACEUNIT, SPACEUNIT, SPACEUNIT);
                g.drawRect(4 * SPACEUNIT, (int) (2.5 * SPACEUNIT), SPACEUNIT, SPACEUNIT);
                g.drawRect(4 * SPACEUNIT, (int) (7.5 * SPACEUNIT), SPACEUNIT, SPACEUNIT);
                g.drawString("y1", 0, 4 * SPACEUNIT);
                g.drawString("y2", 0, 7 * SPACEUNIT);

                g.drawLine(SPACEUNIT, (int) (5.5 * SPACEUNIT), 3 * SPACEUNIT, (int) (3 * SPACEUNIT));
                g.drawLine(3 * SPACEUNIT, (int) (3 * SPACEUNIT), 4 * SPACEUNIT, (int) (3 * SPACEUNIT));

                g.drawLine(SPACEUNIT, (int) (5.5 * SPACEUNIT), 3 * SPACEUNIT, (int) (8 * SPACEUNIT));
                g.drawLine(3 * SPACEUNIT, (int) (8 * SPACEUNIT), 4 * SPACEUNIT, (int) (8 * SPACEUNIT));

                g.drawLine(5 * SPACEUNIT, (int) (8 * SPACEUNIT), 7 * SPACEUNIT, (int) (6 * SPACEUNIT));
                g.drawLine(7 * SPACEUNIT, (int) (6 * SPACEUNIT), 11 * SPACEUNIT, (int) (6 * SPACEUNIT));
                g.drawString("d1", (int) (7 * SPACEUNIT), (int) (5.8 * SPACEUNIT));

                g.drawLine(5 * SPACEUNIT, (int) (3 * SPACEUNIT), 7 * SPACEUNIT, (int) (5 * SPACEUNIT));
                g.drawLine(7 * SPACEUNIT, (int) (5 * SPACEUNIT), 11 * SPACEUNIT, (int) (5 * SPACEUNIT));
                g.drawString("d2", (int) (7 * SPACEUNIT), (int) (4.8 * SPACEUNIT));

                g.drawLine(5 * SPACEUNIT, (int) (3 * SPACEUNIT), 7 * SPACEUNIT, (int) (1 * SPACEUNIT));
                g.drawLine(7 * SPACEUNIT, (int) (1 * SPACEUNIT), 11 * SPACEUNIT, (int) (1 * SPACEUNIT));
                g.drawString("d1", (int) (7 * SPACEUNIT), (int) (0.8 * SPACEUNIT));

                g.drawLine(5 * SPACEUNIT, (int) (8 * SPACEUNIT), 7 * SPACEUNIT, (int) (10 * SPACEUNIT));
                g.drawLine(7 * SPACEUNIT, (int) (10 * SPACEUNIT), 11 * SPACEUNIT, (int) (10 * SPACEUNIT));
                g.drawString("d2", (int) (7 * SPACEUNIT), (int) (9.8 * SPACEUNIT));

                g.drawString("a2 v2 = " + tfBondsProfit.getText(), (int) (11.2 * SPACEUNIT), (int) (1.1 * SPACEUNIT));
                g.drawString("a1 v1 = " + tfStockProfit.getText(), (int) (11.2 * SPACEUNIT), (int) (5.1 * SPACEUNIT));
                g.drawString("a2 v2 = " + tfBondsProfit.getText(), (int) (11.2 * SPACEUNIT), (int) (6.1 * SPACEUNIT));
                g.drawString("a3 v3 = " + tfStockLoss.getText(), (int) (11.2 * SPACEUNIT), (int) (10.1 * SPACEUNIT));
            }
        };
        solutionGraph.setBounds(200, 1720, 850, 400);
        panel.add(solutionGraph);

        JLabel lbGraph = new JLabel(new ImageIcon(InvestitionsTaskModule.class.getResource(IMG_GRAPH)));
        lbGraph.setBounds(250, 2030, 347, 49);
        panel.add(lbGraph);

        JLabel chart = new JLabel() {

            @Override
            public void paint(Graphics g) {
                super.paint(g);
                ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                g.setColor(new Color(0, 0, 0));
                g.fillPolygon(new int[]{5, 10, 15}, new int[]{10, 5, 10}, 3);
                g.drawLine(10, 10, 10, 125);        // Y - ордината
                g.drawLine(5, 120, 240, 120);       // X - ордината
                int y = 50;
                int x = (int) ((Double.valueOf(tfBondsProfit.getText()) - Double.valueOf(tfStockLoss.getText()))
                        / (Double.valueOf(tfStockProfit.getText()) - Double.valueOf(tfStockLoss.getText())) * 230);
                g.drawLine(10, 120, x, y);
                g.drawLine(x, y, 230, 120);
                g.drawString("0", 2, 135);
                g.drawString("1", 225, 135);
                g.drawString(String.format("%.1f", (Double.valueOf(tfBondsProfit.getText()) - Double.valueOf(tfStockLoss.getText()))
                        / (Double.valueOf(tfStockProfit.getText()) - Double.valueOf(tfStockLoss.getText()))), x - 5, 135);
                g.drawString(String.format("%.1f",
                        ((Double.valueOf(tfBondsProfit.getText()) - Double.valueOf(tfStockLoss.getText()))
                                / (Double.valueOf(tfStockProfit.getText()) - Double.valueOf(tfStockLoss.getText())))
                                * (Double.valueOf(tfStockProfit.getText()) - Double.valueOf(tfBondsProfit.getText()))), 15, y - 10);
//                g.drawLine(x, 125, x + 5, 125);
                g.drawLine(5, y, 15, y);
                g.drawLine(x + 3, 118, x + 3, 122);
            }
        };
        chart.setBounds(275, 2150, 500, 200);
        panel.add(chart);

        JLabel lbChart = new JLabel(new ImageIcon(InvestitionsTaskModule.class.getResource(IMG_CHART)));
        lbChart.setBounds(250, 2280, 321, 58);
        panel.add(lbChart);

        JLabel lbExample7 = new JLabel(new ImageIcon(InvestitionsTaskModule.class.getResource(IMG_EXAMPLE_7)));
        lbExample7.setBounds(0, 2350, 728, 300);
        panel.add(lbExample7);

        JLabel lastValue = new JLabel(String.format("%.1f",
                ((Double.valueOf(tfBondsProfit.getText()) - Double.valueOf(tfStockLoss.getText()))
                        / (Double.valueOf(tfStockProfit.getText()) - Double.valueOf(tfStockLoss.getText())))
                        * (Double.valueOf(tfStockProfit.getText()) - Double.valueOf(tfBondsProfit.getText()))));
        lastValue.setFont(new Font("Times new roman", 0, 18));
        lastValue.setBounds(250, 2360, 100, 30);
        panel.add(lastValue);
    }

    public JLabel getFormula(String text) {
        TeXFormula formula = new TeXFormula(text);
        TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 20);
        icon.setInsets(new Insets(5, 5, 5, 5));
        BufferedImage image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = image.createGraphics();
        JLabel jl = new JLabel();
        jl.setForeground(new Color(0, 0, 0));
        icon.paintIcon(jl, g2, 0, 0);
        JLabel formulaIcon = new JLabel(new ImageIcon(image));
        formulaIcon.setSize(icon.getIconWidth(), icon.getIconHeight());
        return formulaIcon;
    }

    /**
     * Проверяет введенные исходные данные на корректность
     */
    private void validateInputData() {
        try {
            double boundsProfit = Double.valueOf(tfBondsProfit.getText());
            double stockProfit = Double.valueOf(tfStockProfit.getText());
            double stockLoss = Double.valueOf(tfStockLoss.getText());
            if (boundsProfit > stockProfit) {
                throw new RuntimeException("Доходность облигаций должна быть меньше акций!");
            }
            if (stockLoss > stockProfit) {
                throw new RuntimeException("Прибыль при неуспешном вложении в акции должна быть меньше, чем при успешном!");
            }
            if (stockLoss > stockProfit) {
                throw new RuntimeException("Прибыль при неуспешном вложении в акции должна быть меньше, чем в облигации!");
            }
        } catch (NumberFormatException ex) {
            throw new RuntimeException("Введенные значения не являются числами!");
        }
    }

    @Override
    public ActionListener getPressSaveListener() throws IllegalArgumentException {
        return null;
    }

    @Override
    public ActionListener getPressLoadListener() throws IllegalArgumentException {
        return null;
    }

    @Override
    public ActionListener getDefaultValuesListener() throws IllegalArgumentException {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tfBondsProfit.setText(String.valueOf(1000));
                tfStockProfit.setText(String.valueOf(5000));
                tfStockLoss.setText(String.valueOf(-5000));
            }
        };
    }
}
