package ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui.rabsourse;

import edu.uci.ics.jung.algorithms.layout.StaticLayout;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.SparseMultigraph;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import org.apache.commons.collections15.Transformer;
import ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui.SolutionPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Point2D;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * Created by Sasha on 09.04.2015.
 */
public class PanelEl {
    private static class LocationTransformer implements Transformer<VertexS, Point2D> {
        private Map vx_coords;

        public LocationTransformer(Map vx_coords)
        {
            this.vx_coords = vx_coords;
        }

        @Override
        public Point2D transform(VertexS vertex) {
            Point2D vx_coord = (Point2D)vx_coords.get(vertex.name);
            return new Point2D.Double((double) vx_coord.getX(), (double) vx_coord.getY());
        }
    };

    private static class EdgeLabelTransformer implements Transformer<String, String> {
        private Map edge_values;

        public EdgeLabelTransformer(Map edge_values)
        {
            this.edge_values = edge_values;
        }

        @Override
        public String transform(String edge) {
            return edge;
        }
    };

    public static void setTextHeader(String header, int oy, int width, int height, JPanel panel)
    {
        JLabel jl = new JLabel("<html><body width=\"" + width + "\" height=\"" + height
                + "\"><center><b><i>" + header + "</i></b></center></body></html>");
        jl.setFont(new Font("Arial", Font.BOLD, 15));
        jl.setSize(width, height);
        jl.setLocation(panel.getWidth() / 2 - jl.getWidth() / 2, oy);
        panel.add(jl);
    }

    public static void setTextHeader(String header, int oy, int width, int height, ElementKord last_el_coords,  JPanel panel)
    {
        JLabel jl = new JLabel("<html><body width=\"" + width + "\" height=\"" + height
                + "\"><center><b><i>" + header + "</i></b></center></body></html>");
        jl.setFont(new Font("Arial", Font.BOLD, 15));
        jl.setSize(width, height);
        jl.setLocation(panel.getWidth() / 2 - jl.getWidth() / 2, last_el_coords.y + last_el_coords.height + oy);
        panel.add(jl);
        last_el_coords.y = jl.getY();
        last_el_coords.height = jl.getHeight();
    }

    public static void setHtmlText(String url_string, int x, int y, int width, int height, JPanel panel)
    {
        JTextPane jtp = new JTextPane();

        try {

            //System.out.print(ClassLoader.getSystemClassLoader().getResource(url_string));
            URI uri = ClassLoader.getSystemClassLoader().getResource(url_string).toURI();
            try {
                jtp.setPage(uri.toURL());
            } catch (Exception e) {
            }

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        JScrollPane scroll = new JScrollPane(jtp);
        scroll.setSize(width, height);
        scroll.setLocation(x, y);
        //scroll.setPreferredSize(new Dimension(width, height));
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        panel.add(scroll);
        //jtp.setEditable(false);
        jtp.setBackground(new Color(240,240,240));
    }

    public static void setHtmlText600Px(String url_string, ElementKord last_el_coords, JPanel panel)
    {
        setHtmlText(url_string, panel.getWidth() / 2 - 300, last_el_coords.y, 600, last_el_coords.height, panel);
    }

    public static void setDescriptionTree(JPanel panel, Vector<String> edges, ElementKord last_el_coords, int offset_y)
    {
        int vx_r = 10;
        int delta_x = 90;
        int delta_x2= 40;
        int ox = 20;
        int oy = 40;
        int dln=60;
        double delta_x3=ox+delta_x*1.3;
        //vector.elementAt()

        Map edge_values = null;
        Map vx_coords = new HashMap();
        vx_coords.put(" ", new Point2D.Double(ox+delta_x/2, oy+vx_r*7));
        vx_coords.put("d1", new Point2D.Double(ox+delta_x/2+delta_x/2, oy+vx_r*7-(vx_r*7)/2.5));
        vx_coords.put("d2", new Point2D.Double(ox+delta_x/2+delta_x/2, oy+vx_r*7+(vx_r*7)/2.5));

        vx_coords.put("L1", new Point2D.Double(ox+delta_x*1.5, oy+vx_r*2));
        vx_coords.put("L2", new Point2D.Double(ox+delta_x*1.5, oy+vx_r*12));

        vx_coords.put("0.7", new Point2D.Double(ox+delta_x*2.5, oy));
        vx_coords.put("0.3", new Point2D.Double(ox+delta_x*2.5, oy+vx_r*5));
        vx_coords.put("0.5", new Point2D.Double(ox+delta_x*2.5, oy+vx_r*10));
        vx_coords.put("0.5 ", new Point2D.Double(ox+delta_x*2.5, oy+vx_r*15));
        vx_coords.put("L1.", new Point2D.Double(ox+delta_x*2.5+delta_x2+dln, oy+vx_r*7));
        vx_coords.put("0.7 ", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+dln, oy));
        vx_coords.put("0.3 ", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+dln, oy+vx_r*5));
        vx_coords.put("О", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+dln, oy+vx_r*10));
        vx_coords.put("O", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+dln, oy+vx_r*15));
        vx_coords.put("a1", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+delta_x2+dln, oy));
        vx_coords.put("a2", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+delta_x2+dln, oy+vx_r*5));
        vx_coords.put("a3", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+delta_x2+dln, oy+vx_r*10));
        vx_coords.put("a4", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+delta_x2+dln, oy+vx_r*15));
        vx_coords.put("L2.", new Point2D.Double(ox+delta_x*2.5+delta_x2+delta_x3+dln, oy+vx_r*7));
        vx_coords.put("0", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+delta_x3+dln, oy));
        vx_coords.put("0 ", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+delta_x3+dln, oy+vx_r*5));
        vx_coords.put("0.5  ", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+delta_x3+dln, oy+vx_r*10));
        vx_coords.put("0.5   ", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+delta_x3+dln, oy+vx_r*15));
        vx_coords.put("а1", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+delta_x2+delta_x3+dln, oy));
        vx_coords.put("а2", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+delta_x2+delta_x3+dln, oy+vx_r*5));
        vx_coords.put("а3", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+delta_x2+delta_x3+dln, oy+vx_r*10));
        vx_coords.put("а4", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+delta_x2+delta_x3+dln, oy+vx_r*15));

        vx_coords.put("r1= 18", new Point2D.Double(ox+delta_x*2.5+50, oy));
        vx_coords.put("r2= 12", new Point2D.Double(ox+delta_x*2.5+50, oy+vx_r*5));
        vx_coords.put("r3= 22", new Point2D.Double(ox+delta_x*2.5+50, oy+vx_r*10));
        vx_coords.put("r2= 12 ", new Point2D.Double(ox+delta_x*2.5+50, oy+vx_r*15));
        if (edges != null)
        {
            edge_values = new HashMap();
            edge_values.put("3w", edges.elementAt(0));
            edge_values.put("e", edges.elementAt(1));
            edge_values.put(" 1", edges.elementAt(2));
            edge_values.put(" w", edges.elementAt(3));
            edge_values.put(" 3", edges.elementAt(4));
            edge_values.put(" 4", edges.elementAt(5));
            edge_values.put(" 11", edges.elementAt(6));
            edge_values.put(" 12", edges.elementAt(7));

        }

        ArrayList<VertexS> vx_list = new ArrayList<VertexS>(25);

        vx_list.add(new VertexS(" ", "Square")); // 0


        vx_list.add(new VertexS("L1", "Circle")); // 1
        vx_list.add(new VertexS("L2", "Circle")); // 2

        vx_list.add(new VertexS("0.7", "Dot")); // 3
        vx_list.add(new VertexS("0.3", "Dot")); // 4
        vx_list.add(new VertexS("0.5", "Dot")); // 5
        vx_list.add(new VertexS("0.5 ", "Dot")); // 6

        vx_list.add(new VertexS("L1.", "Circle")); // 7

        vx_list.add(new VertexS("0.7 ", "Dot")); // 8
        vx_list.add(new VertexS("0.3 ", "Dot")); // 9
        vx_list.add(new VertexS("O", "Dot")); // 10
        vx_list.add(new VertexS("О", "Dot")); // 11
        vx_list.add(new VertexS("a1", "Dot")); // 12
        vx_list.add(new VertexS("a2", "Dot")); // 13
        vx_list.add(new VertexS("a3", "Dot")); // 14
        vx_list.add(new VertexS("a4", "Dot")); // 15
        vx_list.add(new VertexS("L2.", "Circle")); // 16


        vx_list.add(new VertexS("0", "Dot")); // 17
        vx_list.add(new VertexS("0 ", "Dot")); // 18
        vx_list.add(new VertexS("0.5  ", "Dot")); // 19
        vx_list.add(new VertexS("0.5   ", "Dot")); // 20
        vx_list.add(new VertexS("а1", "Dot")); // 21
        vx_list.add(new VertexS("а2", "Dot")); // 22
        vx_list.add(new VertexS("а3", "Dot")); // 23
        vx_list.add(new VertexS("а4", "Dot")); // 24
        vx_list.add(new VertexS("d1", "Dotm")); // 25
        vx_list.add(new VertexS("d2", "Dotm")); // 26

        vx_list.add(new VertexS("r1= 18", "Dotm")); // 25
        vx_list.add(new VertexS("r2= 12", "Dotm")); // 26
        vx_list.add(new VertexS("r3= 22", "Dotm")); // 25
        vx_list.add(new VertexS("r2= 12 ", "Dotm")); // 26
        Graph<VertexS, String> basis = new SparseMultigraph<VertexS, String>();
        for(VertexS vx: vx_list)
        {
            basis.addVertex(vx);
        }

        basis.addEdge("1", vx_list.get(0), vx_list.get(25));
        basis.addEdge("2", vx_list.get(0), vx_list.get(26));
        basis.addEdge("242", vx_list.get(25), vx_list.get(1));
        basis.addEdge("243", vx_list.get(26), vx_list.get(2));
        basis.addEdge(" 1", vx_list.get(1), vx_list.get(3));
        basis.addEdge(" 2", vx_list.get(1), vx_list.get(4));

        basis.addEdge(" 3", vx_list.get(2), vx_list.get(5));
        basis.addEdge(" 4", vx_list.get(2), vx_list.get(6));

        basis.addEdge(" 11", vx_list.get(7), vx_list.get(8));
        basis.addEdge(" 12", vx_list.get(7), vx_list.get(9));

        basis.addEdge(" 21", vx_list.get(7), vx_list.get(10));
        basis.addEdge(" 22", vx_list.get(7), vx_list.get(11));
        basis.addEdge(" 111", vx_list.get(8), vx_list.get(12));
        basis.addEdge(" 122", vx_list.get(9), vx_list.get(13));

        basis.addEdge(" 213", vx_list.get(10), vx_list.get(15));
        basis.addEdge(" 224", vx_list.get(11), vx_list.get(14));

        basis.addEdge(" 11 ", vx_list.get(16), vx_list.get(17));
        basis.addEdge(" 12 ", vx_list.get(16), vx_list.get(18));

        basis.addEdge(" 21 ", vx_list.get(16), vx_list.get(19));
        basis.addEdge(" 22 ", vx_list.get(16), vx_list.get(20));
        basis.addEdge(" 111 ", vx_list.get(17), vx_list.get(21));
        basis.addEdge(" 122 ", vx_list.get(18), vx_list.get(22));

        basis.addEdge(" 213 ", vx_list.get(19), vx_list.get(23));
        basis.addEdge(" 224 ", vx_list.get(20), vx_list.get(24));
        StaticLayout<VertexS, String> layout = new StaticLayout<VertexS, String>(
                basis, new LocationTransformer(vx_coords));
        layout.setSize(new Dimension(250, 250));
        edu.uci.ics.jung.visualization.VisualizationViewer<VertexS, String> vv = new edu.uci.ics.jung.visualization.VisualizationViewer<VertexS, String>(layout);
        vv.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line<VertexS, String>());
        vv.getRenderContext().setVertexLabelTransformer(new Transformer<VertexS, String>() {
            public String transform(VertexS vx) {
                return vx.name;
            }
        });
        // vv.getRenderContext().setEdgeLabelTransformer(new EdgeLabelTransformer(edge_values));
        vv.getRenderer().setVertexRenderer(new RenderDiagram());
        vv.getRenderer().getVertexLabelRenderer().setPosition(edu.uci.ics.jung.visualization.renderers.Renderer.VertexLabel.Position.N);
        vv.setSize(650, 200);
        last_el_coords.updateValues(offset_y, vv.getHeight());
        vv.setLocation(panel.getWidth() / 2 - vv.getWidth() / 2, last_el_coords.y);
        panel.add(vv);
    }


    public static void setDescriptionTree1(JPanel panel, Vector<String> edges, ElementKord last_el_coords, int offset_y)
    {
        int vx_r = 10;
        int delta_x = 90;
        int delta_x2= 40;
        int ox = 20;
        int oy = 45;
        int dln=60;
        double delta_x3=ox+delta_x*1.3;
        //vector.elementAt()

        Map edge_values = null;
        Map vx_coords = new HashMap();
        vx_coords.put(" ", new Point2D.Double(ox+delta_x/2, oy+vx_r*7));
        vx_coords.put("d1", new Point2D.Double(ox+delta_x/2+delta_x/2, oy+vx_r*7-(vx_r*7)/2.5));
        vx_coords.put("d2", new Point2D.Double(ox+delta_x/2+delta_x/2, oy+vx_r*7+(vx_r*7)/2.5));

        vx_coords.put("L1", new Point2D.Double(ox+delta_x*1.5, oy+vx_r*2));
        vx_coords.put("L2", new Point2D.Double(ox+delta_x*1.5, oy+vx_r*12));

        vx_coords.put("P11", new Point2D.Double(ox+delta_x*2.5, oy));
        vx_coords.put("P12", new Point2D.Double(ox+delta_x*2.5, oy+vx_r*5));
        vx_coords.put("P21", new Point2D.Double(ox+delta_x*2.5, oy+vx_r*10));
        vx_coords.put("P22", new Point2D.Double(ox+delta_x*2.5, oy+vx_r*15));
        vx_coords.put("L1.", new Point2D.Double(ox+delta_x*2.5+delta_x2 + dln, oy+vx_r*7));
        vx_coords.put("p11", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+ dln, oy));
        vx_coords.put("p12", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+ dln, oy+vx_r*5));
        vx_coords.put("О", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+ dln, oy+vx_r*10));
        vx_coords.put("O", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+ dln, oy+vx_r*15));
        vx_coords.put("a1", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+delta_x2+ dln, oy));
        vx_coords.put("a2", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+delta_x2+ dln, oy+vx_r*5));
        vx_coords.put("a3", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+delta_x2+ dln, oy+vx_r*10));
        vx_coords.put("a4", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+delta_x2+ dln, oy+vx_r*15));
        vx_coords.put("L2.", new Point2D.Double(ox+delta_x*2.5+delta_x2+delta_x3+ dln, oy+vx_r*7));
        vx_coords.put("0", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+delta_x3+ dln, oy));
        vx_coords.put("0 ", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+delta_x3+ dln, oy+vx_r*5));
        vx_coords.put("p21", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+delta_x3+ dln, oy+vx_r*10));
        vx_coords.put("p22", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+delta_x3+ dln, oy+vx_r*15));
        vx_coords.put("а1", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+delta_x2+delta_x3+ dln, oy));
        vx_coords.put("а2", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+delta_x2+delta_x3+ dln, oy+vx_r*5));
        vx_coords.put("а3", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+delta_x2+delta_x3+ dln, oy+vx_r*10));
        vx_coords.put("а4", new Point2D.Double(ox+delta_x*2.5+delta_x2*2.5+delta_x2+delta_x3+ dln, oy+vx_r*15));

        vx_coords.put("r1=r(a1)", new Point2D.Double(ox+delta_x*2.5+50, oy));
        vx_coords.put("r2=r(a2)", new Point2D.Double(ox+delta_x*2.5+50, oy+vx_r*5));
        vx_coords.put("r3=r(a3)", new Point2D.Double(ox+delta_x*2.5+50, oy+vx_r*10));
        vx_coords.put("r2=r(a4)", new Point2D.Double(ox+delta_x*2.5+50, oy+vx_r*15));

        if (edges != null)
        {
            edge_values = new HashMap();
            edge_values.put("3w", edges.elementAt(0));
            edge_values.put("e", edges.elementAt(1));
            edge_values.put(" 1", edges.elementAt(2));
            edge_values.put(" w", edges.elementAt(3));
            edge_values.put(" 3", edges.elementAt(4));
            edge_values.put(" 4", edges.elementAt(5));
            edge_values.put(" 11", edges.elementAt(6));
            edge_values.put(" 12", edges.elementAt(7));

        }

        ArrayList<VertexS> vx_list = new ArrayList<VertexS>(25);

        vx_list.add(new VertexS(" ", "Square")); // 0


        vx_list.add(new VertexS("L1", "Circle")); // 1
        vx_list.add(new VertexS("L2", "Circle")); // 2

        vx_list.add(new VertexS("P11", "Dot")); // 3
        vx_list.add(new VertexS("P12", "Dot")); // 4
        vx_list.add(new VertexS("P21", "Dot")); // 5
        vx_list.add(new VertexS("P22", "Dot")); // 6

        vx_list.add(new VertexS("L1.", "Circle")); // 7

        vx_list.add(new VertexS("p11", "Dot")); // 8
        vx_list.add(new VertexS("p12", "Dot")); // 9
        vx_list.add(new VertexS("O", "Dot")); // 10
        vx_list.add(new VertexS("О", "Dot")); // 11
        vx_list.add(new VertexS("a1", "Dot")); // 12
        vx_list.add(new VertexS("a2", "Dot")); // 13
        vx_list.add(new VertexS("a3", "Dot")); // 14
        vx_list.add(new VertexS("a4", "Dot")); // 15
        vx_list.add(new VertexS("L2.", "Circle")); // 16


        vx_list.add(new VertexS("0", "Dot")); // 17
        vx_list.add(new VertexS("0 ", "Dot")); // 18
        vx_list.add(new VertexS("p21", "Dot")); // 19
        vx_list.add(new VertexS("p22", "Dot")); // 20
        vx_list.add(new VertexS("а1", "Dot")); // 21
        vx_list.add(new VertexS("а2", "Dot")); // 22
        vx_list.add(new VertexS("а3", "Dot")); // 23
        vx_list.add(new VertexS("а4", "Dot")); // 24
        vx_list.add(new VertexS("d1", "Dotm")); // 25
        vx_list.add(new VertexS("d2", "Dotm")); // 26

        vx_list.add(new VertexS("r1=r(a1)", "Dotm")); // 25
        vx_list.add(new VertexS("r2=r(a2)", "Dotm")); // 26
        vx_list.add(new VertexS("r3=r(a3)", "Dotm")); // 25
        vx_list.add(new VertexS("r2=r(a4)", "Dotm")); // 26
        Graph<VertexS, String> basis = new SparseMultigraph<VertexS, String>();
        for(VertexS vx: vx_list)
        {
            basis.addVertex(vx);
        }

        basis.addEdge("1", vx_list.get(0), vx_list.get(25));
        basis.addEdge("2", vx_list.get(0), vx_list.get(26));
        basis.addEdge("242", vx_list.get(25), vx_list.get(1));
        basis.addEdge("243", vx_list.get(26), vx_list.get(2));
        basis.addEdge(" 1", vx_list.get(1), vx_list.get(3));
        basis.addEdge(" 2", vx_list.get(1), vx_list.get(4));

        basis.addEdge(" 3", vx_list.get(2), vx_list.get(5));
        basis.addEdge(" 4", vx_list.get(2), vx_list.get(6));

        basis.addEdge(" 11", vx_list.get(7), vx_list.get(8));
        basis.addEdge(" 12", vx_list.get(7), vx_list.get(9));

        basis.addEdge(" 21", vx_list.get(7), vx_list.get(10));
        basis.addEdge(" 22", vx_list.get(7), vx_list.get(11));
        basis.addEdge(" 111", vx_list.get(8), vx_list.get(12));
        basis.addEdge(" 122", vx_list.get(9), vx_list.get(13));

        basis.addEdge(" 213", vx_list.get(10), vx_list.get(15));
        basis.addEdge(" 224", vx_list.get(11), vx_list.get(14));

        basis.addEdge(" 11 ", vx_list.get(16), vx_list.get(17));
        basis.addEdge(" 12 ", vx_list.get(16), vx_list.get(18));

        basis.addEdge(" 21 ", vx_list.get(16), vx_list.get(19));
        basis.addEdge(" 22 ", vx_list.get(16), vx_list.get(20));
        basis.addEdge(" 111 ", vx_list.get(17), vx_list.get(21));
        basis.addEdge(" 122 ", vx_list.get(18), vx_list.get(22));

        basis.addEdge(" 213 ", vx_list.get(19), vx_list.get(23));
        basis.addEdge(" 224 ", vx_list.get(20), vx_list.get(24));
        StaticLayout<VertexS, String> layout = new StaticLayout<VertexS, String>(
                basis, new LocationTransformer(vx_coords));
        layout.setSize(new Dimension(250, 250));
        edu.uci.ics.jung.visualization.VisualizationViewer<VertexS, String> vv = new edu.uci.ics.jung.visualization.VisualizationViewer<VertexS, String>(layout);
        vv.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line<VertexS, String>());
        vv.getRenderContext().setVertexLabelTransformer(new Transformer<VertexS, String>() {
            public String transform(VertexS vx) {
                return vx.name;
            }
        });
       // vv.getRenderContext().setEdgeLabelTransformer(new EdgeLabelTransformer(edge_values));
        vv.getRenderer().setVertexRenderer(new RenderDiagram());
        vv.getRenderer().getVertexLabelRenderer().setPosition(edu.uci.ics.jung.visualization.renderers.Renderer.VertexLabel.Position.N);
        vv.setSize(650, 200);
        last_el_coords.updateValues(offset_y, vv.getHeight());
        vv.setLocation(panel.getWidth() / 2 - vv.getWidth() / 2, last_el_coords.y);
        panel.add(vv);
    }



}
