/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.spb.beavers.modules;

import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Objects;

/**
 *
 * @author ckyxc
 */
public class TerritoryDefenceModule implements ITaskModule {

    private static final String RESOURCE_PATH = "src/main/resources/" + TerritoryDefenceModule.class.getPackage().getName().replace('.', '/') + "/territory_defence/";
    private static final String IMAGES_PATH = RESOURCE_PATH + "images/";
    private static final String TEXT_PATH = RESOURCE_PATH + "text/";
    private static final String fileName = "TaskModule_1_3_input_data.txt";
    JTextField c1Field = new JTextField(20);
    JTextField c2Field = new JTextField(20);
    JTextField c3Field = new JTextField(20);
    JTextField c4Field = new JTextField(20);
    JTextField p1Field = new JTextField(20);

    @Override
    public String getTitle() {
        return "1.3. Задача о защите\nтерритории"; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void initDescriptionPanel(JPanel panel) {
        String htmlContent = "<html>\n"
                + "    <head>\n"
                + "        <title>1.3. Задача о защите территории</title>\n"
                + "        <meta charset=\"UTF-8\">\n"
                + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
                + "    </head>\n"
                + "    <body>\n"
                + "        <h3>Неформальное описание задачи.</h3> \n"
                + "        <table border=\"0\" width=\"800\" ali>\n"
                + "            <tr>\n"
                + "                <td><br>"
                + "                    Задача о защите территории аналогична задаче о защите <br>"
                + "                    посевов, но представлена в постановке, принятой в военном <br>"
                + "                    деле. Пусть некий военачальник несет ответственность за <br>"
                + "                    безопасность вверенной ему территории. Существует угроза <br>"
                + "                    нападения противника на территорию, в этом случае территории<br>"
                + "                    будет нанесен значительный ущерб. Существует возможность <br>"
                + "                    провести оборонительные мероприятия, которые позволят <br>"
                + "                    защитить территорию в случае нападения противника, однако <br>"
                + "                    подобные мероприятия являются дорогостоящими. Военачальнику <br>"
                + "                    требуется выбрать один из двух возможных вариантов <br>"
                + "                    решений – проводить или не проводить мероприятия по защите <br>"
                + "                    территории. При выборе первого варианта решения в случае <br>"
                + "                    нападения противника атака будет успешно отражена и <br>"
                + "                    территории будет причинен незначительный ущерб, в случае <br>"
                + "                    ненападения противника будут затрачены лишние средства на <br>"
                + "                    оборонительные мероприятия. При выборе второго варианта <br>"
                + "                    решения в случае нападения противника территории будет <br>"
                + "                    нанесен значительный ущерб, а в случае ненападения <br>"
                + "                    противника территория не понесет никаких потерь.<br>"
                + "                </td>\n"
                + "            </tr>\n"
                + "        </table>\n"
                + "    </body>\n"
                + "</html>\n"
                + "";
        JLabel htmlLabel = new JLabel();
        htmlLabel.setPreferredSize(new Dimension(750, 400));
        htmlLabel.setText(htmlContent);
        panel.add(htmlLabel);
    }

    @Override
    public void initSolutionPanel(JPanel panel) {
        String htmlContent = "<html>\n"
                + "    <head>\n"
                + "        <title>1.3. Задача о защите территории</title>\n"
                + "        <meta charset=\"UTF-8\">\n"
                + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
                + "        <style>\n"
                + "            p {\n"
                + "                text-align: justify;\n"
                + "                text-indent: 15px;\n"
                + "            }\n"
                + "        </style>\n"
                + "    </head>\n"
                + "    <body>\n"
                + "        <h3>Формальная постановка задачи.</h3> \n"
                + "        <table border=\"0\" width=\"800\">\n"
                + "            <tr>\n"
                + "                <td><br>"
                + "                    <p>Множество решений D = {d<sub>1</sub>, d<sub>2</sub>} , где</p><br>"
                + "                    <p style=\"margin-left:2em;\">d<sub>1</sub> – решение  проводить мероприятия  по  защите  территории  от  нападения;</p><br>"
                + "                    <p style=\"margin-left:2em;\">d<sub>2</sub> – решение не проводить мероприятия по защите территории от нападения.</p><br>"
                + "                    <p style=\"margin-left:2em;\">Поведение  потенциального  врага  описывается  случайной  величиной  Y, <br>"
                + "                        принимающей значения из множества Y = {y<sub>1</sub>, y<sub>2</sub>}, где</p><br>"
                + "                    <p style=\"margin-left:2em;\">y<sub>1</sub> – противник нападет на территорию;</p><br>"
                + "                    <p style=\"margin-left:2em;\">y<sub>2</sub> – противник не нападет на территорию.</p><br>"
                + "                    <p>Вероятность события x<sub>i</sub> обозначим символом p<sub>i</sub>.</p><br>"
                + "                    <p>В зависимости <br>"
                + "                        от принятого военачальником решения и поведения потенциального <br>"
                + "                        противника можно выделить четыре исхода (альтернативы), входящих<br>"
                + "                        в множество исходов (альтернатив) A = {a<sub>1</sub>, a<sub>2</sub>, a<sub>3</sub>, a<sub>4</sub>}, где</p><br>"
                + "                    <p style=\"margin-left:2em;\">a<sub>1</sub> – потенциальный противник осуществил нападение на <br>"
                + "                        территорию, однако территория  защищена, в результате атака <br>"
                + "                        успешно отражена, понесены незначительные потери в ходе военных <br>"
                + "                        действий и расход на оборонительные мероприятия;</p><br>"
                + "                    <p style=\"margin-left:2em;\">a<sub>2</sub> – потенциальный противник не осуществлял нападения, однако <br>"
                + "                        территория была защищена, в результате понесен расход на <br>"
                + "                        оборонительные мероприятия;</p><br>"
                + "                    <p style=\"margin-left:2em;\">a<sub>3</sub> – потенциальный противник осуществил нападение на <br>"
                + "                        территорию, территория не была  защищена, в результате <br>"
                + "                        территории нанесен значительный ущерб;</p><br>"
                + "                    <p style=\"margin-left:2em;\">a<sub>4</sub> – потенциальный противник не осуществлял нападения, <br>"
                + "                        мероприятия по защите территории не проводились, в результате<br>"
                + "                        территория не понесла никаких потерь. </p><br>"
                + "                    <p>Каждому из возможных исходов (альтернатив) a<sub>i</sub> поставим в <br>"
                + "                        соответствие функцию потерь c<sub>i</sub>=c(a<sub>i</sub>), значение которой определяется <br>"
                + "                        размером затрат. Для потерь рассмотренных альтернатив справедливо<br>"
                + "                        следующее соотношение:</p><br>"
                + "                    <p style=\"margin-left:2em;\">c<sub>4</sub> &lt; c<sub>2</sub> &lt; c<sub>1</sub> &lt; c<sub>3</sub>.</p><br>"
                + "                    <h3>Дерево решений</h3><br>"
                + "                    <p><img src=\"file:" + IMAGES_PATH + "tree_general.jpg\" alt=\"Дерево решений\"></p><br>"
                + "                    <h3>Лотереи</h3><br>"
                + "                    <p><img src=\"file:" + IMAGES_PATH + "lotteries.jpg\" alt=\"Лотереи\"></p><br>"
                + "                    <h3>Диаграмма влияния</h3><br>"
                + "                    <p><img src=\"file:" + IMAGES_PATH + "diag_effect.jpg\" alt=\"Диаграмма влияния\"></p>                    <br>"
                + "                    <h3>Определение риска от принятия решения</h3><br>"
                + "                    <p>Для оценки качества принимаемого решения при байесовском <br>"
                + "                        подходе  может  быть  использовано математическое <br>"
                + "                        ожидание потерь, которое в дальнейшем будем называть <br>"
                + "                        риском от принятия решения. Таким образом, риск от <br>"
                + "                        принятия решения определим как R(d) = E<sub>Y,D</sub>[C(y,d)], <br>"
                + "                        где</p><br>"
                + "                    <p style=\"margin-left:2em;\">Е – символ математического ожидания,</p><br>"
                + "                    <p style=\"margin-left:2em;\">y &isin; Y,  d &isin; D, С: YxD → R – <br>"
                + "                        ограниченная вещественнозначная функция потерь, которая <br>"
                + "                        паре (y, d)  или, что то же самое, соответствующей аль-<br>"
                + "                        тернативе сопоставляет некоторый расход C(y, d).</p><br>"
                + "                    <h3>Расчет потерь от решений</h3><br>"
                + "                    <p>Потери от исходов (альтернатив) определяются с учетом <br>"
                + "                        введенных обозначений как</p><br>"
                + "                    <p style=\"margin-left:2em;\">C(y<sub>1</sub>, d<sub>1</sub>) = c(a<sub>1</sub>) = c<sub>1</sub></p><br>"
                + "                    <p style=\"margin-left:2em;\">C(y<sub>2</sub>, d<sub>1</sub>) = c(a<sub>2</sub>) = c<sub>2</sub></p><br>"
                + "                    <p style=\"margin-left:2em;\">C(y<sub>1</sub>, d<sub>2</sub>) = c(a<sub>3</sub>) = c<sub>3</sub></p><br>"
                + "                    <p style=\"margin-left:2em;\">C(y<sub>2</sub>, d<sub>2</sub>) = c(a<sub>4</sub>) = c<sub>4</sub></p><br>"
                + "                    <h3>Соотношние функций ценности и потерь</h3><br>"
                + "                    <p>Для ЛПР(лица, принимающего решение) альтернатива a <br>"
                + "                        почтительнее альтернативы  b, если полезность <br>"
                + "                        альтернативы a больше полезности альтернативы b:</p><br>"
                + "                    <p style=\"margin-left:2em;\">v(a) > v(b) ⇔ a >~ b, &forall;a, b &isin; A</p><br>"
                + "                    <p>Для перехода от функции ценности к функции потерь <br>"
                + "                        подходит любая убывающая функция. Например, c(a<sub>i</sub>) = 100 - v(a<sub>i</sub>).</p><br>"
                + "                    <p>Риск от  решения d<sub>1</sub> или, что то же самое, лотереи <br>"
                + "                        L<sub>1</sub> равен</p><br>"
                + "                    <p style=\"margin-left:2em;\">R(d<sub>1</sub>) = p<sub>1</sub>c<sub>1</sub> + (1 - p<sub>1</sub>)c<sub>2</sub>.</p><br>"
                + "                    <p>Риск от  решения d<sub>2</sub> или, что то же самое, лотереи <br>"
                + "                        L<sub>2</sub> равен</p><br>"
                + "                    <p style=\"margin-left:2em;\">R(d<sub>2</sub>) = p<sub>1</sub>c<sub>3</sub> + (1 - p<sub>1</sub>)c<sub>4</sub>.</p><br>"
                + "                    <h3>Выбор решения</h3><br>"
                + "                    <h4><i>Байесовский подход</i></h4><br>"
                + "                    <p>Выбор решения d* = argmin R(d), где d &isin; В, <br>"
                + "                        минимизирующего математическое ожидание потерь.</p><br>"
                + "                    <p>При наличии всего двух альтернатив лучшее в байесовском <br>"
                + "                        смысле решение находится путем сравнения соответствующих рисков:</p><br>"
                + "                    <p style=\"margin-left:2em;\"><img src=\"file:" + IMAGES_PATH + "bayesian.JPG\" alt=\"Байесовское решение\"><br>"
                + "                        , где для определенности при равенстве  рисков принимается решение d<sub>1</sub>.</p><br>"
                + "                    <p>Поскольку разности между значениями потерь остались теми <br>"
                + "                        же, что были разности между значениями ценностей, то <br>"
                + "                        можно предположить, что и остальные полученные <br>"
                + "                        результаты, в частности байесовское решение, осторожное <br>"
                + "                        решение и вероятность безразличия, останутся прежними.</p><br>"
                + "                    <p>Байесовское решение имеет вид</p><br>"
                + "                    <p><img src=\"file:" + IMAGES_PATH + "bayesian_2.JPG\" alt=\"Байесовское решение_2\"></p><br>"
                + "                    <p>где вероятность  <span style=\"letter-spacing:-6\">p<sup>&ndash;</sup></span><br>"
                + "                        определяется из соотношения</p><br>"
                + "                    <p><span style=\"letter-spacing:-6\">p<sup>&ndash;</sup></span>      c<sub>1</sub><br>"
                + "                        + (1 - <span style=\"letter-spacing:-6\">p<sup>&ndash;</sup></span>       )c<sub>2</sub><br>"
                + "                        = <span style=\"letter-spacing:-6\">p<sup>&ndash;</sup></span>        c<sub>3</sub><br>"
                + "                        + (1 - <span style=\"letter-spacing:-6\">p<sup>&ndash;</sup></span>     )c<sub>4</sub>,</p><br>"
                + "                    <p>после преобразования выражение примет вид</p><br>"
                + "                    <p>c<sub>2</sub> - c<sub>4</sub> = <span style=\"letter-spacing:-6\">p<sup>&ndash;</sup></span><br>"
                + "                        (c<sub>2</sub> - c<sub>1</sub> + c<sub>3</sub> - c<sub>4</sub>)</p><br>"
                + "                    <p>откуда</p><br>"
                + "                    <p><img src=\"file:" + IMAGES_PATH + "indifference.JPG\" alt=\"Вероятность безразличия\"></p><br>"
                + "                    <h4><i>Осторожный подход (максиминный)</i></h4><br>"
                + "                    <p>Предполагает выбор решения δ* = argmin[max C(y, d)],где</p><br>"
                + "                    <p style=\"margin-left:2em;\">функция минимума считается по значениям d &isin; D,</p><br>"
                + "                    <p style=\"margin-left:2em;\">функция максимума считается по значениям y &isin; Y,</p><br>"
                + "                    <p>Такой подход минимизирует потери, при самом неблагоприятном поведении <br>"
                + "                        окружающей среды, в качестве которой в настоящей задаче выступает<br>"
                + "                        поведение противника.</p><br>"
                + "                    <h3>Полезность точной информации для Байесовского подхода</h3><br>"
                + "                    <p>Пусть у военачальника есть возможность получить точные <br>"
                + "                        данные о поведении противника. Полезность точной <br>"
                + "                        информации для сторонника байесовского решения с учетом<br>"
                + "                        результатов, полученных в задаче о защите посевов, равна:</p><br>"
                + "                    <p style=\"margin-left:2em;\"><img src=\"file:" + IMAGES_PATH + "info.JPG\" alt=\"Полезность точной информации\"></p><br>"
                + "                    <p>График функции Δ<sub>R</sub>(p<sub>1</sub>):</p><br>"
                + "                    <p style=\"margin-left:2em;\"><img src=\"file:" + IMAGES_PATH + "info_graph.JPG\" alt=\"Полезность точной информации, график\"></p><br>"
                + "                    <p>При вычислении значения Δ<sub>R</sub> вероятность <br>"
                + "                        нападения противника p<sub>1</sub> считается известной. <br>"
                + "                        Вычисленное значение Δ<sub>R</sub> определяет в <br>"
                + "                        единицах потерь цену точной информации, выше которой <br>"
                + "                        не следует за нее платить. Максимального значения <br>"
                + "                        полезность точной информации достигает при p<sub>1</sub> = <br>"
                + "                        <span style=\"letter-spacing:-6\">p<sup>&ndash;</sup></span>     <br>"
                + "                        , что и определяет ситуацию наибольшей неопределенности, когда <br>"
                + "                        стоимость точной информации наиболее высока.</p><br>"
                + "                    <h3>Полезность точной информации для Осторожного подхода</h3><br>"
                + "                    <p>Для сторонника осторожных решений полезность точной <br>"
                + "                        информации может быть вычислена как</p><br>"
                + "                    <p style=\"margin-left:2em;\">Δ<sub>R</sub> = R(δ*) - R(δ**) <br>"
                + "                        = (1 - p<sub>1</sub>)(c<sub>2</sub> - c<sub>4</sub>).</p><br>"
                + "                    <p>График функции Δ<sub>R</sub>(p<sub>1</sub>):</p><br>"
                + "                    <p style=\"margin-left:2em;\"><img src=\"file:" + IMAGES_PATH + "info_graph_2.JPG\" alt=\"Полезность точной информации, график_2\"></p><br>"
                + "                    <p>Максимального значения полезность точной информации <br>"
                + "                        достигает при p<sub>1</sub> = 0, поскольку при этом даже сторонник <br>"
                + "                        осторожных решений не будет защищать территорию.</p><br>"
                + "                </td>\n"
                + "            </tr>\n"
                + "        </table>\n"
                + "    </body>\n"
                + "</html>\n"
                + "";
        JLabel htmlLabel = new JLabel();
        htmlLabel.setText(htmlContent);
        panel.add(htmlLabel);
    }

    @Override
    public void initInputPanel(JPanel panel) {
        panel.setLayout(null);
        Dimension fieldSize = new Dimension(150, 20);
        Dimension labelSize = new Dimension(200, 20);

        JLabel c1Label = new JLabel("<html>Потери при альтернативе a<sub>1</sub>(c<sub>1</sub>): </html>");
        c1Label.setSize(labelSize);
        c1Label.setLocation(260, 10);

        JLabel c2Label = new JLabel("<html>Потери при альтернативе a<sub>2</sub>(c<sub>2</sub>): </html>");
        c2Label.setSize(labelSize);
        c2Label.setLocation(260, 40);

        JLabel c3Label = new JLabel("<html>Потери при альтернативе a<sub>3</sub>(c<sub>3</sub>): </html>");
        c3Label.setSize(labelSize);
        c3Label.setLocation(260, 70);

        JLabel c4Label = new JLabel("<html>Потери при альтернативе a<sub>4</sub>(c<sub>4</sub>): </html>");
        c4Label.setSize(labelSize);
        c4Label.setLocation(260, 100);

        JLabel p1Label = new JLabel("<html>Вероятность x<sub>1</sub> наступления события y<sub>1</sub>(противник нападает на территорию): </html>");
        p1Label.setSize(new Dimension(500, 20));
        p1Label.setLocation(8, 130);

        c1Field.setSize(fieldSize);
        c1Field.setLocation(450, 10);

        c2Field.setSize(fieldSize);
        c2Field.setLocation(450, 40);

        c3Field.setSize(fieldSize);
        c3Field.setLocation(450, 70);

        c4Field.setSize(fieldSize);
        c4Field.setLocation(450, 100);

        p1Field.setSize(fieldSize);
        p1Field.setLocation(450, 130);

        panel.add(c1Label);
        panel.add(c1Field);

        panel.add(c2Label);
        panel.add(c2Field);

        panel.add(c3Label);
        panel.add(c3Field);

        panel.add(c4Label);
        panel.add(c4Field);

        panel.add(p1Label);
        panel.add(p1Field);
    }

    @Override
    public void initExamplePanel(JPanel panel) {
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        Integer c1 = Integer.parseInt(c1Field.getText());
        Integer c2 = Integer.parseInt(c2Field.getText());
        Integer c3 = Integer.parseInt(c3Field.getText());
        Integer c4 = Integer.parseInt(c4Field.getText());
        Float p1 = Float.valueOf(p1Field.getText());
        Float p2 = 1 - p1;
        Float p = (c2.floatValue() - c4.floatValue()) / (c2.floatValue() - c1.floatValue() + c3.floatValue() - c4.floatValue());
        Float r1 = p1 * c1 + p2 * c2;
        Float r2 = p1 * c3 + p2 * c4;
        String bayesTag;
        if (r1 <= r2) {
            bayesTag = "<p>Т.к. " + r1.toString() + " &le; " + r2.toString() + ", т.е. R<sub>1</sub> &le; R<sub>2</sub>"
                    + "то Байесовское решение d* = d<sub>1</sub></p>";
        } else {
            bayesTag = "<p>Т.к. " + r1.toString() + " &gt; " + r2.toString() + ", т.е. R<sub>1</sub> &gt; R<sub>2</sub>"
                    + "то Байесовское решение d* = d<sub>2</sub></p>";
        }

        JLabel formulaIndiff = new JLabel();
        String latex = "\\overline{p}=\\frac{" + c2.toString() + " - " + c4.toString() + "}"
                + "{" + c2.toString() + " - " + c1.toString() + " + " + c3.toString() + " - " + c4.toString() + "} = " + p.toString();

        TeXFormula formula = new TeXFormula(latex);

        TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 15);

        BufferedImage image = new BufferedImage(icon.getIconWidth() - 15,
                icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2 = image.createGraphics();
        g2.setColor(Color.white);
        g2.fillRect(0, 0, icon.getIconWidth() * 2, icon.getIconHeight());

        JLabel jl = new JLabel();
        jl.setForeground(new Color(0, 0, 0));
        icon.paintIcon(jl, g2, 0, 0);

        formulaIndiff.setIcon(new ImageIcon(image));
        formulaIndiff.setBounds(20, 20, icon.getIconWidth(), icon.getIconHeight());

        JLabel htmlLabel_part1 = new JLabel();
        htmlLabel_part1.setText("<html>\n"
                + "    <head>\n"
                + "        <title>1.3. Задача о защите территории</title>\n"
                + "        <meta charset=\"UTF-8\">\n"
                + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
                + "        <style>\n"
                + "            p {\n"
                + "                text-align: justify;\n"
                + "                text-indent: 15px;\n"
                + "            }\n"
                + "        </style>\n"
                + "    </head>\n"
                + "    <body>\n"
                + "        <h3>Частное решение.</h3> \n"
                + "        <table border=\"0\" width=\"800\">\n"
                + "            <tr>\n"
                + "                <td><br>"
                + "                    <p>Множество решений D = {d<sub>1</sub>, d<sub>2</sub>} , где</p><br>"
                + "                    <p style=\"margin-left:2em;\">d<sub>1</sub> – решение  проводить мероприятия  по  защите  территории  от  нападения;</p><br>"
                + "                    <p style=\"margin-left:2em;\">d<sub>2</sub> – решение не проводить мероприятия по защите территории от нападения.</p><br>"
                + "                    <p style=\"margin-left:2em;\">Поведение  потенциального  врага  описывается  случайной  величиной  Y, <br>"
                + "                        принимающей значения из множества Y = {y<sub>1</sub>, y<sub>2</sub>}, где</p><br>"
                + "                    <p style=\"margin-left:2em;\">y<sub>1</sub> – противник нападет на территорию;</p><br>"
                + "                    <p style=\"margin-left:2em;\">y<sub>2</sub> – противник не нападет на территорию.</p><br>"
                + "                    <p>Вероятность события x<sub>1</sub> обозначим символом p<sub>1</sub> = " + p1 + "</p><br>"
                + "                    <p>А вероятность события x<sub>2</sub> обозначим символом p<sub>2</sub> = 1 - p<sub>1</sub> = " + p2 + "</p><br>"
                + "                    <p>В зависимости <br>"
                + "                        от принятого военачальником решения и поведения потенциального <br>"
                + "                        противника можно выделить четыре исхода (альтернативы), входящих<br>"
                + "                        в множество исходов (альтернатив) A = {a<sub>1</sub>, a<sub>2</sub>, a<sub>3</sub>, a<sub>4</sub>}, где</p><br>"
                + "                    <p style=\"margin-left:2em;\">a<sub>1</sub> – потенциальный противник осуществил нападение на <br>"
                + "                        территорию, однако территория  защищена, в результате атака <br>"
                + "                        успешно отражена, понесены незначительные потери в ходе военных <br>"
                + "                        действий и расход на оборонительные мероприятия;</p><br>"
                + "                    <p style=\"margin-left:2em;\">a<sub>2</sub> – потенциальный противник не осуществлял нападения, однако <br>"
                + "                        территория была защищена, в результате понесен расход на <br>"
                + "                        оборонительные мероприятия;</p><br>"
                + "                    <p style=\"margin-left:2em;\">a<sub>3</sub> – потенциальный противник осуществил нападение на <br>"
                + "                        территорию, территория не была  защищена, в результате <br>"
                + "                        территории нанесен значительный ущерб;</p><br>"
                + "                    <p style=\"margin-left:2em;\">a<sub>4</sub> – потенциальный противник не осуществлял нападения, <br>"
                + "                        мероприятия по защите территории не проводились, в результате<br>"
                + "                        территория не понесла никаких потерь. </p><br>"
                + "                    <p>Каждому из возможных исходов (альтернатив) a<sub>i</sub> поставим в <br>"
                + "                        соответствие функцию потерь c<sub>i</sub>=c(a<sub>i</sub>), значение которой определяется <br>"
                + "                        размером затрат.</p><br>"
                + "                    <p>В нашем случае:</p><br>"
                + "                    <p style=\"margin-left:2em;\">c<sub>1</sub> = " + c1 + "</p><br>"
                + "                    <p style=\"margin-left:2em;\">c<sub>2</sub> = " + c2 + "</p><br>"
                + "                    <p style=\"margin-left:2em;\">c<sub>3</sub> = " + c3 + "</p><br>"
                + "                    <p style=\"margin-left:2em;\">c<sub>4</sub> = " + c4 + "</p><br>"
                + "                    <h3>Дерево решений</h3><br>"
                + "                    <p><img src=\"file:" + IMAGES_PATH + "tree_general.jpg\" alt=\"Дерево решений\"></p><br>"
                + "                    <h3>Лотереи</h3><br>"
                + "                    <p><img src=\"file:" + IMAGES_PATH + "lotteries.jpg\" alt=\"Лотереи\"></p><br>"
                + "                    <h3>Диаграмма влияния</h3><br>"
                + "                    <p><img src=\"file:" + IMAGES_PATH + "diag_effect.jpg\" alt=\"Диаграмма влияния\"></p><br>"
                + "                    <h3>Определение риска от принятия решения</h3><br>"
                + "                    <p>Риск от  решения d<sub>1</sub> или, что то же самое, лотереи <br>"
                + "                        L<sub>1</sub> равен</p><br>"
                + "                    <p style=\"margin-left:2em;\">R(d<sub>1</sub>) = <br>"
                + "                        p<sub>1</sub>c<sub>1</sub> + (1 - p<sub>1</sub>)c<sub>2</sub> = <br>"
                + "                    " + p1 + "*" + c1 + "+" + p2 + "*" + c2 + " = " + r1 + "</p><br>"
                + "                    <p>Риск от  решения d<sub>2</sub> или, что то же самое, лотереи <br>"
                + "                        L<sub>2</sub> равен</p><br>"
                + "                    <p style=\"margin-left:2em;\">R(d<sub>2</sub>) =<br>"
                + "                        p<sub>1</sub>c<sub>3</sub> + (1 - p<sub>1</sub>)c<sub>4</sub> = <br>"
                + "                        " + p1 + "*" + c3 + "+" + p2 + "*" + c4 + " = " + r2 + ".</p><br>"
                + "                    <h3>Выбор решения</h3><br>"
                + "                    <h4><i>Байесовский подход</i></h4><br>"
                + "                    <p>Выбор решения d* = argmin R(d), где d &isin; В, <br>"
                + "                        минимизирующего математическое ожидание потерь.</p><br>"
                + "                    " + bayesTag
                + "                    <p>Вычислим вероятность безразличия</p><br>"
                + "                </td>\n"
                + "            </tr>\n"
                + "        </table>\n"
                + "    </body>\n"
                + "</html>\n");
        panel.add(htmlLabel_part1);
        panel.add(formulaIndiff);

        Integer maxc1c2 = c1 > c2 ? c1 : c2;
        Integer maxc3c4 = c3 > c4 ? c3 : c4;
        Integer argmin = maxc1c2 < maxc3c4 ? maxc1c2 : maxc3c4;
        String maxc1c2Tag;
        String maxc3c4Tag;
        String argminTag;
        String result;

        if (Objects.equals(maxc1c2, c1)) {
            maxc1c2Tag = "c<sub>1</sub>";
        } else {
            maxc1c2Tag = "c<sub>2</sub>";
        }

        if (Objects.equals(maxc3c4, c3)) {
            maxc3c4Tag = "c<sub>3</sub>";
        } else {
            maxc3c4Tag = "c<sub>4</sub>";
        }

        if (Objects.equals(argmin, c1)) {
            argminTag = "c<sub>1</sub>";
            result = "d<sub>1</sub>";
        } else if (Objects.equals(argmin, c2)) {
            argminTag = "c<sub>2</sub>";
            result = "d<sub>1</sub>";
        } else if (Objects.equals(argmin, c3)) {
            argminTag = "c<sub>3</sub>";
            result = "d<sub>2</sub>";
        } else {
            argminTag = "c<sub>4</sub>";
            result = "d<sub>2</sub>";
        }

        JLabel formulaIndiffBayesLabel = new JLabel();
        String latexIndiffBayes = "\\Delta_R = R(d*) - R(d**) = \\left\\{"
                + "\\begin{eqnarray}"
                + "(1 - p_1)(" + c2 + " + " + c4 + "), p_1 \\ge \\overline{p};\\\\"
                + "p_1(" + c3 + " - " + c1 + "),  p_1 < \\overline{p}"
                + "\\end{eqnarray}\\right\\}";
        TeXFormula formulaIndiffBayes = new TeXFormula(latexIndiffBayes);

        TeXIcon iconIndiffBayes = formulaIndiffBayes.createTeXIcon(TeXConstants.STYLE_DISPLAY, 15);

        BufferedImage imageIndiffBayes = new BufferedImage(iconIndiffBayes.getIconWidth() - 15,
                iconIndiffBayes.getIconHeight(), BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2IndiffBayes = imageIndiffBayes.createGraphics();
        g2IndiffBayes.setColor(Color.white);
        g2IndiffBayes.fillRect(0, 0, iconIndiffBayes.getIconWidth() * 2, iconIndiffBayes.getIconHeight());

        JLabel jlIndiffBayes = new JLabel();
        jlIndiffBayes.setForeground(new Color(0, 0, 0));
        iconIndiffBayes.paintIcon(jlIndiffBayes, g2IndiffBayes, 0, 0);

        formulaIndiffBayesLabel.setIcon(new ImageIcon(imageIndiffBayes));
        formulaIndiffBayesLabel.setBounds(20, 20, iconIndiffBayes.getIconWidth(), iconIndiffBayes.getIconHeight());

        JLabel htmlLabel_part2 = new JLabel();
        htmlLabel_part2.setText("<html>\n"
                + "    <head>\n"
                + "        <title>1.3. Задача о защите территории</title>\n"
                + "        <meta charset=\"UTF-8\">\n"
                + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
                + "        <style>\n"
                + "            p {\n"
                + "                text-align: justify;\n"
                + "                text-indent: 15px;\n"
                + "            }\n"
                + "        </style>\n"
                + "    </head>\n"
                + "    <body>\n"
                + "        <table border=\"0\" width=\"800\">\n"
                + "            <tr>\n"
                + "                <td><br>"
                + "                    <h4><i>Осторожный подход (максиминный)</i></h4><br>"
                + "                    <p>Предполагает выбор решения δ* = argmin[max C(y, d)],где</p><br>"
                + "                    <p style=\"margin-left:2em;\">функция минимума считается по значениям d &isin; D,</p><br>"
                + "                    <p style=\"margin-left:2em;\">функция максимума считается по значениям y &isin; Y,</p><br>"
                + "                    <br><p>max[C(y<sub>1</sub>, d<sub>1</sub>), C(y<sub>2</sub>, d<sub>1</sub>)] = " + maxc1c2Tag + " = " + maxc1c2 + "</p><br>"
                + "                    <p>max[C(y<sub>1</sub>, d<sub>2</sub>), C(y<sub>2</sub>, d<sub>2</sub>)] = " + maxc3c4Tag + " = " + maxc3c4 + "</p><br>"
                + "                    <p>argmin( " + maxc1c2Tag + ", " + maxc3c4Tag + " ) = " + argminTag + " = " + argmin + "</p><br>"
                + "                    <p>Получили δ* = " + result + "</p><br>"
                + "                    <h3>Полезность точной информации для Байесовского подхода</h3><br>"
                + "                    <p>Пусть у военачальника есть возможность получить точные <br>"
                + "                        данные о поведении противника. Полезность точной <br>"
                + "                        информации для сторонника байесовского решения равна:</p><br>"
                + "                 </td>\n"
                + "            </tr>\n"
                + "      </table>\n"
                + "    </body>\n"
                + "</html>");
        panel.add(htmlLabel_part2);
        panel.add(formulaIndiffBayesLabel);

        JLabel graphBayesLabel = new JLabel();
        BufferedImage imageGraphBayes = new BufferedImage(250, 150, BufferedImage.TYPE_INT_RGB);
        Graphics g = imageGraphBayes.getGraphics();
        g.setColor(new Color(255, 255, 255));
        g.fillRect(0, 0, 250, 150);
        g.setColor(new Color(0, 0, 0));
        g.drawLine(10, 10, 10, 125);
        g.drawLine(5, 120, 240, 120);
        int y = 50;
        int x = (int) (p * 230);
        g.drawLine(10, 120, x, y);
        g.drawLine(x, y, 230, 120);
        g.drawString("0", 2, 135);
        g.drawString("1", 225, 135);
        g.drawString("p", x, 135);
        g.drawLine(x, 125, x + 5, 125);
        g.drawLine(5, y, 15, y);
        g.drawLine(x, 118, x, 122);

        graphBayesLabel.setIcon(new ImageIcon(imageGraphBayes));
        graphBayesLabel.setBounds(20, 20, 250, 150);

        JLabel htmlLabel_part3 = new JLabel();
        htmlLabel_part3.setText("<html>\n"
                + "    <head>\n"
                + "        <title>1.3. Задача о защите территории</title>\n"
                + "        <meta charset=\"UTF-8\">\n"
                + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
                + "        <style>\n"
                + "            p {\n"
                + "                text-align: justify;\n"
                + "                text-indent: 15px;\n"
                + "            }\n"
                + "        </style>\n"
                + "    </head>\n"
                + "    <body>\n"
                + "        <table border=\"0\" width=\"800\">\n"
                + "            <tr>\n"
                + "                <td>\n"
                + "                      <p>График функции Δ<sub>R</sub>(p<sub>1</sub>):</p>\n"
                + "                 </td>\n"
                + "            </tr>\n"
                + "      </table>\n"
                + "    </body>\n"
                + "</html>");
        panel.add(htmlLabel_part3);
        panel.add(graphBayesLabel);
        
        

        JLabel graphCarryLabel = new JLabel();
        BufferedImage imageGraphCarry = new BufferedImage(180, 130, BufferedImage.TYPE_INT_RGB);
        Graphics g1 = imageGraphCarry.getGraphics();
        g1.setColor(new Color(255, 255, 255));
        g1.fillRect(0, 0, 180, 130);
        g1.setColor(new Color(0, 0, 0));
        g1.drawLine(10, 10, 10, 115);
        g1.drawLine(5, 110, 170, 110);
        g1.drawLine(10, 50, 150, 110);
        g1.drawString("0", 2, 122);
        g1.drawString("1", 150, 122);
        g1.drawString((c2 - c4) + "", 13, 50);

        graphCarryLabel.setIcon(new ImageIcon(imageGraphCarry));
        graphCarryLabel.setBounds(20, 20, 180, 130);

        JLabel htmlLabel_part4 = new JLabel();
        htmlLabel_part4.setText("<html>\n"
                + "    <head>\n"
                + "        <title>1.3. Задача о защите территории</title>\n"
                + "        <meta charset=\"UTF-8\">\n"
                + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
                + "        <style>\n"
                + "            p {\n"
                + "                text-align: justify;\n"
                + "                text-indent: 15px;\n"
                + "            }\n"
                + "        </style>\n"
                + "    </head>\n"
                + "    <body>\n"
                + "    <h3>Полезность точной информации для Осторожного подхода</h3>\n"
                + "        <table border=\"0\" width=\"800\">\n"
                + "            <tr>\n"
                + "                <td>\n"
                + "                      <p style=\"margin-left:2em;\">Δ<sub>R</sub> = R(δ*) - R(δ**) \n"
                + "                        = (1 - p<sub>1</sub>)(" + c2 + " - " + c4 + ") = (1 - p<sub>1</sub>)*" + (c2 - c4) + ".</p>\n"
                + "                      <p>График функции Δ<sub>R</sub>(p<sub>1</sub>):</p>\n"
                + "                 </td>\n"
                + "            </tr>\n"
                + "      </table>\n"
                + "    </body>\n"
                + "</html>");
        panel.add(htmlLabel_part4);
        panel.add(graphCarryLabel);
    }

    @Override
    public ActionListener getPressSaveListener() throws IllegalArgumentException {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evn) {
                File file = new File(TEXT_PATH + fileName);
                try {
                    String c1Data = c1Field.getText();
                    String c2Data = c2Field.getText();
                    String c3Data = c3Field.getText();
                    String c4Data = c4Field.getText();
                    String p1Data = p1Field.getText();

                    if (c1Data.equals("")) {
                        c1Data = "0";
                    }

                    if (c2Data.equals("")) {
                        c2Data = "0";
                    }

                    if (c3Data.equals("")) {
                        c3Data = "0";
                    }

                    if (c4Data.equals("")) {
                        c4Data = "0";
                    }

                    if (p1Data.equals("")) {
                        p1Data = "0";
                    }

                    if (!file.exists()) {
                        file.createNewFile();
                    }
                    PrintWriter out = new PrintWriter(file.getAbsoluteFile());
                    try {
                        out.println(c1Data);
                        out.println(c2Data);
                        out.println(c3Data);
                        out.println(c4Data);
                        out.println(p1Data);
                    } finally {
                        out.close();
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

        };
    }

    @Override
    public ActionListener getPressLoadListener() throws IllegalArgumentException {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evn) {
                StringBuilder sb = new StringBuilder();
                File file = new File(TEXT_PATH + fileName);
                file.exists();
                try {
                    BufferedReader in = new BufferedReader(new FileReader(file.getAbsoluteFile()));
                    try {
                        c1Field.setText(in.readLine());
                        c2Field.setText(in.readLine());
                        c3Field.setText(in.readLine());
                        c4Field.setText(in.readLine());
                        p1Field.setText(in.readLine());
                    } finally {
                        in.close();
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

        };
    }

    @Override
    public ActionListener getDefaultValuesListener() throws IllegalArgumentException {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                c1Field.setText("50");
                c2Field.setText("25");
                c3Field.setText("150");
                c4Field.setText("0");
                p1Field.setText("0.3");
            }

        };
    }

}
