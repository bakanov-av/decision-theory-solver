package ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.GUI.Utility;

public class VertexShape
{
    public String name;
    public String shape;

    public VertexShape(String name, String shape)
    {
        this.name = name;
        this.shape = shape;
    }
}