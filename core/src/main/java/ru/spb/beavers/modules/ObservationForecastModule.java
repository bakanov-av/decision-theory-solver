package ru.spb.beavers.modules;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;
import javax.swing.*;

/**
 * 
 * @author Libi
 */
public class ObservationForecastModule implements ITaskModule {
    private JTextField c1TextField;
    private JTextField c2TextField;
    private JTextField c3TextField;
    private JTextField c4TextField;
    private JTextField kTextField;
    private JTextField p1TextField;
    private JTextField p2TextField;
    private JTextField f1TextField;
    private JTextField f2TextField;
    
    public ObservationForecastModule () {
        c1TextField = new JTextField("50");
        c2TextField = new JTextField("25");
        c3TextField = new JTextField("150");
        c4TextField = new JTextField("0");
        p1TextField = new JTextField("0.33");
        p2TextField = new JTextField("0.67");
        f1TextField = new JTextField("0.8");
        f2TextField = new JTextField("0.75");
        kTextField = new JTextField("100");
        parms = new TaskParms();
    }


    private class TaskParms {
        public TaskParms() {
            income = new float[4];
            losses = new float[4];
            weatherP = new float[4];
            forecastP = new float[4];
        }
        final public static boolean GOOD_WEATHER = true;
        final public static boolean BAD_WEATHER = false;
        final public static boolean PROTECT_SOWINGS = true;
        final public static boolean NOT_PROTECT_SOWINGS = false;

        public boolean weatherState;   // X = {x0, x1}
        public boolean forecast;       // Z = {z0, z1}
        public float   income [];      // C = {c11, c10, c01, c00}
        public float   losses [];      // V = {v11, v10, v01, v00}
        public float   weatherP [];    // P = {p0, p1}
        public float   forecastP [];   // f(z/x) 
    }
    
    private ObservationForecastModule.TaskParms parms = null;
    
    public String getTitle() {
        return "12. Наблюдение\nв форме прогноза";
    }
    
    /**
     * Инициализация элементов на панели, дающей краткое описание решаемой задачи.
     * @param panel Инициализируемая панель.
     */
    @Override
    public void initDescriptionPanel(JPanel panel) {
        panel.removeAll();
        String description_htext = 
           "<html>"                                                           +
           "<div align='center'><h1><font color='66CCF0'>Неформальная постановка задачи</font></h1></div>" +
                "<div align='center'>"                                         +
                    "Имеется площадь посевов, чувствительных к изменениям "    +
                    "погоды. Посевы могут быть защищены от плохой погоды, но " +
                    "это мероприятие<br> требует определенных затрат. Если же "+
                    "посевы не защитить, то, в случае плохой погоды, они "     +
                    "погибнут. Задача заключается в том,<br>чтобы на основе "  +
                    "данных о погоде, предоставленных метеобюро, принять "     +
                    "решение - защищать посевы или нет.            <br>"       +
                "</div>"                                                       +
            "</html>";
        
        panel.add(new JLabel(description_htext));
    }

    @Override
    public void initSolutionPanel(JPanel panel) {
        panel.removeAll();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(new JLabel("<html>" +
            "<div align='center'><h1><font color='9966F0'>Формальная постановка задачи</font></h1></div><p>"+
            "Рассмотрим задачу о защите посевов при наличии наблюдений"+
            "за погодными явлениями и прогнозных значений,"            +
            " предоставляемых<br/> метеобюро. "                        +
            "В качестве возможных состояний природы будем "            +
            "рассматривать множество состояний            <br/> <br/>" +
            "<b>X</b> = {x<sub>0</sub>, x<sub>1</sub>}, где     <br/>" +
            "x<sub>1</sub> - наблюдаются заморозки,             <br/>" +
            "x<sub>0</sub> - заморозков не наблюдается.   <br/> <br/>" +
            "Прогнозные значения описываются множеством   <br/> <br/>" +
            "<b>Z</b> = {z<sub>0</sub>, z<sub>1</sub>}, где     <br/>" +
            "z<sub>1</sub> - прогнозируются заморозки,          <br/>" +
            "z<sub>0</sub> - прогнозируется теплая погода.<br/> <br/>" +
            "Множество решений <b>D</b> включает в себя два "          +
            "возможных решения:                           <br/> <br/>" +
            "<b>D</b> = {d<sub>0</sub>, d<sub>1</sub>}, где     <br/>" +
            "d<sub>1</sub> - решение защищать посевы,           <br/>" +
            "d<sub>0</sub> - решение не защищать посевы   <br/> <br/>" +
            "Запишем задачу в терминах потерь, то есть определим потери"+
            " от принимаемых решений при различных состояниях природы.</p></html>"));
        panel.add(new JLabel(new ImageIcon(getClass().getResource("observation_forecast/images/Task12_t1.png"))));
        panel.add(new JLabel(
            "<html><p><br/><br/><br/>Запишем задачу в терминах доходов, то есть определим доходы"+
            "от принимаемых решений при различных состояниях природы.</p></html>"));
        panel.add(new JLabel(new ImageIcon(getClass().getResource("observation_forecast/images/Task12_t2.png"))));
        panel.add(new JLabel(
            "<html><p><br/>"+
            "Для принятия решения необходимо задать функцию "+
            "правдоподобия f (z / x) , x ? X или вероятность наблюдения "+
            "Z = z при состоянии<br/>природы X = x в случае приборных"+
            "измерений Pr{x / z}, x ? X . При использовании"+
            "байесовского подхода к принятию решений необходимо<br/>"+
            "также знать априорные вероятности p<sub>1</sub> и p<sub>0</sub> состояний "+
            "окружающей среды. Априорная вероятность заморозков p<sub>1</sub> = "+
            "Pr{x = x<sub>1</sub>},<br/>априорная вероятность теплой погоды p<sub>0</sub> ="+
            " Pr{x = x<sub>0</sub>}.<br/><br/>"+
            "Диаграмма влияния представлена на рис. 12.1, а. "+
            "На диаграмме используются следующие обозначения: Px (x) ="+
            " Pr{x}, f (z / x) = Pr{z / x}.<br/>Данная диаграмма неудобна"+
            "для построения дерева решений, поэтому будем рассматривать"+
            "диаграмму, допускающую влияние прогноза<br/>на состояние "+
            "окружающей среды (рис. 12.1, б). </p></html>"));
        panel.add(new JLabel(new ImageIcon(getClass().getResource("observation_forecast/images/Task12_d1.png"))));
        panel.add(new JLabel(
            "<html><p>На диаграмме используются следующие обозначения: f (z) = "+
            "Pr{z}, Px/z (x / z) = Pr{x / z}."+
            "<br/><h2><font color='330090'>Выбор решения:</font></h2>"+
            "<i><b>Байесовское решение.</i></b><br/> Вычислим совместное"+
            "распределение p<sub>x,z</sub> (x, z) случайных величин X и Z : p<sub>x,z</sub> (x, z) "+
            "= Pr{x, z} – в случае дискретных случайных<br/>величин и p<sub>x,z</sub> (x, z) "+
            "= f {z / x}p<sub>x</sub> (x) – в случае непрерывных случайных величин. Тогда</p></html>"));
        panel.add(new JLabel(new ImageIcon(getClass().getResource("observation_forecast/images/Task12_f2.png"))));
        panel.add(new JLabel(
            "<html><p>Вычислим априорное распределение f (z) случайной величины</p></html>"));
        panel.add(new JLabel(new ImageIcon(getClass().getResource("observation_forecast/images/Task12_f4.png"))));
        panel.add(new JLabel(
            "<html><p>Тогда</p></html>"));
        panel.add(new JLabel(new ImageIcon(getClass().getResource("observation_forecast/images/Task12_f5.png"))));
        panel.add(new JLabel(
            "<html><p>Как правило, на практике априорное распределение не вычисляется. "+
            "Выполним проверку корректности полученных значений:</p></html>"));
        panel.add(new JLabel(new ImageIcon(getClass().getResource("observation_forecast/images/Task12_f6.png"))));
        panel.add(new JLabel(
            "<html><p>Вычислим апостериорное распределение p<sub>x/z</sub> (x / z) , "+
            "z?Z случайной величины X по формуле Байеса:</p></html>"));
        panel.add(new JLabel(new ImageIcon(getClass().getResource("observation_forecast/images/Task12_f7.png"))));
        panel.add(new JLabel(
            "<html><p>Тогда</p></html>"));
        panel.add(new JLabel(new ImageIcon(getClass().getResource("observation_forecast/images/Task12_f8.png"))));
        panel.add(new JLabel(
            "<html><p>Выполним проверку корректности полученных значений:</p></html>"));
        panel.add(new JLabel(new ImageIcon(getClass().getResource("observation_forecast/images/Task12_f9.png"))));
        panel.add(new JLabel(
            "<html><p>Дерево решений для байесовского подхода.</p></html>"));
        panel.add(new JLabel(new ImageIcon(getClass().getResource("observation_forecast/images/Task12_d2.png"))));
        panel.add(new JLabel(
            "<html><p><br/>Байесовский риск R(d*) равен: * V1 = R(d*) = Ex/z[C(x,d*/z)]<br/></p></html>"));
        panel.add(new JLabel(
            "<html><p><br/><i><b>Осторожное решение.<b></i><br/>"+
            "Дерево решений для осторожного подхода.</p></html>"));
        panel.add(new JLabel(new ImageIcon(getClass().getResource("observation_forecast/images/Task12_d3.png"))));
        panel.add(new JLabel(
            "<html><p>Вычислим риск от принятия решений d<sub>1</sub> и d<sub>0</sub> :<br/>"+
            "V2 = R(d<sub>1</sub>) = Ex[C(x,d<sub>1</sub>)]<br/>V3 = R(d<sub>0</sub>) = Ex[C(x,d<sub>0</sub>)]<br/>"+
            "Тогда осторожное решение</p></html>"));
        panel.add(new JLabel(new ImageIcon(getClass().getResource("observation_forecast/images/Task12_f10.png"))));
        panel.add(new JLabel(
            "<html><p>Риск от принятия осторожного решения R(?*).<br/>"+
            "Вычислим цену, которую имеет смысл платить метеобюро за прогноз погоды:<br/>"+
            "?v = R(?*) - R(d*), где d* – байесовское решение с прогнозом.<br/>"+
            "Откуда стоимость прогноза не должна превышать ?v.<br/><br/>"+
            "<b>Ценность точной информации</b> для сторонника осторожных решений "+
            "определяется из соотношения ?v = R(d**) - R(?*),<br/>где d** – решение с точной информацией."+
            "<br/>Диаграмма влияния при наличии точной информации представлена на рис. 12.4.<br/>"+
            "Дерево решений при наличии точной информации показано на рис. 12.5.</p></html>"));
        panel.add(new JLabel(new ImageIcon(getClass().getResource("observation_forecast/images/Task12_d4.png"))));
        panel.add(new JLabel(
            "<html><p>Риск от принятия решения при наличии точной информации равен: R(d**) = p<sub>1</sub>C<sub>11</sub> + p<sub>0</sub>C<sub>00</sub>.<br/>"+
            "Тогда ценность точной информации для сторонника осторожных решений ?v = R(d**).<br/>"+
            "Для сторонника байесовских решений ценность точной информации ?v = R(d**) - R(d*),<br/>"+
            " так как точная информация полностью устраняет риск.</p></html>"));
    }
    
    @Override
    public void initInputPanel(JPanel panel) {
        panel.removeAll();
        panel.setLayout(new GridLayout(23,1));
        panel.add(new JLabel("<html><div align='center'><h2><font color='0099F0'>Введите исходные данные</font></h2></div></html>"));
        panel.add(new JLabel("<html><h3>Пусть потери принимают следующие значения</h3></html>"));
        panel.add(new JLabel("при решении защищать посевы от заморозков в случае, когда наблюдаются заморозки:"));
        panel.add(c1TextField);
        panel.add(new JLabel("при решении защищать посевы от заморозков в случае, когда заморозков не наблюдается:"));
        panel.add(c2TextField);
        panel.add(new JLabel("при решении не защищать посевы, когда наблюдаются заморозки:"));
        panel.add(c3TextField);
        panel.add(new JLabel("при решении не защищать посевы, когда заморозков не наблюдается:"));
        panel.add(c4TextField);
        panel.add(new JLabel("Коэффициент функции дохода:"));
        panel.add(kTextField);
        panel.add(new JLabel("<html><h3>Априорная вероятность</h3></html>"));
        panel.add(new JLabel("заморозков:"));
        panel.add(p1TextField);
        panel.add(new JLabel(" теплой погоды:"));
        panel.add(p2TextField);
        panel.add(new JLabel("<html><h3>Вероятность правильнго предсказания местным метеобюро</h3></html>"));
        panel.add(new JLabel("заморозков:"));
        panel.add(f1TextField);
        panel.add(new JLabel("теплой погоды:"));
        panel.add(f2TextField);
    }

    @Override
    public void initExamplePanel(JPanel panel) {
        parms.losses[0] = Float.parseFloat(c1TextField.getText());
        parms.losses[1] = Float.parseFloat(c2TextField.getText());
        parms.losses[2] = Float.parseFloat(c3TextField.getText());
        parms.losses[3] = Float.parseFloat(c4TextField.getText());
        float k = Float.parseFloat(kTextField.getText());
        parms.income[0] = k - parms.losses[0];
        parms.income[1] = k - parms.losses[1];
        parms.income[2] = k - parms.losses[2];
        parms.income[3] = k - parms.losses[3];
        parms.weatherP[0] = Float.parseFloat(p1TextField.getText());
        parms.weatherP[1] = Float.parseFloat(p2TextField.getText());
        parms.forecastP[0] = Float.parseFloat(f1TextField.getText());
        parms.forecastP[1] = Float.parseFloat(f2TextField.getText());
        float f11 = parms.forecastP[1];
        float f00 = parms.forecastP[0];
        float f01 = (1f - parms.forecastP[0]);
        float f10 = 1f - parms.forecastP[0];
        float p11 = ((f11 * parms.weatherP[1])/(f11 * parms.weatherP[1] + f01 * parms.weatherP[0]))  ;
        float p10 = ((f01 * parms.weatherP[0])/((f11 * parms.weatherP[1] + f01 * parms.weatherP[0])));
        float p01 = ((f10 * parms.weatherP[1])/(f10 * parms.weatherP[1] + f00 * parms.weatherP[0])) ;
        float p00 = ((f00 * parms.weatherP[0])/((f11 * parms.weatherP[1] + f00 * parms.weatherP[0]))); 
        float r01 = p11 * parms.losses[2] + p01 * parms.losses[0];
        float r10 = p10 * parms.losses[3] + p00 * parms.losses[1];
        float r00 = p10 * parms.losses[2] + p00 * parms.losses[0];
        float r11 = p11 * parms.losses[3] + p01 * parms.losses[1];
        float bayes = Math.min(Math.min(r11, r10),Math.min(r01, r00));
        float rd0 = parms.weatherP[1] * parms.losses[3] + parms.weatherP[0] * parms.losses[1];
        float rd1 = parms.weatherP[1] * parms.losses[2] + parms.weatherP[0] * parms.losses[0];
        float rdd = parms.weatherP[1] * parms.losses[3] + parms.weatherP[0] * parms.losses[0];
        panel.removeAll();
        boolean parmsValid = true;
        if(parms.weatherP[0] < 0 || parms.weatherP[1] < 0 || parms.forecastP[0] < 0 || parms.forecastP[1] < 0 || 
           parms.weatherP[0] > 1 || parms.weatherP[1] > 1 || parms.forecastP[0] > 1 || parms.forecastP[1] > 1 ||
           parms.weatherP[0] + parms.weatherP[1] != 1) {
                parmsValid = false;
        }
        if(!parmsValid) {
            panel.add(new JLabel("<html><div align='center'<h1><font color='ff003f'>Введены неверные параметры!<br/>"
                    + "Вернитесь назад и повторите ввод значений<br>или нажмите кнопку 'Задать значения по умолчанию'.</font></h1></div></html>"));
            return;
        }
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(new JLabel("<html>" +
            "<div align='center'><h1><font color='9966F0'>Решение</font></h1></div><p>"+
            "Рассмотрим задачу о защите посевов при наличии наблюдений"+
            "за погодными явлениями и прогнозных значений,"            +
            " предоставляемых<br/> метеобюро. "                        +
            "В качестве возможных состояний природы будем "            +
            "рассматривать множество состояний            <br/> <br/>" +
            "<b>X</b> = {x<sub>0</sub>, x<sub>1</sub>}, где     <br/>" +
            "x<sub>1</sub> - наблюдаются заморозки,             <br/>" +
            "x<sub>0</sub> - заморозков не наблюдается.   <br/> <br/>" +
            "Прогнозные значения описываются множеством   <br/> <br/>" +
            "<b>Z</b> = {z<sub>0</sub>, z<sub>1</sub>}, где     <br/>" +
            "z<sub>1</sub> - прогнозируются заморозки,          <br/>" +
            "z<sub>0</sub> - прогнозируется теплая погода.<br/> <br/>" +
            "Множество решений <b>D</b> включает в себя два "          +
            "возможных решения:                           <br/> <br/>" +
            "<b>D</b> = {d<sub>0</sub>, d<sub>1</sub>}, где     <br/>" +
            "d<sub>1</sub> - решение защищать посевы,           <br/>" +
            "d<sub>0</sub> - решение не защищать посевы   <br/> <br/>" +
            "Запишем задачу в терминах потерь, то есть определим потери"+
            " от принимаемых решений при различных состояниях природы:<br/><br/>" +
            "C(x<sub>0</sub>, d<sub>0</sub>) = c<sub>00</sub> = " + parms.losses[0] + ";<br/>" +
            "C(x<sub>0</sub>, d<sub>1</sub>) = c<sub>01</sub> = " + parms.losses[1] + ";<br/>" +
            "C(x<sub>1</sub>, d<sub>0</sub>) = c<sub>10</sub> = " + parms.losses[2] + ";<br/>" +
            "C(x<sub>1</sub>, d<sub>1</sub>) = c<sub>11</sub> = " + parms.losses[3] + ".<br/>" +
            "<br/><br/>Запишем задачу в терминах доходов, то есть определим доходы"+
            "от принимаемых решений при различных состояниях природы.<br/><br/>"+
            "V(x<sub>0</sub>, d<sub>0</sub>) = c<sub>00</sub> = " + k + " - " + parms.losses[0] + " = " + parms.income[0] + ";<br/>" +
            "V(x<sub>0</sub>, d<sub>1</sub>) = c<sub>01</sub> = " + k + " - " + parms.losses[1] + " = " + parms.income[1] + ";<br/>" +
            "V(x<sub>1</sub>, d<sub>0</sub>) = c<sub>10</sub> = " + k + " - " + parms.losses[2] + " = " + parms.income[2] + ";<br/>" +
            "V(x<sub>1</sub>, d<sub>1</sub>) = c<sub>11</sub> = " + k + " - " + parms.losses[3] + " = " + parms.income[3] + ";<br/>" + ".<br/><br/>" +
            "Вероятность теплой погоды p<sub>0</sub> = " + parms.weatherP[0] + ";<br/>" + 
            "Вероятность заморозков p<sub>1</sub> = " + parms.weatherP[1] + ".<br/><br/>" + 
            "Вероятность корректного предсказания теплой погоды: " + parms.forecastP[0] + ";<br/>" + 
            "Вероятность корректного предсказания заморозков: " + parms.forecastP[1] + ".<br/><br/>" + 
            "Функция правдоподобия f (z / x) :<br/> "+
            "f(x<sub>0</sub>, z<sub>0</sub>) = " + f00 + ";<br/>" + 
            "f(x<sub>1</sub>, z<sub>0</sub>) = " + f10 + ";<br/>" + 
            "f(x<sub>1</sub>, z<sub>1</sub>) = " + f11 + ";<br/>" + 
            "f(x<sub>0</sub>, z<sub>1</sub>) = " + f01 + ".<br/><br/>" + 
            "Диаграмма влияния представлена на рис. 12.1, а. "+
            "На диаграмме используются следующие обозначения: Px (x) ="+
            " Pr{x}, f (z / x) = Pr{z / x}.<br/>Данная диаграмма неудобна"+
            "для построения дерева решений, поэтому будем рассматривать"+
            "диаграмму, допускающую влияние прогноза<br/>на состояние "+
            "окружающей среды (рис. 12.1, б). </p></html>"));
        panel.add(new JLabel(new ImageIcon(getClass().getResource("observation_forecast/images/Task12_d1.png"))));
        panel.add(new JLabel(
            "<html><p>На диаграмме используются следующие обозначения: f (z) = "+
            "Pr{z}, Px/z (x / z) = Pr{x / z}."+
            "<br/><h2><font color='330090'>Выбор решения:</font></h2>"+
            "<i><b>Байесовское решение.</i></b><br/> Вычислим совместное"+
            "распределение px,z (x, z) случайных величин X и Z : px,z (x, z) "+
            "= Pr{x, z} – в случае дискретных случайных<br/>величин и px,z (x, z) "+
            "= f {z / x}px (x) – в случае непрерывных случайных величин: <br/><br/>" +
            "p<sub>X,Z</sub>(x<sub>1</sub>, z<sub>1</sub>) = f(x<sub>1</sub>, z<sub>1</sub>)p<sub>1</sub> = " +
            f11 + " * " + parms.weatherP[1] + " = " + (f11 * parms.weatherP[1]) + ";<br>" +
            "p<sub>X,Z</sub>(x<sub>1</sub>, z<sub>0</sub>) = f(x<sub>1</sub>, z<sub>1</sub>)p<sub>1</sub> = " +
            f10 + " * " + parms.weatherP[1] + " = " + (f10 * parms.weatherP[1]) + ";<br>" +
            "p<sub>X,Z</sub>(x<sub>0</sub>, z<sub>1</sub>) = f(x<sub>0</sub>, z<sub>1</sub>)p<sub>0</sub> = " +
            f01 + " * " + parms.weatherP[0] + " = " + (f01 * parms.weatherP[0]) + ";<br>" +
            "p<sub>X,Z</sub>(x<sub>0</sub>, z<sub>0</sub>) = f(x<sub>0</sub>, z<sub>0</sub>)p<sub>0</sub> = " +
            f00 + " * " + parms.weatherP[0] + " = " + (f00 * parms.weatherP[0]) + ".<br><br>" +
            "Вычислим априорное распределение f (z) случайной величины Z:<br/><br/>" + 
            "f(z<sub>0</sub>) = p<sub>X,Z</sub>(x<sub>1</sub>, z<sub>0</sub>) + p<sub>X,Z</sub>(x<sub>0</sub>, z<sub>0</sub>)" + 
            " = " + (f10 * parms.weatherP[1]) + " + " + (f00 * parms.weatherP[0]) + " = " +
            (f10 * parms.weatherP[1] + f00 * parms.weatherP[0]) + ";<br/>" +
            "f(z<sub>1</sub>) = p<sub>X,Z</sub>(x<sub>1</sub>, z<sub>1</sub>) + p<sub>X,Z</sub>(x<sub>0</sub>, z<sub>1</sub>)" + 
            " = " + (f11 * parms.weatherP[1]) + " + " + (f01 * parms.weatherP[0]) + " = " +
            (f11 * parms.weatherP[1] + f01 * parms.weatherP[0]) + ".<br/><br/>" +
            "Вычислим апостериорное распределение px/z (x / z) по формуле Байеса:<br/><br/>" +
            "p<sub>X/Z</sub>(x<sub>1</sub> / z<sub>1</sub>) = " + f11 + " * " + parms.weatherP[1] + " / " +
            (f11 * parms.weatherP[1] + f01 * parms.weatherP[0]) + " = " + 
            ((f11 * parms.weatherP[1])/(f11 * parms.weatherP[1] + f01 * parms.weatherP[0])) + ";<br/>" +
            "p<sub>X/Z</sub>(x<sub>0</sub> / z<sub>1</sub>) = " + f10 + " * " + parms.weatherP[1] + " / " +
            (f10 * parms.weatherP[1] + f00 * parms.weatherP[0]) + " = " + 
            ((f10 * parms.weatherP[1])/(f10 * parms.weatherP[1] + f00 * parms.weatherP[0])) + ";<br/>" +
             "p<sub>X/Z</sub>(x<sub>1</sub> / z<sub>0</sub>) = " + f01 + " * " + parms.weatherP[0] + " / " +
            (f11 * parms.weatherP[1] + f01 * parms.weatherP[0]) + " = " + 
            ((f01 * parms.weatherP[0])/((f11 * parms.weatherP[1] + f01 * parms.weatherP[0]))) +  ";<br/>" +
             "p<sub>X/Z</sub>(x<sub>0</sub> / z<sub>0</sub>) = " + f00 + " * " + parms.weatherP[0] + " / " +
            (f10 * parms.weatherP[1] + f00 * parms.weatherP[0]) + " = " + 
            ((f00 * parms.weatherP[0])/((f11 * parms.weatherP[1] + f00 * parms.weatherP[0]))) + ".<br/><br/>" + 
                
            "Дерево решений для байесовского подхода.</p></html>"));
        panel.add(new JLabel(new ImageIcon(getClass().getResource("observation_forecast/images/Task12_d2.png"))));
        panel.add(new JLabel(
            "<html><p><br/>Рассчитаем Байесовский риск:<br/><br/>" + 
            "R(d<sub>1</sub>, z<sub>1</sub>) = " + r11 + ";<br/>" +
            "R(d<sub>0</sub>, z<sub>1</sub>) = " + r01 + ";<br/>" +
            "R(d<sub>1</sub>, z<sub>0</sub>) = " + r10 + ";<br/>" +
            "R(d<sub>0</sub>, z<sub>0</sub>) = " + r00 + ".<br/><br/>" +
            "Таким образом будет принято решение " + 
            (bayes == r11 || bayes == r10 ? "защитить посевы" : "не защищать посевы.") + "</p></html>"));
        panel.add(new JLabel(
            "<html><p><br/><i><b>Осторожное решение.<b></i><br/>"+
            "Дерево решений для осторожного подхода.</p></html>"));
        panel.add(new JLabel(new ImageIcon(getClass().getResource("observation_forecast/images/Task12_d3.png"))));
        panel.add(new JLabel(
            "<html><p>Вычислим риск от принятия решений d<sub>1</sub> и d<sub>0</sub> :<br/><br/>"+
            "R(d<sub>1</sub>) = " + rd1 + ";<br/>R(d<sub>0</sub>) = " + rd0 + ".<br/><br/>"+
            
            "Таким образом будет принято решение " + 
            (rd1 <= rd0 ? "защитить посевы" : "не защищать посевы.") + "<br/><br/>" + 
            "Риск от принятия осторожного решения R(?*) = " + (rd1 <= rd0 ? rd1 : rd0) + ".<br/>"+
            "Вычислим цену, которую имеет смысл платить метеобюро за прогноз погоды:<br/>"+
            "?v = R(?*) - R(d*) = " + (rd1 <= rd0 ? rd1 : rd0) + " - " + bayes + " = " + 
            (bayes - (rd1 <= rd0 ? rd1 : rd0)) + ", где d* – байесовское решение с прогнозом.<br/>"+
            "Откуда стоимость прогноза не должна превышать " + (bayes - (rd1 <= rd0 ? rd1 : rd0)) + ".<br/><br/>"+
            "<b>Ценность точной информации</b> для сторонника осторожных решений "+
            "определяется из соотношения ?v = R(d**) - R(?*),<br/>где d** – решение с точной информацией."+
            "<br/>Диаграмма влияния при наличии точной информации представлена на рис. 12.4.<br/>"+
            "Дерево решений при наличии точной информации показано на рис. 12.5.</p></html>"));
        panel.add(new JLabel(new ImageIcon(getClass().getResource("observation_forecast/images/Task12_d4.png"))));
        panel.add(new JLabel(
            "<html><p>Риск от принятия решения при наличии точной информации равен: R(d**) = " + rdd + ".<br/>"+
            "Тогда ценность точной информации для сторонника осторожных решений ?v = " + rdd + ".<br/>"+
            "Для сторонника байесовских решений ценность точной информации ?v = " + rdd + " - " + bayes + " = " + (rdd - bayes) + ",<br/>"+
            " так как точная информация полностью устраняет риск.</p></html>"));
    }

    @Override
    public ActionListener getPressSaveListener() throws IllegalArgumentException {
        return new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser saveDialog = new JFileChooser();
                saveDialog.showSaveDialog(null);
                File file = saveDialog.getSelectedFile();
                if(file == null) return;
                PrintWriter out = null;
                try {
                    out = new PrintWriter(file);
                    out.println(c1TextField.getText());
                    out.println(c2TextField.getText());
                    out.println(c3TextField.getText());
                    out.println(c4TextField.getText());
                    out.println(f1TextField.getText());
                    out.println(f2TextField.getText());
                    out.println(p1TextField.getText());
                    out.println(p2TextField.getText());
                    out.println(kTextField.getText());
                    out.close();
                }
                catch (FileNotFoundException ex) { }
                finally {
                    if(out != null) out.close();
                }
            }
        };
    }

    @Override
    public ActionListener getPressLoadListener() throws IllegalArgumentException {
        return new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser openDialog = new JFileChooser();
                openDialog.showOpenDialog(null);
                File file = openDialog.getSelectedFile();
                if(file == null) return;
                Scanner sc = null;
                try {
                    sc = new Scanner(file);
                    c1TextField.setText(sc.nextLine());
                    c2TextField.setText(sc.nextLine());
                    c3TextField.setText(sc.nextLine());
                    c4TextField.setText(sc.nextLine());
                    f1TextField.setText(sc.nextLine());
                    f2TextField.setText(sc.nextLine());
                    p1TextField.setText(sc.nextLine());
                    p2TextField.setText(sc.nextLine());
                    kTextField.setText(sc.nextLine());
                }
                catch (FileNotFoundException ex) { }
                finally {
                if(sc != null) sc.close();
                }
            }
            
        };
    }

    @Override
    public ActionListener getDefaultValuesListener() throws IllegalArgumentException {
        return new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {       
                c1TextField.setText("50");
                c2TextField.setText("25");
                c3TextField.setText("150");
                c4TextField.setText("0");
                p1TextField.setText("0.33");
                p2TextField.setText("0.67");
                f1TextField.setText("0.8");
                f2TextField.setText("0.75");
                kTextField.setText("100");
            }
            
        };
    }
}