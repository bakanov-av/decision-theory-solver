package ru.spb.beavers.modules.Task_3_1_1;

/**
 * Created by Nikita on 10.04.2015.
 */
public class TaskCommon {
    public static final String GLOBAL_TITLE = "3.1.1 Задача о распределении\nресурсов";
    public static final String DESC_TITLE = "<html><h2 align='center'>Неформальное описание задачи</h2></html>";
    public static final String FORMAL_TITLE = "Формальная постановка задачи";
    public static final String USEFULL_TITLE = "Расчет полезностей";
    public static final String SOLUTION_TITLE = "Решение";

    public static final String SPROS_CLASS_TYPE = "Тип спроса: ";
    public static final String SPROS_CLASS_1 = "Спрос на билеты I класса";
    public static final String SPROS_CLASS_2 = "Спрос на билеты II класса";
    public static final String SPROS_SIZE =  "Величина спроса:                   ";
    public static final String COUNT_PLACE = "Количество мест(M):              ";
    public static final String PRICE_CLASS_1 = "Стоимость билетов I класса: ";
    public static final String PRICE_CLASS_2 = "Стоимость билетов II класса: ";


    public static final String DIAGRAMM_NAME = "Диаграмма влияния нашей задачи";
    public static final String DESC_MESSAGE = "Пусть авиакомпания продает билеты " +
            "на плановый рейс самолета. Имеющиеся места в самолете разделяются на" +
            "места первого или второго классов. Стоимость мест второго класса сущест" +
            "венно ниже стоимости мест первого класса, и, следовательно, спрос на места" +
            "первого класса также ниже. Распределение мест первого и второго классов" +
            "осуществляется до момента начала продажи билетов и в дальнейшем " +
            "скорректировано быть не может. Также предполагается, что никто из пасса" +
            "жиров купленные на рейс билеты не сдает. Доход авиакомпании определя" +
            "ется стоимостью проданных билетов. В случае если авиакомпания принимает " +
            "решение выделить мест под первый класс меньше, чем спрос на первый " +
            "класс, то авиакомпания не получит части дохода, который определяется ко" +
            "личеством пассажиров, которым не хватило билетов на места первого класса. " +
            "В случае если авиакомпания принимает решение выделить мест под первый" +
            "класс больше, чем спрос на места первого класса, то часть билетов останутся" +
            "не проданными и авиакомпания понесет убытки. Задача сводится к выбору" +
            "соотношения между местами первого и второго классов, при котором" +
            "авиакомпания получит максимальный доход от продажи билетов.";



    public static final String PIC_AIRPLANE ="task_3_1/airplane.jpg";


    public static final String PIC_FORMAL = "task_3_1/task_3_1_1_diagramm.png";
    public static final String PIC_USEFULL = "task_3_1/task_3_1_1_useful.png";
    public static final String PIC_SOLUTION = "task_3_1/task_3_1_1_solution.png";
    public static final String PIC_RESOLVE_1 = "task_3_1/task_3_1_1_solve_1.png";
    public static final String PIC_RESOLVE_2 = "task_3_1/task_3_1_1_solve_2.png";
    public static final String PIC_RESOLVE_USE_FORMULA = "task_3_1/task_3_1_1_formulaUse.png";
    public static final String PIC_RESOLVE_USE_FORMULA2 = "task_3_1/task_3_1_1_formulaUse2.png";



}
