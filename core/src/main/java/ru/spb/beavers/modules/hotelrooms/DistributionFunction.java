package ru.spb.beavers.modules.hotelrooms;


/**
 * Contains examples of the distribution functions
 * @author AntoVita
 *
 */
public enum DistributionFunction {	
	NORMAL1 {
		@Override
		public double f(double x) {
			mParam = mParam == null ? 0 : mParam;
			
			double arg = mParam / 3.0;
			double value = Math.exp(-(x - mParam)*(x-mParam) / (arg*arg *  2));
			System.out.println(value);
			return value;
		}
		
		@Override
		public String toString() {
			return "Нормальное распределение 1";
		}		
	}, 	NORMAL2 {
		@Override
		public double f(double x) {
			mParam = mParam == null ? 0 : mParam;
			
			double arg = mParam / 4.0 ;
			double value = Math.exp(-(x - mParam)*(x-mParam) / (arg*arg *  2));
					 // /(arg * Math.sqrt(2 * Math.PI));
			System.out.println(value);
			return value;
		}
		
		@Override
		public String toString() {
			return "Нормальное распределение 2";
		}
	}/*, 	EXP {
		@Override
		public double f(double x) {
			mParam = mParam == null ? 0 : mParam;
			
			double arg = 5.0 / mParam;
			double value = Math.exp(- arg * x);
			System.out.println(value);
			return value;
		}
		
		@Override
		public String toString() {
			return "Экспоненциальное распределение";
		}
	}*/;
		
	private static Double mParam = 100.0;
	public abstract double f(double x);
	public void setParam(double param) {
		mParam = param;
	}
}