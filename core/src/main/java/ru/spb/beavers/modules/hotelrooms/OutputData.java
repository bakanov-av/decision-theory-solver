package ru.spb.beavers.modules.hotelrooms;

import java.util.List;


/**
 * Contains output data
 * @author AntoVita
 *
 */
public class OutputData {
	public OutputData(){}
	public int d;
	public double Vd;
	public List<Point> coordinates;
}
