package ru.spb.beavers.modules.protection_of_crops;

import java.awt.Dimension;

public class Constants {
    public static final Dimension TEXT_FIELD_SIZE = new Dimension(800, 30);
    public static final Dimension CHART_SIZE = new Dimension(800, 300);
    public static final String TITLE = "1.2 Задача о защите посевов";
    public static final String RESOURCE_PATH = "./ru/spb/beavers/modules/protection_of_crops/";
    public static final String DESCRIPTION_FILE = "description.res";
    public static final String EXAPMLE_FILE = "example.res";
    public static final String SOLUTION_FILE1 = "solution_1.res";
    public static final String SOLUTION_FILE2 = "solution_2.res";
    public static final String DIAGRAM_FILE = "diagram.png";
    public static final String TREE_FILE = "tree.png";
    public static final float  CHART_STEP = 0.001f;
    
    public static final String INPUT_PANEL_TITLE = 
            "<html><h2 align='center'>Введите исходные данные</h2></html>";  
	
    public static final String DIAGRAM_TEXT = 
            "<html><br/><b>Диаграмма влияния:</b><br/><br/></html>";
    
    public static final String V1_INPUT_STRING = 
            "<html>Доход v<sub>1</sub> (наблюдаются заморозки, однако посевы защищены," + 
            "в результате посевы сохранены частично, фермер получает низкий доход): </html>";
    public static final String V2_INPUT_STRING = 
            "<html>Доход v<sub>2</sub> (заморозки не наблюдаются, при этом посевы " + 
            "защищены, в результате посевы сохранены полностью, фермер получает средний доход): </html>";
    public static final String V3_INPUT_STRING = 
            "<html>Доход v<sub>3</sub> (наблюдаются заморозки, но посевы не защищены, " + 
            "в результате посевы погибают полностью, фермер несет большие убытки): </html>";
    public static final String V4_INPUT_STRING = 
            "<html>Доход v<sub>4</sub> (заморозки не наблюдаются и посевы не защищены, " + 
            "в результате посевы сохранены полностью, фермер получает высокий доход): </html>";
    public static final String P1_INPUT_STRING = 
            "<html>Вероятность p<sub>1</sub> (вероятность заморозков): </html>";
    
    public static final String PARMS_ERROR_STRING = 
            "<html><h2><font color='#FF9B9B'>Исходные данные заданы не полностью" +
            " либо содержат ошибки</font></h2></html>";
}
