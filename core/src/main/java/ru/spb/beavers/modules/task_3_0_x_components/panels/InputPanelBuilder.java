package ru.spb.beavers.modules.task_3_0_x_components.panels;

import edu.uci.ics.jung.algorithms.layout.DAGLayout;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.*;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import ru.spb.beavers.modules.task_3_0_x_components.graph.GraphContainer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Vladimir_ermakov on 4/11/2015.
 */
public class InputPanelBuilder{
    private VisualizationViewer visualizationServer;
    private JButton addVertexButton;
    private JButton addEdgeButton;
    private JButton clearButton;

    private JList<String> vertexList;
    private JList<String> edgeList;
    private DefaultListModel  vertexModel;
    private DefaultListModel edgeModel;

    private JComboBox vertexToBox;
    private JComboBox vertexFromBox;

    private DefaultComboBoxModel vertexToBoxModel;
    private DefaultComboBoxModel vertexFromBoxModel;




    private final static String edgeName = "e";
    private final static String vertexName = "v";

    private GraphContainer graphContainer;

    public InputPanelBuilder(){
        graphContainer = GraphContainer.getInstance();
        initButtons();
        initLists();
        initComboBoxes();
        initVisualisationServer();
    }

    public void initPanel(JPanel panel){
        panel.setLayout(null);

        addVertexButton.setBounds(10, 10, 100, 40);
        addEdgeButton.setBounds(110, 10, 100, 40);
        vertexToBox.setBounds(160, 60, 40, 40);
        vertexFromBox.setBounds(110, 60, 40, 40);
        clearButton.setBounds(10, 60, 100, 50);

        JScrollPane scrollPane1 = new JScrollPane();
        JScrollPane scrollPane2 = new JScrollPane();
        scrollPane1.setViewportView(vertexList);
        scrollPane2.setViewportView(edgeList);
        scrollPane1.setBounds(10, 180, 50, 200);
        scrollPane2.setBounds(110, 180, 100, 200);
        JLabel label1 = new JLabel("Узлы графа.");
        JLabel label2 = new JLabel("Дуги графа.");
        label1.setBounds(10, 120, 100, 50);
        label2.setBounds(110, 120, 100, 50);

        panel.add(addEdgeButton);
        panel.add(addVertexButton);
        panel.add(scrollPane1);
        panel.add(scrollPane2);
        panel.add(vertexToBox);
        panel.add(vertexFromBox);
        panel.add(visualizationServer);
        panel.add(clearButton);
        panel.add(label1);
        panel.add(label2);
    }

    private void initButtons(){
        addVertexButton = new JButton("Добавить вершину.");
        addVertexButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!addEdgeButton.isVisible()){
                    addEdgeButton.setVisible(true);
                }
                graphContainer.addVertexToGraph(vertexName + graphContainer.getLastVertexId());
                vertexModel.addElement(vertexName + (graphContainer.getLastVertexId() - 1));
                vertexToBoxModel.addElement(vertexName + (graphContainer.getLastVertexId() - 1));
                vertexFromBoxModel.addElement(vertexName + (graphContainer.getLastVertexId() - 1));
                visualizationServer.repaint();
            }
        });


        addEdgeButton = new JButton("Добавить дугу.");
        addEdgeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String edge = edgeName + graphContainer.getLastEdgeId();
                String fromVertex = (String) vertexFromBoxModel.getSelectedItem();
                String toVertex = (String) vertexToBoxModel.getSelectedItem();
                graphContainer.addEdgeToGraph(edge, fromVertex, toVertex);
                visualizationServer.repaint();
                if(!edge.equals(edgeName+graphContainer.getLastEdgeId())){
                    edgeModel.addElement(edge + " : " + fromVertex + " -> " + toVertex);
                }
            }
        });

        clearButton = new JButton("Отчистить ввод.");
        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                java.util.List<String> edges = new ArrayList<>();
                edges.addAll(graphContainer.getGraph().getEdges());
                for (String edge : edges) {
                    graphContainer.getGraph().removeEdge(edge);
                }
                java.util.List<String> vertexses = new ArrayList<>();
                vertexses.addAll(graphContainer.getGraph().getVertices());
                for (String vertexse : vertexses) {
                    graphContainer.getGraph().removeVertex(vertexse);
                }
                vertexToBox.removeAllItems();
                vertexFromBox.removeAllItems();
                graphContainer.getCorrespondenceMap().clear();
                vertexModel.clear();
                edgeModel.clear();
                visualizationServer.repaint();
                addEdgeButton.setVisible(false);

            }
        });
        addEdgeButton.setVisible(false);
        addVertexButton.setVisible(true);
    }

    private void initLists(){
        vertexModel = new DefaultListModel();
        vertexList = new JList<>(vertexModel);

        edgeModel = new DefaultListModel();
        edgeList = new JList<>(edgeModel);

        for (String v : graphContainer.getGraph().getVertices()) {
            vertexModel.addElement(v);
        }

        for (String v : graphContainer.getGraph().getEdges()) {
            edgeModel.addElement(v);
        }
    }

    private void initComboBoxes(){
        vertexToBox = new JComboBox();
        vertexFromBox = new JComboBox();

        vertexToBoxModel = new DefaultComboBoxModel();
        vertexFromBoxModel = new DefaultComboBoxModel();

        vertexToBox.setModel(vertexToBoxModel);
        vertexFromBox.setModel(vertexFromBoxModel);

    }

    private void initVisualisationServer(){
        ScalingControl scaler = new CrossoverScalingControl();

        VisualizationViewer server = new VisualizationViewer(new DAGLayout(graphContainer.getGraph()), new Dimension(500, 500));
        server.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
        server.setVisible(true);
        visualizationServer = server;


        DefaultModalGraphMouse gm = new DefaultModalGraphMouse();
        gm.setMode(ModalGraphMouse.Mode.TRANSFORMING);

        visualizationServer.setGraphMouse(gm);

        scaler.scale(server, 0.7f,  server.getCenter());

        visualizationServer.setBounds(250, -100, 800, 800);

    }
}
