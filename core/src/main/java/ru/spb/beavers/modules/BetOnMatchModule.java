/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.spb.beavers.modules;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;

/**
 *
 * @author zavgoroa
 */
public class BetOnMatchModule implements ITaskModule {
    String resourcePath = "TaskModule_1_4_3_resources\\";
    String fileName = "TaskModule_1_4_3_input_data.txt";
    private final JTextField jf_v = new JTextField(8);
    private final JTextField jf_p11 = new JTextField(8);
    private final JTextField jf_p12 = new JTextField(8);
    private final JTextField jf_pc = new JTextField(8);
    private final JTextField platesh1 = new JTextField(8);
    private final JTextField platesh2 = new JTextField(8);
    private final JTextField platesh3 = new JTextField(8);
    JCheckBox cb1 = new JCheckBox("Да", false);
    JCheckBox cb2 = new JCheckBox("Нет", true);
    JCheckBox cb3 = new JCheckBox("Гол", true);
    JCheckBox cb4 = new JCheckBox("Тачдаун", false);
    
    public class TaskParams {
        Double task_v2;
        Double task_p11;
        Double task_p12;
        Double task_pc;
        Double task_v1;
        Double task_v3;
        Double task_v4;
        Double task_v5;
        Double task_v6;

        Double task_p11_;
        Double task_p12_;
        
        public TaskParams() {
            this.task_v2 = 0.0;
            this.task_p11 = 0.0;
            this.task_p12 = 0.0;
            this.task_pc = 0.0;
            this.task_v1 = 0.0;
            this.task_v2 = 0.0;
            this.task_v3 = 0.0;
            this.task_v4 = 0.0;
            this.task_v5 = 0.0;
            this.task_v6 = 0.0;
            this.task_p11_ = 0.0;
            this.task_p12_ = 0.0;
        }

        public String showParams() {
            String str = "<html><span style='font-size:11px'>v2 = " + task_v2.toString() +"<br>pc = " + task_pc.toString() + "<br>p11 = " + task_p11.toString() + "<br>p12 = " + task_p12.toString()
                    + "<br>v1 = " + task_v1.toString() + "<br>v3 = " + task_v3.toString() + "<br>v4 = " + task_v4.toString() + "<br>v5 = " + task_v5.toString() + "<br>v6 = " + task_v6.toString() + "</span></html>";
            return str;
        }
    }

    @Override
    public String getTitle() {
        return "1.4.3. Пари на исход матча\nс учетом величины ставок";
    }

    @Override
    public void initDescriptionPanel(JPanel panel) {
        panel.removeAll();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        JLabel descriptionOne = new JLabel();
        String moformaldesc = "<html><span style='font-size:11px'><b>Неформальная постановка задачи.</b>"
                + "<br>Друзья, Нэнси и Рон, наблюдая за футбольным матчем, решили заключить пари на исход матча после того, как<br>"
                + "тренер примет решение попытаться забить гол или совершить еще один тачдаун. <br>"
                + "Нэнси предлагает следующие ставки Рону, и он соглашается:– если тренер решится на попытку тачдауна и его команда победит,<br>"
                + "Рон должен Нэнси $platesh1, если же не выиграют, тогда Нэнси платит Рону $platesh2;<br>"
                + "– если тренер принимает решение попробовать забить гол и команда выигрывает,<br>"
                + "Рон платит Нэнси $v, если проигрывают, то Нэнси платит $platesh3.<br>"
                + "Сравним две ставки для различных значений v, чтобы понять, влияет ли это значение на принимаемое решение или нет.<br><br></span></html>";
        descriptionOne.setText(moformaldesc);
        panel.add(descriptionOne);
    }

    @Override
    public void initSolutionPanel(JPanel panel) {
        panel.removeAll();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        String noFormalDesc1 = "<html><span style='font-size:11px'><b>Формальная постановка задачи.</b><br>"
                + "Множество решений D = {d<sub>1</sub>, d<sub>2</sub>} соответствует решению Нэнси,где<br>"
                + "d<sub>1</sub> – решение делать ставку;<br>"
                + "d<sub>2</sub> – решение не делать ставку.<br>"
                + "Случайная величина Y = {y<sub>1</sub>, y<sub>2</sub>}, где<br>"
                + "y<sub>1</sub> – тренер принял решение попытаться забить гол и<br>"
                + "y<sub>2</sub> – тренер принял решение совершить тачдаун,<br>"
                + "X = {x<sub>1</sub>, x<sub>2</sub>}, где<br>"
                + "x<sub>1</sub> – команда победила<br>"
                + "x<sub>2</sub> – команда проиграла.<br>"
                + "Принятое решение описывается величиной D, принимающей значения из множества D.<br>"
                + "Вероятность события x<sub>i</sub> в случае принятия тренером решения y<sub>j</sub> будем обозначать как <br>"
                + "p<sub>ij</sub> = Pr{X = x<sub>i</sub> /Y = y<sub>j</sub>}, при этом p<sub>11</sub> &gt; p<sub>12</sub> , так как шанc <br>"
                + "заработать одно очко гораздо выше, чем шанс заработать два очка.<br>"
                + "В зависимости от принятого решения Нэнси, тренера и его результата<br>"
                + "можно выделить шесть исходов (альтернатив), входящих во множество исходов<br>"
                + "(альтернатив) A = {a<sub>1</sub>, a<sub>2</sub>, a<sub>3</sub>, a<sub>4</sub>, a<sub>5</sub>, a<sub>6</sub>},<br>"
                + "где a<sub>1</sub> – тренер принял решение попытаться забить гол, Нэнси решила не делать ставку;<br>"
                + "a<sub>2</sub> – тренер принял решение попытаться забить гол, Нэнси сделала ставку, команда победила;<br>"
                + "a<sub>3</sub> – тренер принял решение попытаться забить гол, Нэнси сделала ставку, команда проиграла;<br>"
                + "a<sub>4</sub> – тренер принял решение совершить тачдаун, Нэнси решила не делать ставку;<br>"
                + "a<sub>5</sub> – тренер принял решение совершить тачдаун, Нэнси сделала ставку, команда победила;<br>"
                + "a<sub>6</sub> – тренер принял решение совершить тачдаун, Нэнси сделала ставку, команда проиграла.<br>"
                + "Каждому из возможных исходов a<sub>i</sub> поставим в соответствие ценность исхода (альтернативы) v<sub>i</sub> = v(a<sub>i</sub>)."
                + "<br>Очевидно, что v должно быть меньше $platesh1 (шанс заработать одно очко гораздо выше,<br> "
                + "чем шанс получить два очка, разумно на результат с более высоким процентом выигрыша поставить меньший коэффициент).<br>"
                + "Также известно, что каждая из условных вероятностей должна находиться в интервале между 0 и 1, таким образом, имеем:<br>"
                + "<b>0 &le; p<sub>12</sub> &le; p<sub>11</sub> &le; 1</b>.</span></html>";
        JLabel lbDesc1 = new JLabel();
        lbDesc1.setText(noFormalDesc1);
        panel.add(lbDesc1);

        String treeDesc = "<html><span style='font-size:11px'><br><b>Дерево решений.<br><br></b></span></html";
        JLabel treeLb = new JLabel();
        treeLb.setText(treeDesc);
        panel.add(treeLb);
        MyGraph tree = new MyGraph(null, 1);
        panel.add(tree);

        String poleznost = "<html><span style='font-size:11px'><br><br><b>Расчет полезностей.</b><br>"
                + "<b>Ценности исходов (альтернатив) с учетом новой введенной переменной определяются так:</b><br>"
                + "V(y<sub>1</sub>,d<sub>2</sub>)=v(a<sub>1</sub>)=v<sub>1</sub> = 0;<br>"
                + "V(y<sub>1</sub>,d<sub>1</sub>,x<sub>1</sub>)=v(a<sub>2</sub>)=v<sub>2</sub> = $v;<br>"
                + "V(y<sub>1</sub>,d<sub>1</sub>,x<sub>2</sub>)=v(a<sub>3</sub>)=v<sub>3</sub> = -$platesh3;<br>"
                + "V(y<sub>2</sub>,d<sub>2</sub>)=v(a<sub>4</sub>)=v<sub>4</sub> = 0;<br>"
                + "V(y<sub>2</sub>,d<sub>1</sub>,x<sub>1</sub>)=v(a<sub>5</sub>)=v<sub>5</sub> = $platesh1;<br>"
                + "V(y<sub>2</sub>,d<sub>1</sub>,x<sub>2</sub>)=v(a<sub>6</sub>)=v<sub>6</sub> = -$platesh2.<br>"
                + "<br><b>Полезности решений принимают следующий вид:</b>"
                //+ "при Y=y<sub>1</sub>:<br> <b>u(d<sub>1</sub>)=p<sub>11</sub>v<sub>2</sub>+p<sub>21</sub>v<sub>3</sub>+p<sub>11</sub>v<sub>2</sub>+(1-p<sub>11</sub>)*v<sub>3</sub>=(v<sub>2</sub>-v<sub>3</sub>)*p<sub>11</sub>+v<sub>3</sub>,<br> u(d<sub>2</sub>)=v<sub>1</sub>=0 ;</b><br>"
                //+ "при Y=y<sub>2</sub>:<br> <b>u(d<sub>1</sub>)=p<sub>12</sub>v<sub>5</sub>+p<sub>22</sub>v<sub>6</sub>=p<sub>12</sub>*v<sub>5</sub>+(1-p<sub>12</sub>)*v<sub>6</sub>=(v<sub>5</sub>-v<sub>6</sub>)*p<sub>12</sub>+v<sub>6</sub>,<br> u(d<sub>2</sub>)=v<sub>4</sub>=0.</b><br>"
                + "<br></span></html";
        String formula = "Y=y_1: u(d_1)=p_{11}*v_2+p_{21}*v_3=p_{11}*v_2+(1-p_{11})*v_3=(v_2-v_3)*p_{11}+v_3, u(d_2)=v_1=0";
        String formula2 = "Y=y_2: u(d_1)=p_{12}*v_5+p_{22}*v_6=p_{12}*v_5+(1-p_{12})*v_6=(v_5-v_6)*p_{12}+v_6, u(d_2)=v_4=0";
        JLabel poleznostLb = new JLabel();
        poleznostLb.setText(poleznost);
        panel.add(poleznostLb);
        panel.add(getFormula(formula));
        panel.add(getFormula(formula2));
        String solutionTextP1 = "<html><span style='font-size:11px'><br><b>Выбор решения.</b><br>"
                + "Нэнси считает, что p<sub>12</sub> &le; p<sub>11</sub>. Более того, если она хочет, чтобы обе ставки имели неотрицательное математическое ожидание<br>"
                + "(получаемый ею доход), должны быть выполнены следующие неравенства:</span></html><br>";        
        String solutionTextP11 = "<html><span style='font-size:11px'>, где p'<sub>11</sub>, p'<sub>12</sub> – вероятности безразличия, т. е. критические значения вероятностей p<sub>11</sub>, p<sub>12</sub> ,<br>"
                + "при которых уже нет разницы:заключать пари или нет. Если построить график значений p<sub>11</sub>, p<sub>12</sub> ,<br>"
                + "которые удовлетворяют вышеприведенным неравенствам, то получим область I (рис. 1.29).<br></span></html>";
        String solutionTextP2 = "<html><span style='font-size:11px'>Любые комбинации p<sub>11</sub>, p<sub>12</sub> в этой области дают положительный ожидаемый доход.<br>"
                + "Таким образом, если p<sub>11</sub>, p<sub>12</sub> попадают в область I, то оптимальное решение не зависит от решения тренера,<br>"
                + "доход всегда будет положительным. Если p<sub>11</sub>, p<sub>12</sub> попадают в область II,<br>"
                + "то ожидаемый доход будет положительным только в том случае, если тренер решит попробовать забить гол;<br>"
                + "в область III – если решит совершить тачдаун. Полагаем, что Нэнси знает решение тренера.<br>"
                + "Тогда области I–III можно объединить в одну. Оставшаяся область IV области допустимых значений включает те значения<br>"
                + "p<sub>11</sub>, p<sub>12</sub>, при которых ожидаемый доход всегда будет отрицательным, вне зависимости от того, что решит тренер.<br>"
                + "В итоге, имеем:<br>"
                + "– область I: если Y = y<sub>1</sub> или Y = y<sub>2</sub> – заключать пари;<br>"
                + "– область II: если Y = y<sub>1</sub> – заключать пари, если Y = y<sub>2</sub> – не заключать;<br>"
                + "– область III: если Y = y<sub>1</sub> – не заключать пари, если Y = y<sub>2</sub> – заключать;<br>"
                + "– область IV: если Y = y<sub>1</sub> или Y = y<sub>2</sub> – не заключать пари.<br>"
                + "Обратите внимание, что при v > $platesh1 нет области III; любая комбинация p<sub>11</sub>, p<sub>12</sub> (с учетом 0 &le; p<sub>12</sub> &le; p<sub>11</sub> &le; 1),<br>"
                + "которая дает положительный доход, если тренер решает попытаться совершить тачдаун,<br>"
                + "также дает положительный доход и при решении тренера попробовать забить гол.<br>"
                + "Теперь рассмотрим более подробно области с положительным ожидаемым доходом (рис. 1.30).<br></spa></html>";
        String solutionTextP3 = "<html><span style='font-size:11px'>В области I решение тренера не влияет на решение Нэнси, при этом</span></html>";
        String solutionTextP31 = "<html><span style='font-size:11px'>Это «уравнение безразличия» показано на рис. 1.29 в виде прямой линии</span></html>"; 
        String solutionTextP32 = "<html><span style='font-size:11px'>проходящей через левый нижний угол области I. Точки ( p<sub>11</sub>, p<sub>12</sub> ) ,<br>"
                + "лежащие на одной из сплошных линий областей I–III и имеющие одинаковый ожидаемый доход,<br>"
                + "дают значение вероятности принятия тренером решения попытаться забить гол p<sub>c</sub> = Pr{Y = y<sub>1</sub>}.<br>"
                + "Эти линии называются изоконтурами. Оптимальным ожидаемым доходом в области IV является 0,<br>"
                + "в области III – линия, параллельная оси p<sub>11</sub>, в области II – линия, параллельная оси p<sub>12</sub> ,<br>"
                + "а в области I – линия, возрастающая с ростом p<sub>11</sub> и убывающая по p<sub>12</sub> .<br>"
                + "Если Нэнси перед заключением пари не знает о решении тренера, она должна оценить вероятность<br>"
                + "принятия тренером того или иного решения.Предполагается, что ее решение будет состоять в том,<br>"
                + "что бы заключать пари только в том случае, если ожидаемый доход будет неотрицательным.<br>"
                + "Напомним, что p<sub>c</sub> = Pr{Y = y<sub>1</sub>} – вероятность принятия тренером решения попытаться забить гол.Тогда<br></span></html>";
        String solutionTextP4 = "<html><span style='font-size:11px'>Нэнси заключит пари, только если первое выражение будет больше нуля;<br>"
                + "ее решение, зависит не только от значения p<sub>с</sub> , но и от p<sub>11</sub> и p<sub>12</sub>.<br><br></span></html>";
        JLabel solutionLb1 = new JLabel();
        JLabel solutionLb11 = new JLabel();
        JLabel solutionLb2 = new JLabel();
        JLabel solutionLb3 = new JLabel();
        JLabel solutionLb31 = new JLabel();
        JLabel solutionLb32 = new JLabel();
        JLabel solutionLb4 = new JLabel();
        String formula4 = "u(d_1) = p_c[(v_2-v_3)*p_{11}+v_3]+(1 - p_c)*[(v_5-v_6)*p_{12}+v_6],  u(d_2) = 0";
        String formula5 = "p_{12} = ((v_2 - v_3)*p_{11}+ v_3 - v_6)/(v_5 - v_6)";
        String formula6 = "(v_6*(v_3-v_2))/(v_3*(v_6-v_5))";
        String formula7 = "p_{11} \\geq p'_{11} = v_3/(v_3-v_2)";
        String formula8 = "p_{12} \\geq p'_{12}=v_6/(v_6-v_5)";
                
        solutionLb1.setText(solutionTextP1);
        solutionLb11.setText(solutionTextP11);
        solutionLb2.setText(solutionTextP2);
        solutionLb3.setText(solutionTextP3);
        solutionLb31.setText(solutionTextP31);
        solutionLb32.setText(solutionTextP32);
        solutionLb4.setText(solutionTextP4);
        MyGraph graphicsFunc1 = new MyGraph(null, 2);
        MyGraph graphicsFunc2 = new MyGraph(null, 3);
        panel.add(solutionLb1);
        panel.add(getFormula(formula7));
        panel.add(getFormula(formula8));
        panel.add(solutionLb11);
        panel.add(graphicsFunc1);
        panel.add(solutionLb2);
        panel.add(graphicsFunc2);
        panel.add(solutionLb3);
        panel.add(getFormula(formula5));
        panel.add(solutionLb31);
        panel.add(getFormula(formula6));
        panel.add(solutionLb32);
        panel.add(getFormula(formula4));
        panel.add(solutionLb4);
    }

    @Override
    public void initInputPanel(JPanel panel) {
        panel.removeAll();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        panel.setAlignmentY(200);
        
        JLabel lb1 = new JLabel();
        lb1.setMaximumSize(new Dimension(1000, 25));
        lb1.setText("<html><span style='font-size:11px'><b>Ввод исходных данных для решения задачи</b></span></html>");
        JPanel p0 = new JPanel();
        p0.setLayout(new BoxLayout(p0, BoxLayout.X_AXIS));
        p0.add(lb1);
        panel.add(p0);
        
        JLabel lb2 = new JLabel();
        lb2.setMaximumSize(new Dimension(650, 25));
        lb2.setText("Сумма(v), которую платит Рон Ненси, если тренер решает забить гол и команда побеждает.");
        jf_v.setMaximumSize(new Dimension(350, 25));
        AbstractDocument doc = (AbstractDocument) jf_v.getDocument();
        DocumentFilter filter = new UppercaseDocumentFilter();
        doc.setDocumentFilter(filter);
        JPanel p2 = new JPanel();
        p2.setLayout(new BoxLayout(p2, BoxLayout.X_AXIS));
        p2.add(lb2);
        p2.add(jf_v);
        panel.add(p2);

        JLabel lb12 = new JLabel();
        lb12.setMaximumSize(new Dimension(650, 25));
        lb12.setText("Сумма(platesh3), которую платит Рон Ненси, если тренер принимает решение попробовать забить гол и команда проигрывает.");
        platesh3.setMaximumSize(new Dimension(350, 25));
        AbstractDocument doc12 = (AbstractDocument) platesh3.getDocument();
        doc12.setDocumentFilter(filter);
        JPanel p12 = new JPanel();
        p12.setLayout(new BoxLayout(p12, BoxLayout.X_AXIS));
        p12.add(lb12);
        p12.add(platesh3);
        panel.add(p12);
        
        JLabel lb10 = new JLabel();
        lb10.setMaximumSize(new Dimension(650, 25));
        lb10.setText("Сумма(platesh), которую платит Рон Ненси, если тренер решится на попытку тачдауна и команда победит.");
        platesh1.setMaximumSize(new Dimension(350, 25));
        AbstractDocument doc10 = (AbstractDocument) platesh1.getDocument();
        doc10.setDocumentFilter(filter);
        JPanel p10 = new JPanel();
        p10.setLayout(new BoxLayout(p10, BoxLayout.X_AXIS));
        p10.add(lb10);
        p10.add(platesh1);
        panel.add(p10);
        
        JLabel lb11 = new JLabel();
        lb11.setMaximumSize(new Dimension(650, 25));
        lb11.setText("Сумма(platesh2), которую платит Рон Ненси, если тренер решится на попытку тачдауна и команда проиграет.");
        platesh2.setMaximumSize(new Dimension(350, 25));
        AbstractDocument doc11 = (AbstractDocument) platesh2.getDocument();
        doc11.setDocumentFilter(filter);
        JPanel p11 = new JPanel();
        p11.setLayout(new BoxLayout(p11, BoxLayout.X_AXIS));
        p11.add(lb11);
        p11.add(platesh2);
        panel.add(p11);
  
        JLabel lb3 = new JLabel();
        lb3.setMaximumSize(new Dimension(650, 25));
        lb3.setText("<html><span style='font-size:11px'>Вероятность(p<sub>11</sub>) того, что тренер решит забить гол(y<sub>1</sub>) и команда победить(x<sub>1</sub>).</span></html>");
        jf_p11.setMaximumSize(new Dimension(350, 25));
        AbstractDocument doc3 = (AbstractDocument) jf_p11.getDocument();
        doc3.setDocumentFilter(filter);
        JPanel p3 = new JPanel();
        p3.setLayout(new BoxLayout(p3, BoxLayout.X_AXIS));
        p3.add(lb3);
        p3.add(jf_p11);
        panel.add(p3);
        
        JLabel lb4 = new JLabel();
        lb4.setMaximumSize(new Dimension(650, 25));
        lb4.setText("<html><span style='font-size:11px'>Вероятность(p<sub>12</sub>) того, что тренер решит забить гол(y<sub>1</sub>) и команда проиграет(x<sub>2</sub>).</span></html>");
        jf_p12.setMaximumSize(new Dimension(350, 25));
        AbstractDocument doc4 = (AbstractDocument) jf_p12.getDocument();
        doc4.setDocumentFilter(filter);
        JPanel p4 = new JPanel();
        p4.setLayout(new BoxLayout(p4, BoxLayout.X_AXIS));
        p4.add(lb4);
        p4.add(jf_p12);
        panel.add(p4);
        
        JLabel lb7 = new JLabel();
        lb7.setMaximumSize(new Dimension(450, 25));
        lb7.setText("<html><span style='font-size:11px'>Вероятность(p<sub>c</sub>) того, что тренер примет решение забить гол(y<sub>1</sub>).</span></html>");
        jf_pc.setMaximumSize(new Dimension(350, 25));
        AbstractDocument doc7 = (AbstractDocument) jf_pc.getDocument();
        doc7.setDocumentFilter(filter);
        JPanel p7 = new JPanel();
        p7.setLayout(new BoxLayout(p7, BoxLayout.X_AXIS));
        p7.add(lb7);
        p7.add(jf_pc);
        panel.add(p7);
        
        JLabel lb8 = new JLabel();
        lb8.setMaximumSize(new Dimension(250, 25));
        lb8.setText("<html><span style='font-size:11px'>Ненси знает решение тренера?</span></html>");
        ButtonGroup group = new ButtonGroup();
        group.add(cb1);
        cb1.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    cb3.setEnabled(true);
                    cb4.setEnabled(true);
                } else {
                    cb3.setEnabled(false);
                    cb4.setEnabled(false);
                }
            }
        });
        group.add(cb2);
        JPanel p8 = new JPanel();
        
        p8.setLayout(new BoxLayout(p8, BoxLayout.X_AXIS));
        p8.add(lb8);
        for (Enumeration<AbstractButton> en = group.getElements(); en.hasMoreElements();) {
            AbstractButton b = en.nextElement();
            p8.add(b);
        }
        panel.add(p8);
        
        JLabel lb9 = new JLabel();
        lb9.setMaximumSize(new Dimension(250, 25));
        lb9.setText("<html><span style='font-size:11px'>Тренер решил сделать:</span></html>");
        ButtonGroup group2 = new ButtonGroup();
        group2.add(cb3);
        group2.add(cb4);
        cb3.setEnabled(false);
        cb4.setEnabled(false);
        JPanel p9 = new JPanel();
        p9.setMaximumSize(new Dimension(300, 20));
        p9.setLayout(new BoxLayout(p9, BoxLayout.X_AXIS));
        p9.add(lb9);
        for (Enumeration<AbstractButton> en = group2.getElements(); en.hasMoreElements();) {
            AbstractButton b = en.nextElement();
            p9.add(b);
        }
        panel.add(p9);
    }
    
    public class UppercaseDocumentFilter extends DocumentFilter {
        public void replace(DocumentFilter.FilterBypass fb, int offset, int length,
                String text, javax.swing.text.AttributeSet attr)
                throws BadLocationException {
            fb.insertString(offset, text.replaceAll("[^0-9.]", ""), attr);
        }
    }
    
    @Override
    public void initExamplePanel(JPanel panel) {
        panel.removeAll();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        TaskParams tp = new TaskParams();
        JLabel errorParms = new JLabel();
        try {
            tp.task_v2 = Double.parseDouble(jf_v.getText());
        } catch (Exception ex) {
            errorParms.setText("Задано некорректное значение параметра v.");
            panel.add(errorParms);
            return;
        }

        try {
            tp.task_p11 = Double.parseDouble(jf_p11.getText());
            if (tp.task_p11 > 1.0 || tp.task_p11 < 0.0) {
                errorParms.setText("Задано некорректное значение параметра p11.Должно быть в промежутке [0;1].");
                panel.add(errorParms);
                return;
            }
        } catch (Exception ex) {
            errorParms.setText("Задано некорректное значение параметра p11.");
            panel.add(errorParms);
            return;
        }

        try {
            tp.task_p12 = Double.parseDouble(jf_p12.getText());
            if (tp.task_p12 > 1.0 || tp.task_p12 < 0.0) {
                errorParms.setText("Задано некорректное значение параметра p12.Должно быть в промежутке [0;1]");
                panel.add(errorParms);
                return;
            }
        } catch (Exception ex) {
            errorParms.setText("Задано некорректное значение параметра p12.");
            panel.add(errorParms);
            return;
        }

        try {
            tp.task_pc = Double.parseDouble(jf_pc.getText());
            if (tp.task_pc > 1.0 || tp.task_pc < 0.0) {
                errorParms.setText("Задано некорректное значение параметра pc.Должно быть в промежутке [0;1]");
                panel.add(errorParms);
                return;
            }
        } catch (Exception ex) {
            errorParms.setText("Задано некорректное значение параметра pc.");
            panel.add(errorParms);
            return;
        }
//
        try {
            tp.task_v5 = Double.parseDouble(platesh1.getText());
        } catch (Exception ex) {
            errorParms.setText("Задано некорректное значение параметра v1.");
            panel.add(errorParms);
            return;
        }
        
        try {
            tp.task_v6 = Double.parseDouble(platesh2.getText());
            tp.task_v6 = tp.task_v6 * (-1.0);
        } catch (Exception ex) {
            errorParms.setText("Задано некорректное значение параметра v2.");
            panel.add(errorParms);
            return;
        }
        try {
            tp.task_v3 = Double.parseDouble(platesh3.getText());
            tp.task_v3 = tp.task_v3 * (-1.0);
        } catch (Exception ex) {
            errorParms.setText("Задано некорректное значение параметра v3.");
            panel.add(errorParms);
            return;
        }
        
        if (tp.task_p12 > tp.task_p11) {
            errorParms.setText("Заданs некорректные значения параметров p11 и p12. p11 должно быть больше или равно p12.");
            panel.add(errorParms);
            return;
        }
        
        boolean solutionNensi = false, solutionTrener = false;
        if(cb1.isSelected()) {
            solutionNensi = true;
            if(cb3.isSelected()) {
                solutionTrener = true;
            } else {
                solutionTrener = false;
            }
        } else {
            solutionNensi = false;
        }
        
        JLabel getParamsLb = new JLabel();
        JLabel getParamsLb2 = new JLabel();
        getParamsLb.setText("Введенные параметры:");
        getParamsLb2.setText(tp.showParams());
        panel.add(getParamsLb);
        panel.add(getParamsLb2);
        //------------------------------------------------------------------------------------------------
        String poleznost = "<html><span style='font-size:11px'><br><b>Расчет полезностей решений.</b><br>"
                + "<b>Ценности исходов:</b><br>"
                + "V(y<sub>1</sub>,d<sub>2</sub>)=v(a<sub>1</sub>)=v<sub>1</sub>=" + tp.task_v1.toString() + ";<br>"
                + "V(y<sub>1</sub>,d<sub>1</sub>,x<sub>1</sub>)=v(a<sub>2</sub>)=v<sub>2</sub>=" + tp.task_v2.toString() + ";<br>"
                + "V(y<sub>1</sub>,d<sub>1</sub>,x<sub>2</sub>)=v(a<sub>3</sub>)=v<sub>3</sub>=" + tp.task_v3.toString() + ";<br>"
                + "V(y<sub>2</sub>,d<sub>2</sub>)=v(a<sub>4</sub>)=v<sub>4</sub>=" + tp.task_v4.toString() + ";<br>"
                + "V(y<sub>2</sub>,d<sub>1</sub>,x<sub>1</sub>)=v(a<sub>5</sub>)=v<sub>5</sub>=" + tp.task_v5.toString() + ";<br>"
                + "V(y<sub>2</sub>,d<sub>1</sub>,x<sub>2</sub>)=v(a<sub>6</sub>)=v<sub>6</sub>=" + tp.task_v6 + ".<br>"
                + "<b>Полезности решений:</b><br>"
                + "при Y=y<sub>1</sub><br></span></html>";
        String poleznost1 = "u(d_1)=p_{11}*v_2+p_{21}*v_3=p_{11}*v_2+(1-p_{11})*(v_3)=(v_2-v_3)*p_{11}+v_3 = "
                + "(" + tp.task_v2.toString() + "-(" + tp.task_v3.toString() + "))*" + tp.task_p11.toString() + "+" + tp.task_v3.toString() + "=" + String.format("%.4f", ((tp.task_v2 - tp.task_v3) * tp.task_p11 + tp.task_v3)) + " ,"
                + "u(d_2)=v_1=" + tp.task_v1.toString() + ";";
        String poleznost2 = "<html><span style='font-size:11px'>при Y=y<sub>2</sub></span></html>";
        String poleznost3 = "u(d_1)=p_{12}*v_5+p_{22}*v_6=p_{12}*v_5+(1-p_{12})*v_6=(v_5-v_6)*p_{12}+(v_6)"
                + "(" + tp.task_v5.toString() + "-(" + tp.task_v6.toString() + "))*" + tp.task_p12.toString() + "+" + tp.task_v6.toString() + "=" + String.format("%.4f", ((tp.task_v5 - tp.task_v6) * tp.task_p12 + tp.task_v6)) + " ,"
                + "u(d_2)=v_4=" + tp.task_v4.toString() + ".";
        JLabel poleznostLb = new JLabel();
        JLabel poleznostLb2 = new JLabel();
        poleznostLb.setText(poleznost);
        poleznostLb2.setText(poleznost2);
        panel.add(poleznostLb);
        panel.add(getFormula(poleznost1));
        panel.add(poleznostLb2);
        panel.add(getFormula(poleznost3));
        
        //------------------------------------------------------------------------------------------------
        if (solutionNensi) {
            double p12_ = (double)tp.task_v6/(tp.task_v6 - tp.task_v5);
            double p11_ = (double)tp.task_v3/(tp.task_v3 - tp.task_v2);
            tp.task_p11_ = p11_;
            tp.task_p12_ = p12_;
            String solution = "<html><span style='font-size:11px'><b>Выбор решения</b><br>";
            String formula9 = "\\[ d* = \\left\\{"
                    + "  \\begin{array}{l l}"
                    + "    d_1, & \\quad \\text{если u(d1)>u(d2)}\\\\"
                    + "    d_2, & \\quad \\text{иначе}"
                    + "  \\end{array} \\right.\\]";
            String solution1 = "<html><span style='font-size:11px'>При Y = y<sub>1</sub>:</span></html>";
            String formula12 ="(v_2-v_3)*p_{11}+v_3 \\geq 0, (v_2-v_3)*p'_{11}+v_3 = 0,"
                            + "p'_{11} = v_3/(v_3-v_2) = " + String.format("%.4f", p11_);
            String solution2 =  "<html><span style='font-size:11px'>где p'<sub>11</sub> и p'<sub>12</sub> - вероятность безразличия<br>При Y = y<sub>2</sub>:</span></html>";
            String formula13 = "(v_5-v_6)*p_{12}+v_6 \\geq 0, (v_5-v_6)*p'_{12}+v_6 = 0,"
                    + "p'_{12} = v_6/(v_6-v_5) = " + String.format("%.4f", p12_);

            int area = 0;
            if (tp.task_p12 >= p12_ && tp.task_p11 >= p11_) {
                area = 1;
            } else if (tp.task_p12 < p12_ && tp.task_p11 >= p11_ && tp.task_p12 >= 0) {
                area = 2;
            } else if (tp.task_p12 >= p12_ && tp.task_p11 < p11_) {
                area = 3;
            } else if (tp.task_p12 < p12_ && tp.task_p11 < p11_) {
                area = 4;
            }
            JLabel solutionResult = new JLabel();
            if (area == 1) {
                solutionResult.setText("Область I, заключить пари.");
            } else if (area == 2) {
                if (solutionTrener) {
                    solutionResult.setText("Область II, заключить пари.");
                } else {
                    solutionResult.setText("Область II, не заключать пари.");
                }
            } else if (area == 3) {
                if (solutionTrener) {
                    solutionResult.setText("Область III, не заключать пари.");
                } else {
                    solutionResult.setText("Область III, заключить пари.");
                }
            } else if (area == 4) {
                solutionResult.setText("Область IV, не заключать пари.");
            }
            JLabel solutionLb = new JLabel();
            solutionLb.setText(solution);
            panel.add(solutionLb);
            panel.add(getFormula(formula9));
            
            JLabel solutionLb1 = new JLabel();
            solutionLb1.setText(solution1);
            panel.add(solutionLb1);
            panel.add(getFormula(formula12));
            
            JLabel solutionLb2 = new JLabel();
            solutionLb2.setText(solution2);
            panel.add(solutionLb2);
            panel.add(getFormula(formula13));
            //MyGraph graphicsFunc3 = new MyGraph(tp, 4);
            //panel.add(graphicsFunc3);
            JLabel solEnd = new JLabel();
            String result = "<html><span style='font-size:11px'><b>Ответ : </b>" + solutionResult.getText() + "</span></html>";
            solEnd.setText(result);
            panel.add(solEnd);
        //------------------------------------------------------------------------------------------------    
        } else {
            Double solutionValue = tp.task_pc * ((tp.task_v2 - tp.task_v3) * tp.task_p11 + tp.task_v3) + (1 - tp.task_pc) * ((tp.task_v5-tp.task_v6) * tp.task_p12 + tp.task_v6);
            String solution2 = "<html><span style='font-size:11px'><b>Выбор решения.</b><br></b><br></span></html>";
            String formula9 = "\\[ d* = \\left\\{"
                    + "  \\begin{array}{l l}"
                    + "    d_1, & \\quad \\text{если u(d1)>0}\\\\"
                    + "    d_2, & \\quad \\text{иначе}"
                    + "  \\end{array} \\right.\\]";
            String formula10 = "u(d_1) = p_c[(v_2-v_3)*p_{11}+v_3] + (1 - p_c)*[(v_5 - v_6)*p_{12} + v_6]="
                    + tp.task_pc.toString() + "*[" + tp.task_v2.toString() + "-(" + tp.task_v3.toString() + "))"
                    + "*" + tp.task_p11.toString() + "+" + tp.task_v3.toString() + "*(1-" + tp.task_pc +")*((" + tp.task_v5.toString() + "-(" + tp.task_v6.toString() + "))*" + tp.task_p12.toString() + "+(" + tp.task_v6.toString() + ")="
                    + String.format("%.4f", solutionValue) + "  ,"
                    + "u(d_2) = 0.";
            JLabel solutionLb2 = new JLabel();
            solutionLb2.setText(solution2);
            panel.add(solutionLb2);
            panel.add(getFormula(formula9));
            panel.add(getFormula(formula10));
            
            JLabel solutionResult2 = new JLabel();
            if (solutionValue > 0.0) {
                solutionResult2.setText("<html><span style='font-size:11px'><b>Ответ : </b>заключить пари.</span></html>");
            } else {
                solutionResult2.setText("<html><span style='font-size:11px'><b>Ответ : </b>не заключать пари.</span></html>");
            }
            panel.add(solutionResult2);
        }
    }

    @Override
    public ActionListener getPressSaveListener() throws IllegalArgumentException {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evn) {
                File file = new File(resourcePath + fileName);
                try {
                    String saved1 = jf_v.getText();
                    String saved2 = jf_p11.getText();
                    String saved3 = jf_p12.getText();
                    String saved4 = jf_pc.getText();
                    String saved5 = platesh1.getText();
                    String saved6 = platesh2.getText();
                    String saved7 = platesh3.getText();
                    if (saved1.equals("")) {
                        saved1 = "0";
                    }
                    if (saved2.equals("")) {
                        saved2 = "0";
                    }
                    if (saved3.equals("")) {
                        saved3 = "0";
                    }
                    if (saved4.equals("")) {
                        saved4 = "0";
                    }
                    if (saved5.equals("")) {
                        saved5 = "0";
                    }
                    if (saved6.equals("")) {
                        saved6 = "0";
                    }
                    if (saved7.equals("")) {
                        saved7 = "0";
                    }
                    if (!file.exists()) {
                        file.createNewFile();
                    }
                    PrintWriter out = new PrintWriter(file.getAbsoluteFile());
                    try {
                        out.println(saved1);
                        out.println(saved2);
                        out.println(saved3);
                        out.println(saved4);
                        out.println(saved5);
                        out.println(saved6);
                        out.println(saved7);
                    } finally {
                        out.close();
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        };
    }

    @Override
    public ActionListener getPressLoadListener() throws IllegalArgumentException {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evn) {
                StringBuilder sb = new StringBuilder();
                File file = new File(resourcePath + fileName);
                file.exists();
                try {
                    BufferedReader in = new BufferedReader(new FileReader(file.getAbsoluteFile()));
                    try {
                        jf_v.setText(in.readLine());
                        jf_p11.setText(in.readLine());
                        jf_p12.setText(in.readLine());
                        jf_pc.setText(in.readLine());
                        platesh1.setText(in.readLine());
                        platesh2.setText(in.readLine());
                        platesh3.setText(in.readLine());
                    } finally {
                        in.close();
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        };
    }

    @Override
    public ActionListener getDefaultValuesListener() throws IllegalArgumentException {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jf_v.setText("2");
                jf_p11.setText("0.6");
                jf_p12.setText("0.4");
                jf_pc.setText("0.5");
                platesh1.setText("1.5");
                platesh2.setText("3");
                platesh3.setText("1");
            }
        };
    }

    public JLabel getFormula(String text) {
        TeXFormula formula = new TeXFormula(text);
        TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 20);
        icon.setInsets(new Insets(5, 5, 5, 5));
        BufferedImage image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = image.createGraphics();
        JLabel jl = new JLabel();
        jl.setForeground(new Color(0, 0, 0));
        icon.paintIcon(jl, g2, 0, 0);
        JLabel formulaIcon = new JLabel(new ImageIcon(image));
        formulaIcon.setBounds(20, 20, 200, 100);
        return formulaIcon;
    }

    public class MyGraph extends JLabel {
        private final static int SPACEUNIT = 30;
        private int type = 1;
        private TaskParams parms;
        public MyGraph(TaskParams parms, int type) {
            super();
            this.type = type;
            this.parms = parms;
            setMinimumSize(new Dimension(845, 390));
            setMaximumSize(new Dimension(845, 390));
            setPreferredSize(new Dimension(845, 390));
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponents(g);
            switch (this.type) {
                case 1:
                    paintTree(g);
                    break;
                case 2:
                    paintGraphics1(g);
                    break;
                case 3:
                    paintGraphics2(g);
                    break;
                case 4:
                    paintGraphics3(g);
                    break;
                default:
                    break;
            }
        }

        private void paintTree(Graphics g) {
            g.drawOval(0, 5 * SPACEUNIT, SPACEUNIT, SPACEUNIT);
            g.drawRect(4 * SPACEUNIT, (int) (2.5 * SPACEUNIT), SPACEUNIT, SPACEUNIT);
            g.drawRect(4 * SPACEUNIT, (int) (7.5 * SPACEUNIT), SPACEUNIT, SPACEUNIT);
            g.drawOval(8 * SPACEUNIT, (int) (4.5 * SPACEUNIT), SPACEUNIT, SPACEUNIT);
            g.drawOval(8 * SPACEUNIT, (int) (9.5 * SPACEUNIT), SPACEUNIT, SPACEUNIT);

            g.drawLine(SPACEUNIT, (int) (5.5 * SPACEUNIT), 3 * SPACEUNIT, (int) (3 * SPACEUNIT));
            g.drawLine(3 * SPACEUNIT, (int) (3 * SPACEUNIT), 4 * SPACEUNIT, (int) (3 * SPACEUNIT));
            g.drawString("y1", (int) (3.5 * SPACEUNIT), (int) (2.8 * SPACEUNIT));

            g.drawLine(SPACEUNIT, (int) (5.5 * SPACEUNIT), 3 * SPACEUNIT, (int) (8 * SPACEUNIT));
            g.drawLine(3 * SPACEUNIT, (int) (8 * SPACEUNIT), 4 * SPACEUNIT, (int) (8 * SPACEUNIT));
            g.drawString("y2", (int) (3.5 * SPACEUNIT), (int) (7.8 * SPACEUNIT));

            g.drawLine(5 * SPACEUNIT, (int) (3 * SPACEUNIT), 7 * SPACEUNIT, (int) (5 * SPACEUNIT));
            g.drawLine(7 * SPACEUNIT, (int) (5 * SPACEUNIT), 8 * SPACEUNIT, (int) (5 * SPACEUNIT));
            g.drawString("d2", (int) (7 * SPACEUNIT), (int) (4.8 * SPACEUNIT));

            g.drawLine(5 * SPACEUNIT, (int) (8 * SPACEUNIT), 7 * SPACEUNIT, (int) (10 * SPACEUNIT));
            g.drawLine(7 * SPACEUNIT, (int) (10 * SPACEUNIT), 8 * SPACEUNIT, (int) (10 * SPACEUNIT));
            g.drawString("d1", (int) (7 * SPACEUNIT), (int) (9.8 * SPACEUNIT));
       
            g.drawLine(5 * SPACEUNIT, (int) (3 * SPACEUNIT), 7 * SPACEUNIT, (int) (1 * SPACEUNIT));
            g.drawLine(7 * SPACEUNIT, (int) (1 * SPACEUNIT), 11 * SPACEUNIT, (int) (1 * SPACEUNIT));
            g.drawString("d1", (int) (7 * SPACEUNIT), (int) (0.8 * SPACEUNIT));

            g.drawLine(5 * SPACEUNIT, (int) (8 * SPACEUNIT), 7 * SPACEUNIT, (int) (6 * SPACEUNIT));
            g.drawLine(7 * SPACEUNIT, (int) (6 * SPACEUNIT), 11 * SPACEUNIT, (int) (6 * SPACEUNIT));
            g.drawString("d2", (int) (7 * SPACEUNIT), (int) (5.8 * SPACEUNIT));

            g.drawLine(9 * SPACEUNIT, (int) (5 * SPACEUNIT), 10 * SPACEUNIT, (int) (3.5 * SPACEUNIT));
            g.drawString("x1", (int) (9.3 * SPACEUNIT), (int) (4 * SPACEUNIT));
            g.drawLine(10 * SPACEUNIT, (int) (3.5 * SPACEUNIT), 11 * SPACEUNIT, (int) (3.5 * SPACEUNIT));
            g.drawString("p11", (int) (10.5 * SPACEUNIT), (int) (3.3 * SPACEUNIT));

            g.drawLine(9 * SPACEUNIT, (int) (5 * SPACEUNIT), 10 * SPACEUNIT, (int) (5.5 * SPACEUNIT));
            g.drawString("x2", (int) (9.3 * SPACEUNIT), (int) (5.2 * SPACEUNIT));
            g.drawLine(10 * SPACEUNIT, (int) (5.5 * SPACEUNIT), 11 * SPACEUNIT, (int) (5.5 * SPACEUNIT));
            g.drawString("p21", (int) (10.5 * SPACEUNIT), (int) (5.3 * SPACEUNIT));

            g.drawLine(9 * SPACEUNIT, (int) (10 * SPACEUNIT), 10 * SPACEUNIT, (int) (8.5 * SPACEUNIT));
            g.drawString("x1", (int) (9.3 * SPACEUNIT), (int) (9 * SPACEUNIT));
            g.drawLine(10 * SPACEUNIT, (int) (8.5 * SPACEUNIT), 11 * SPACEUNIT, (int) (8.5 * SPACEUNIT));
            g.drawString("p12", (int) (10.5 * SPACEUNIT), (int) (8.3 * SPACEUNIT));

            g.drawLine(9 * SPACEUNIT, (int) (10 * SPACEUNIT), 10 * SPACEUNIT, (int) (10.5 * SPACEUNIT));
            g.drawString("x2", (int) (9.3 * SPACEUNIT), (int) (10.2 * SPACEUNIT));
            g.drawLine(10 * SPACEUNIT, (int) (10.5 * SPACEUNIT), 11 * SPACEUNIT, (int) (10.5 * SPACEUNIT));
            g.drawString("p22", (int) (10.5 * SPACEUNIT), (int) (10.3 * SPACEUNIT));

            g.drawString("v1 = v(a1)    a1", (int) (11.2 * SPACEUNIT), (int) (1 * SPACEUNIT));
            g.drawString("v2 = v(a2)    a3", (int) (11.2 * SPACEUNIT), (int) (3.5 * SPACEUNIT));
            g.drawString("v3 = v(a3)    a3", (int) (11.2 * SPACEUNIT), (int) (5.5 * SPACEUNIT));
            g.drawString("v4 = v(a4)    a4", (int) (11.2 * SPACEUNIT), (int) (6 * SPACEUNIT));
            g.drawString("v5 = v(a5)    a5", (int) (11.2 * SPACEUNIT), (int) (8.5 * SPACEUNIT));
            g.drawString("v6 = v(a6)    a6", (int) (11.2 * SPACEUNIT), (int) (10.5 * SPACEUNIT));
        }

        private void paintGraphics1(Graphics g) {
            g.setFont(new Font("Terminal", Font.BOLD, 12));
            super.paintComponent(g);
            g.drawLine((int) (1.1 * SPACEUNIT), (int) (1 * SPACEUNIT), (int) (1.1 * SPACEUNIT), (int) (10.2 * SPACEUNIT));
            g.drawLine(SPACEUNIT, (int) (10 * SPACEUNIT), 10 * SPACEUNIT, (int) (10 * SPACEUNIT));

            g.drawLine((int) (1.1 * SPACEUNIT), (int) (10 * SPACEUNIT), 8 * SPACEUNIT, (int) (2 * SPACEUNIT));
            g.drawLine((int) (8 * SPACEUNIT), (int) (10 * SPACEUNIT), 8 * SPACEUNIT, (int) (2 * SPACEUNIT));

            g.drawLine((int) (2.8 * SPACEUNIT), (int) (8 * SPACEUNIT), 8 * SPACEUNIT, (int) (8 * SPACEUNIT));
            g.drawLine((int) (4.6 * SPACEUNIT), (int) (6 * SPACEUNIT), (int) (4.6 * SPACEUNIT), (int) (10 * SPACEUNIT));

            g.drawLine((int) (SPACEUNIT), (int) (8 * SPACEUNIT), (int) (1.2 * SPACEUNIT), (int) (8 * SPACEUNIT));
            g.drawLine((int) (SPACEUNIT), (int) (2 * SPACEUNIT), (int) (1.2 * SPACEUNIT), (int) (2 * SPACEUNIT));

            g.drawString("0", (int) (0.7 * SPACEUNIT), (int) (10.5 * SPACEUNIT));
            g.drawString("v3/(v3-v2)", (int) (4.3 * SPACEUNIT), (int) (10.5 * SPACEUNIT));
            g.drawString("1     p11", (int) (8 * SPACEUNIT), (int) (10.5 * SPACEUNIT));

            g.drawString("v6/(v6-v5)", (int) (0.5 * SPACEUNIT), (int) (8 * SPACEUNIT));
            g.drawString("1", (int) (0.5 * SPACEUNIT), (int) (2 * SPACEUNIT));
            g.drawString("p22", (int) (0.5 * SPACEUNIT), (int) (1.5 * SPACEUNIT));

            g.drawString("I", (int) (6 * SPACEUNIT), (int) (7 * SPACEUNIT));
            g.drawString("II", (int) (6 * SPACEUNIT), (int) (9 * SPACEUNIT));
            g.drawString("III", (int) (4 * SPACEUNIT), (int) (7.5 * SPACEUNIT));
            g.drawString("IV", (int) (3 * SPACEUNIT), (int) (9 * SPACEUNIT));

            g.drawString("Рис. 1.29. Вероятность выиграть пари", (int) (0.5 * SPACEUNIT), (int) (11.2 * SPACEUNIT));
            g.drawString("в зависимости от различных решений тренера", (int) (0.5 * SPACEUNIT), (int) (11.6 * SPACEUNIT));
        }

        private void paintGraphics2(Graphics g) {
            g.setFont(new Font("Terminal", Font.BOLD, 12));
            super.paintComponent(g);
            g.drawLine((int) (5.0 * SPACEUNIT), (int) (8 * SPACEUNIT), (int) (5.0 * SPACEUNIT), (int) (10 * SPACEUNIT));
            g.drawLine((int) (5.4 * SPACEUNIT), (int) (8 * SPACEUNIT), (int) (5.4 * SPACEUNIT), (int) (10 * SPACEUNIT));

            g.drawLine((int) (5.0 * SPACEUNIT), (int) (8 * SPACEUNIT), (int) (4.6 * SPACEUNIT), (int) (7.5 * SPACEUNIT));
            g.drawLine((int) (5.4 * SPACEUNIT), (int) (8 * SPACEUNIT), (int) (4.6 * SPACEUNIT), (int) (7 * SPACEUNIT));

            g.drawLine((int) (4.6 * SPACEUNIT), (int) (7.5 * SPACEUNIT), (int) (3.3 * SPACEUNIT), (int) (7.5 * SPACEUNIT));
            g.drawLine((int) (4.6 * SPACEUNIT), (int) (7 * SPACEUNIT), (int) (3.7 * SPACEUNIT), (int) (7 * SPACEUNIT));

            g.drawLine((int) (4.6 * SPACEUNIT), (int) (8 * SPACEUNIT), (int) (8 * SPACEUNIT), (int) (4.8 * SPACEUNIT));


            g.drawLine((int) (1.1 * SPACEUNIT), (int) (1 * SPACEUNIT), (int) (1.1 * SPACEUNIT), (int) (10.2 * SPACEUNIT));
            g.drawLine(SPACEUNIT, (int) (10 * SPACEUNIT), 10 * SPACEUNIT, (int) (10 * SPACEUNIT));

            g.drawLine((int) (1.1 * SPACEUNIT), (int) (10 * SPACEUNIT), 8 * SPACEUNIT, (int) (2 * SPACEUNIT));
            g.drawLine((int) (8 * SPACEUNIT), (int) (10 * SPACEUNIT), 8 * SPACEUNIT, (int) (2 * SPACEUNIT));

            g.drawLine((int) (2.8 * SPACEUNIT), (int) (8 * SPACEUNIT), 8 * SPACEUNIT, (int) (8 * SPACEUNIT));
            g.drawLine((int) (4.6 * SPACEUNIT), (int) (6 * SPACEUNIT), (int) (4.6 * SPACEUNIT), (int) (10 * SPACEUNIT));

            g.drawLine((int) (SPACEUNIT), (int) (8 * SPACEUNIT), (int) (1.2 * SPACEUNIT), (int) (8 * SPACEUNIT));
            g.drawLine((int) (SPACEUNIT), (int) (2 * SPACEUNIT), (int) (1.2 * SPACEUNIT), (int) (2 * SPACEUNIT));

            g.drawString("0", (int) (0.7 * SPACEUNIT), (int) (10.5 * SPACEUNIT));
            g.drawString("v3/(v3-v2)", (int) (4.3 * SPACEUNIT), (int) (10.5 * SPACEUNIT));
            g.drawString("1     p11", (int) (8 * SPACEUNIT), (int) (10.5 * SPACEUNIT));

            g.drawString("v6/(v6-v5)", (int) (0.5 * SPACEUNIT), (int) (8 * SPACEUNIT));
            g.drawString("1", (int) (0.5 * SPACEUNIT), (int) (2 * SPACEUNIT));
            g.drawString("p22", (int) (0.5 * SPACEUNIT), (int) (1.5 * SPACEUNIT));

            g.drawString("I", (int) (6 * SPACEUNIT), (int) (7 * SPACEUNIT));
            g.drawString("II", (int) (6 * SPACEUNIT), (int) (9 * SPACEUNIT));
            g.drawString("III", (int) (4 * SPACEUNIT), (int) (7.85 * SPACEUNIT));
            g.drawString("IV", (int) (3 * SPACEUNIT), (int) (9 * SPACEUNIT));

            g.drawString("Рис. 1.30. Области с положительным", (int) (0.5 * SPACEUNIT), (int) (11.2 * SPACEUNIT));
            g.drawString("ожидаемым исходом", (int) (0.5 * SPACEUNIT), (int) (11.6 * SPACEUNIT));
        }
        
        private void paintGraphics3(Graphics g) {
            if(parms == null) {
                return;
            }
            g.setFont(new Font("Terminal", Font.BOLD, 12));
            super.paintComponent(g);
            g.drawLine((int) (1.1 * SPACEUNIT), (int) (1 * SPACEUNIT), (int) (1.1 * SPACEUNIT), (int) (10.2 * SPACEUNIT));
            g.drawLine(SPACEUNIT, (int) (10 * SPACEUNIT), 10 * SPACEUNIT, (int) (10 * SPACEUNIT));

            g.drawLine((int) (1.1 * SPACEUNIT), (int) (10 * SPACEUNIT), 8 * SPACEUNIT, (int) (2 * SPACEUNIT));
            g.drawLine((int) (8 * SPACEUNIT), (int) (10 * SPACEUNIT), 8 * SPACEUNIT, (int) (2 * SPACEUNIT));
            
            g.drawOval((int)(parms.task_p11 * 10) * SPACEUNIT, (int) ((10 - (parms.task_p12 * 10) + 1) * SPACEUNIT), 4, 4);
            g.drawLine((int) (1 * SPACEUNIT), (int) ((10 - (parms.task_p12_ * 10) + 1) * SPACEUNIT), 8 * SPACEUNIT, (int) ((10 - (parms.task_p12_ * 10) + 1) * SPACEUNIT));
            g.drawLine((int) ((parms.task_p11_ * 10) * SPACEUNIT), (int) (2 * SPACEUNIT), (int) ((parms.task_p11_ * 10) * SPACEUNIT), (int) (10 * SPACEUNIT));
            
            g.drawLine((int) (SPACEUNIT), (int) (8 * SPACEUNIT), (int) (1.2 * SPACEUNIT), (int) (8 * SPACEUNIT));
            g.drawLine((int) (SPACEUNIT), (int) (2 * SPACEUNIT), (int) (1.2 * SPACEUNIT), (int) (2 * SPACEUNIT));

            g.drawString("0", (int) (0.7 * SPACEUNIT), (int) (10.5 * SPACEUNIT));
            g.drawString("1     p11", (int) (8 * SPACEUNIT), (int) (10.5 * SPACEUNIT));

            g.drawString("1", (int) (0.5 * SPACEUNIT), (int) (2 * SPACEUNIT));
            g.drawString("p22", (int) (0.5 * SPACEUNIT), (int) (1.5 * SPACEUNIT));
        }
        @Override
        public void update(Graphics g) {
            paintComponent(g);
        }

    }
}
